# Customise this file, documentation can be found here:
# https://github.com/fastlane/fastlane/tree/master/fastlane/docs
# All available actions: https://docs.fastlane.tools/actions
# can also be listed using the `fastlane actions` command

# Change the syntax highlighting to Ruby
# All lines starting with a # are ignored when running `fastlane`

# If you want to automatically update fastlane if a new version is available:
# update_fastlane

# This is the minimum version number required.
# Update this, if you use features of a newer version
fastlane_version "2.28.3"

default_platform :android

platform :android do
  desc "Submits the APK to Google Play internal testing track"
  lane :upload_to_play_store_internal do
    upload_to_play_store(
      track: 'internal',
      apk: '../build/app/outputs/apk/release/app-release.apk',
      json_key_data: ENV['JSON_KEY_DATA']
    )
  end

  desc "Promote Internal to Alpha"
  lane :promote_internal_to_alpha do
    upload_to_play_store(
      track: 'internal',
      track_promote_to: 'alpha',
      json_key_data: ENV['JSON_KEY_DATA']
    )
  end

  desc "Promote Alpha to Beta"
  lane :promote_alpha_to_beta do
    upload_to_play_store(
      track: 'alpha',
      track_promote_to: 'beta',
      json_key_data: ENV['JSON_KEY_DATA']
    )
  end

  desc "Promote Alpha to Production"
  lane :promote_alpha_to_production do
    upload_to_play_store(
      track: 'alpha',
      track_promote_to: 'production',
      json_key_data: ENV['JSON_KEY_DATA']
    )
  end

  desc "Promote Beta to Production"
  lane :promote_beta_to_production do
    upload_to_play_store(
      track: 'beta',
      track_promote_to: 'production',
      json_key_data: ENV['JSON_KEY_DATA']
    )
  end

  after_all do |lane|
    # This block is called, only if the executed lane was successful

    slack(
      message: "Successfully deployed new App Update."
    )
  end

  error do |lane, exception|
    slack(
      message: exception.message,
      success: false
    )
  end
end


# More information about multiple platforms in fastlane: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Platforms.md
# All available actions: https://docs.fastlane.tools/actions

# fastlane reports which actions are used
# No personal data is sent or shared. Learn more at https://github.com/fastlane/enhancer
