/// 2019.10.11
/// 1. pagination
///
/// 2019.10.08
/// 1. get purchase history
///
/// 2019.10.06
/// factored out from user_my_page.dart
/// menuPage (name to be changed to payment page with modifications)

part of 'main.dart';

final GlobalKey<ScaffoldState> paymentPageScaffoldKey =
    GlobalKey<ScaffoldState>();

class PaymentWidget extends StatefulWidget {
  final UserWidgetState parent;
  PaymentWidget(this.parent);

  @override
  State createState() => new PaymentWidgetState(parent);
}

class PaymentWidgetState extends State<PaymentWidget> {
  final UserWidgetState parent;
  PaymentWidgetState(this.parent);

  String fullname;
  List<dynamic> _purchaseHistoryListRaw;
  String _nextCursorUrl;
  bool _getPurchaseHistoryCalled;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  initState() {
    super.initState();
    fullname = Singleton().sharedPrefs.getString("user_fullname");
    _purchaseHistoryListRaw = List<dynamic>();
    _nextCursorUrl = null;
    _getPurchaseHistoryCalled = false;

    getPurchaseHistory();
  }

  Future<void> getPurchaseHistory() async {
    String url = SERVER_URL + PURCHASES_API;
    if (_nextCursorUrl != null) url = _nextCursorUrl;

    if (_purchaseHistoryListRaw.isNotEmpty && _nextCursorUrl == null) {
      return;
    }
    if (_getPurchaseHistoryCalled) {
      return;
    }
    _getPurchaseHistoryCalled = true;

    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + Singleton().sharedPrefs.getString('user_key')
    };

    Response response = await get(url, headers: headers).catchError((error) {
      Singleton().noInternetDialog(context).show();
    });
    // check the status code for the result
    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    // if success, store the key
    // using the key, get the user info
    if (statusCode == 200) {
      debugPrint('[payment page] get purchase history - OK!');
      setState(() {
        if (_purchaseHistoryListRaw.isEmpty && _nextCursorUrl == null) {
          _purchaseHistoryListRaw = data["results"];
        } else {
          _purchaseHistoryListRaw = [
            ..._purchaseHistoryListRaw,
            ...data["results"]
          ];
        }
        _nextCursorUrl = data["next"];
        _getPurchaseHistoryCalled = false;
      });
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
      debugPrint(
          '[payment page] get purchase history - FAILED [${statusCode.toString()}]');
      _getPurchaseHistoryCalled = false;
    }
  }

  Future tossDeposit() async {
    String url = TOSS_DEPOSIT_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };
    // job_id: 1 ~ 13
    // confirmation_method: 1 - email; 2 - business card; 3 - proof of employment
    String body = jsonEncode({
      "apiKey": '${DotEnv().env['TOSS_API_KEY']}',
      "bankName": '국민',
      "bankAccountNo": "92160101345143",
      "amount": int.parse(Singleton().remoteConfig.getString('chat_price')),
      "message": Singleton().sharedPrefs.getString('user_fullname')
    });

    parent._queServerTimer(LastServerCall.sendCompanyEmailVerification);
    Response response =
        await post(url, headers: headers, body: body).catchError((error) {
      Singleton().noInternetDialog(context).show();
      parent._dequeServerTimer();
      return;
    });

    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
      debugPrint('[payment_page] toss deposit - OK!');
      parent._dequeServerTimer();

      switch (data["resultType"]) {
        case 'SUCCESS':
          String scheme = data["success"]["scheme"];
          String link = data["success"]["link"];
          if (await canLaunch(scheme)) {
            await launch(scheme);
          } else {
            await launch(link);
          }
          break;
        case 'FAIL':
          Alert(
            style: AlertStyle(
              animationType: AnimationType.grow,
              animationDuration: const Duration(milliseconds: 500),
              isCloseButton: false,
              alertBorder: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
                side: BorderSide(
                  color: Colors.grey,
                ),
              ),
            ),
            context: context,
            title: Singleton().remoteConfig.getString('alert_title'),
            type: AlertType.error,
            desc: data,
            buttons: [
              DialogButton(
                child: Text(
                  Singleton()
                      .remoteConfig
                      .getString('toss_deposit_fail_dialog_button'),
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
                width: 120,
                color: Colors.grey,
              )
            ],
          ).show();
          break;
      }
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
      debugPrint('[payment_page] toss deposit - FAIL!');
      parent._dequeServerTimer();
    }
  }

  Widget paymentDialog() {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      Container(
        child: Text(
          Singleton().remoteConfig.getString('payment_dialog_subtitle'),
          style: TextStyle(fontSize: 18),
        ),
      ),
      Container(height: 20),
      Container(
        child: Text(
          Singleton()
              .remoteConfig
              .getString('bank_account_info')
              .replaceAll('\\n', '\n'),
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 18),
        ),
      ),
      Container(height: 10),
      Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Text(
          '${Singleton().remoteConfig.getString('payment_dialog_content').replaceAll('\\n', '\n')} (${Singleton().remoteConfig.getString('payment_dialog_fullname_label')}: $fullname)',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 12, color: Colors.black),
        ),
      ),
      Container(height: 10),
      Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Text(
          Singleton()
              .remoteConfig
              .getString('payment_dialog_notification')
              .replaceAll('\\n', '\n'),
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 12, color: Colors.red[300]),
        ),
      ),
      Container(height: 20),
      Row(
        children: <Widget>[
          Expanded(
            child: FlatButton(
                padding: EdgeInsets.all(8),
                color: Colors.red[300],
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0)),
                onPressed: () {
                  Clipboard.setData(ClipboardData(
                    text: Singleton()
                        .remoteConfig
                        .getString('bank_account_info')
                        .replaceAll('\\n', '\n'),
                  ));
                  paymentPageScaffoldKey.currentState.showSnackBar(SnackBar(
                      duration: const Duration(seconds: SNACKBAR_DURATION),
                      content: Text(Singleton()
                          .remoteConfig
                          .getString('payment_dialog_sb_info_copied'))));
                  Navigator.pop(context);
                },
                child: Text(
                    Singleton()
                        .remoteConfig
                        .getString('payment_dialog_info_copy_button'),
                    style: TextStyle(color: Colors.white, fontSize: 18))),
          ),
        ],
      ),
      Row(
        children: <Widget>[
          Expanded(
            child: FlatButton(
                padding: EdgeInsets.all(8),
                color: Colors.blueAccent.shade700,
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0)),
                onPressed: () {
                  tossDeposit();
                  Navigator.pop(context);
                },
                child: Text(
                    Singleton().remoteConfig.getString('toss_deposit_button'),
                    style: TextStyle(color: Colors.white, fontSize: 18))),
          ),
        ],
      )
    ]);
  }

  List<Widget> purchaseHistory() {
    List<Widget> purchaseHistoryWidgetList = List<Widget>();
    int userId = Singleton().sharedPrefs.getInt("user_id");

    purchaseHistoryWidgetList.add(Divider());
    for (var purchaseHistory in _purchaseHistoryListRaw) {
      Color textColor;
      if (purchaseHistory["transaction_from"] == userId) {
        textColor = Colors.red;
      } else {
        //if (purchaseHistory["transaction_to"] == userId) {
        textColor = Colors.green;
      }
      print('purchaseHistory["amount"]: ${purchaseHistory["amount"]}');
      if (purchaseHistory["amount"] == '4600') {
        purchaseHistoryWidgetList.add(Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text(
              reformatDateTime(UTC2LOCAL(purchaseHistory["created_at"])),
              style: TextStyle(color: textColor),
            ),
            Text(
              '기프티콘',
              style: TextStyle(color: textColor),
            )
          ],
        ));
      } else {
        purchaseHistoryWidgetList.add(Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text(
              reformatDateTime(UTC2LOCAL(purchaseHistory["created_at"])),
              style: TextStyle(color: textColor),
            ),
            Text(
              NumberFormat("#,###").format(
                purchaseHistory["amount"],
              ),
              style: TextStyle(color: textColor),
            )
          ],
        ));
      }
      purchaseHistoryWidgetList.add(Divider());
    }

    return purchaseHistoryWidgetList;
  }

  @override
  Widget build(BuildContext context) {
    Singleton().updateUserBalance();
    int _point = Singleton().sharedPrefs.getInt("user_balance");

    return Column(children: [
      Container(height: 40),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            '${Singleton().remoteConfig.getString('payment_balance_label')}\n${NumberFormat("#,###").format(_point)} P',
            style: TextStyle(fontSize: 18),
          ),
          FlatButton(
              color: Colors.red[300],
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(2.0)),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0)),
                        title: Text(Singleton()
                            .remoteConfig
                            .getString('payment_dialog_title')),
                        content: paymentDialog(),
                      );
                    });
              },
              child: Text(
                  Singleton()
                      .remoteConfig
                      .getString('payment_page_dialog_button'),
                  style: TextStyle(color: Colors.white, fontSize: 16))),
        ],
      ),
      Container(height: 40),
      Container(
        padding: EdgeInsets.only(left: 40, right: 40),
        child: Row(
          children: <Widget>[
            Text(
              Singleton().remoteConfig.getString('payment_history'),
              textAlign: TextAlign.right,
              style: TextStyle(fontSize: 18),
            ),
          ],
        ),
      ),
      Container(height: 20),
      NotificationListener<ScrollNotification>(
          onNotification: (ScrollNotification scrollInfo) {
            if (scrollInfo.metrics.pixels ==
                scrollInfo.metrics.maxScrollExtent) {
              getPurchaseHistory();
            }
            return true;
          },
          child: Expanded(
              child: ListView.builder(
            shrinkWrap: true,
            itemCount: _purchaseHistoryListRaw.length,
            itemBuilder: (context, index) {
              int userId = Singleton().sharedPrefs.getInt("user_id");
              Color textColor;
              if (_purchaseHistoryListRaw[index]["transaction_from"] ==
                  userId) {
                textColor = Colors.red;
              } else {
                textColor = Colors.green;
              }

              // TODO : needs to get rid of this two conditionals in one of the iterations where reward is not only one constant gifticon
              if (_purchaseHistoryListRaw[index]["amount"] ==
                      int.parse(Singleton()
                          .remoteConfig
                          .getString('payment_page_gifticon_price')) &&
                  _purchaseHistoryListRaw[index]["item_type"] == 'gifticon') {
                return Container();
              } else if (_purchaseHistoryListRaw[index]["amount"] ==
                      int.parse(Singleton()
                          .remoteConfig
                          .getString('payment_page_gifticon_price')) &&
                  _purchaseHistoryListRaw[index]["item_type"] == 'answer') {
                return Container(
                    padding: EdgeInsets.only(left: 16, right: 16),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Text(
                              reformatDateTime(UTC2LOCAL(
                                  _purchaseHistoryListRaw[index]
                                      ["created_at"])),
                              style: TextStyle(color: textColor, fontSize: 14),
                            ),
                            Text(
                              Singleton()
                                  .remoteConfig
                                  .getString('payment_page_gifticon_label'),
                              style: TextStyle(color: textColor, fontSize: 14),
                            )
                          ],
                        ),
                        Divider(color: Colors.grey)
                      ],
                    ));
              } else {
                return Container(
                    padding: EdgeInsets.only(left: 16, right: 16),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Text(
                              reformatDateTime(UTC2LOCAL(
                                  _purchaseHistoryListRaw[index]
                                      ["created_at"])),
                              style: TextStyle(color: textColor, fontSize: 14),
                            ),
                            Text(
                              NumberFormat("#,###").format(
                                _purchaseHistoryListRaw[index]["amount"],
                              ),
                              style: TextStyle(color: textColor, fontSize: 14),
                            )
                          ],
                        ),
                        Divider(color: Colors.grey)
                      ],
                    ));
              }
            },
          )))
    ]);
  }
}

void puchasePage(BuildContext context, UserWidgetState parent) {
  Navigator.push(
      context,
      MaterialPageRoute(
        settings: RouteSettings(name: 'PaymentPage'),
        builder: (BuildContext context) {
          return Scaffold(
            key: paymentPageScaffoldKey,
            appBar: AppBar(
              elevation: 0.0,
              title: Text(
                Singleton().remoteConfig.getString('payment_page_title'),
                style: TextStyle(fontSize: 24, color: Colors.black),
              ),
              centerTitle: true,
              backgroundColor: Colors.white,
              leading: Builder(builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(Icons.arrow_back_ios),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                );
              }),
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Container(child: PaymentWidget(parent)),
          );
        },
      ));
}
