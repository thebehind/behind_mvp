/// 2019.10.09
/// 1. chat message deducts at least 1 sec
///
/// 2019.10.02
/// 1. chat message copy on long click
///
/// 2019.10.01
/// 1. chat status of the other participant - isOnline, isTyping
/// 2. multiline chat input
///
/// 2019.09.30
/// 1. timer sync among users
/// 2. set status of the chatroom STOPPED (2) when time is up
/// 3. if chatroom is STOPPED, system message, ignore input
///
/// 2019.09.29
/// 1. send message via websocket, and handle it with the message handler
/// 2. show init Q/A & intro shit at the beginning of the chat (non-server)
///
/// 2019.09.28
/// 1. show intro of each other
/// 2. add websocket
///   - https://flutter.dev/docs/cookbook/networking/web-sockets
///
/// 2019.09.27
/// 1. check if im the questioner or the answerer
///
/// 2019.09.11
/// ychoi
/// referenced https://medium.com/@peterekeneeze/flutter-build-a-chat-app-2630171f92f2
/// combined with flutter package: bubble
///
/// 2019.09.23
/// new chat room initial values class
///
part of 'main.dart';

final GlobalKey<ScaffoldState> chatScreenScaffoldKey =
    GlobalKey<ScaffoldState>();

class ChatScreenWidget extends StatefulWidget {
  final UserWidgetState parent;
  final NewChatScreenWidgetState subParent;
  final InitQA initQA;

  ChatScreenWidget(this.parent, this.subParent, this.initQA);

  @override
  State createState() => new ChatScreenWidgetState(parent, subParent, initQA);
}

class ChatScreenWidgetState extends State<ChatScreenWidget>
    with WidgetsBindingObserver {
  final UserWidgetState parent;
  final NewChatScreenWidgetState subParent;
  final InitQA initQA;

  ChatScreenWidgetState(this.parent, this.subParent, this.initQA);

  Singleton singleton;
  UserInfo _chatUserInfo;

  AppLifecycleState _appLifecycleState;

  final TextEditingController _chatController = new TextEditingController();
  final List<ChatMessage> _messages = <ChatMessage>[];
  ScrollController _scrollController = new ScrollController();
  bool _shouldScrollToBottom;

  String _myUsername;
  int _myUserId;
  var _chatRoomDetails;

  Timer _chatTimer;
  Duration _timeLeft;
  bool _isTyping;
  Timer _checkLastTimerPing;
  bool _shouldIUpdateTimeLeft;
  bool _isSomebodyOnline;
  bool _isSomebodyTyping;
  Timer _checkSomebodyTypingTimer;

  bool _isChatRoomStopped;
  IOWebSocketChannel _chatRoomChannel;

//  final echoChannel = IOWebSocketChannel.connect('ws://echo.websocket.org');

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    if (!_isChatRoomStopped) {
      _chatRoomChannel.sink.close();
    }
    cancelChatTimer();
    _chatController.dispose();
    super.dispose();
  }

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    _shouldScrollToBottom = false;

    singleton = Singleton();
    _myUsername = singleton.sharedPrefs.getString("user_username");
    _myUserId = singleton.sharedPrefs.getInt("user_id");

    _isTyping = false;
    _shouldIUpdateTimeLeft = true;
    _isChatRoomStopped = false;
    _isSomebodyOnline = false;
    _isSomebodyTyping = false;
    _chatTimer = Timer(Duration(), () => null);
    _checkLastTimerPing = Timer(Duration(), () => null);
    _checkSomebodyTypingTimer = Timer(Duration(), () => null);
    _timeLeft = Duration(minutes: 0);
    _chatController.addListener(deductChatTimer);
    getChatRoomDetailsThenStartListening();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      _appLifecycleState = state;
    });
    switch (_appLifecycleState) {
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        if (!_isChatRoomStopped) {
          _chatRoomChannel.sink.close();
        }
        cancelChatTimer();
        break;
      case AppLifecycleState.resumed:
        _isTyping = false;
        _shouldIUpdateTimeLeft = true;
        _isChatRoomStopped = false;
        _isSomebodyOnline = false;
        _isSomebodyTyping = false;
        _chatTimer = Timer(Duration(), () => null);
        _checkLastTimerPing = Timer(Duration(), () => null);
        _checkSomebodyTypingTimer = Timer(Duration(), () => null);
        _timeLeft = Duration(minutes: 0);
        getChatRoomDetailsThenStartListening();
        break;
      case AppLifecycleState.suspending:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    if (subParent._endChatTrigger && !_isChatRoomStopped) {
      _chatRoomChannel.sink
          .add(jsonEncode({"type": "chat_timer", "time_left": '00:00:00'}));
    }

    return new Column(
      children: <Widget>[
        Divider(color: Colors.black, height: 0),
        Container(
          padding: EdgeInsets.only(left: 16, right: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              stateIcon(),
              Text(
                  '${Singleton().remoteConfig.getString('chat_time_left_label')}: ${durationToMinSecString(_timeLeft)}')
            ],
          ),
        ),
        messageBubbleSection(),
        Divider(
          height: 1.0,
          color: Colors.black,
        ),
        chatInputSection(),
      ],
    );
  }

  void _scrollToBottom() {
    _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
  }

  Widget messageBubbleSection() {
    List<Widget> messageBubbleList = List<Widget>();
    for (int i = 0; i < _messages.length; i++) {
      messageBubbleList.add(GestureDetector(
          onLongPress: () {
            Clipboard.setData(ClipboardData(text: _messages[i].content));
            chatScreenScaffoldKey.currentState.showSnackBar(SnackBar(
                duration: const Duration(seconds: SNACKBAR_DURATION),
                content: Text(Singleton()
                    .remoteConfig
                    .getString('chat_bubble_sb_copied'))));
          },
          child: _messages[i]));
    }

    if (_shouldScrollToBottom) {
      WidgetsBinding.instance.addPostFrameCallback((_) => _scrollToBottom());
      _shouldScrollToBottom = false;
    }
    return Flexible(
        child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: ListView(
                controller: _scrollController,
                padding: new EdgeInsets.all(8.0),
                children: messageBubbleList)));
  }

  Widget chatInputSection() {
    if (_isChatRoomStopped) {
      return Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
          child: Row(
            children: <Widget>[
              Flexible(
                child: new TextField(
                  decoration: new InputDecoration.collapsed(
                      hintText: Singleton()
                          .remoteConfig
                          .getString('chat_room_ended')),
                  maxLines: 3,
                  enabled: false,
                ),
              )
            ],
          ));
    }
    return IconTheme(
      data: new IconThemeData(color: Colors.black),
      child: new Container(
        margin: const EdgeInsets.only(left: 16, right: 16, top: 16),
        child: new Row(
          children: <Widget>[
            new Flexible(
              child: new TextField(
                decoration: new InputDecoration.collapsed(
                    hintText: Singleton()
                        .remoteConfig
                        .getString('chat_input_placeholder')),
                cursorColor: Colors.grey,
                keyboardType: TextInputType.multiline,
                maxLines: 3,
                controller: _chatController,
              ),
            ),
            new Container(
              margin: const EdgeInsets.symmetric(horizontal: 4.0),
              child: new IconButton(
                icon: new Icon(Icons.send),
                onPressed: () {
                  if (_isChatRoomStopped == false) {
                    _chatRoomChannel.sink.add(jsonEncode({
                      "type": "chat_message",
                      "message": _chatController.text
                    }));
                    if (_shouldIUpdateTimeLeft) {
                      if (_timeLeft > Duration(seconds: 0)) {
                        _timeLeft = _timeLeft - Duration(seconds: 1);
                        if (_timeLeft == Duration(seconds: 0)) {
//                          stopChatRoom();
                        }
                        _chatRoomChannel.sink.add(jsonEncode({
                          "type": "chat_timer",
                          "time_left":
                              '00:${twoDigits(_timeLeft.inMinutes.remainder(60))}:${twoDigits(_timeLeft.inSeconds.remainder(60))}'
                        }));
                      } else {
                        stopChatRoom();
                      }
                    }
                  }
                  _chatController.clear();
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget stateIcon() {
    String sombodyUsername = '';
    if (_chatUserInfo != null) {
      sombodyUsername = _chatUserInfo.username;
    }
    if (_isSomebodyTyping) {
      return Row(
        children: <Widget>[
          Text(
              sombodyUsername +
                  Singleton().remoteConfig.getString('chat_is_typing_label'),
              style: TextStyle(color: Colors.black)),
          Icon(
            Icons.edit,
            color: Colors.black,
            size: 16,
          ),
        ],
      );
    } else if (_isSomebodyOnline) {
      return Row(
        children: <Widget>[
          Text(
              sombodyUsername +
                  Singleton().remoteConfig.getString('chat_is_online_label'),
              style: TextStyle(color: Colors.black)),
          Icon(
            Icons.visibility,
            color: Colors.black,
            size: 16,
          ),
        ],
      );
    } else {
      // if (!_isSomebodyOnline)
      return Row(
        children: <Widget>[
          Text(
            sombodyUsername +
                Singleton().remoteConfig.getString('chat_is_offline_label'),
            style: TextStyle(color: Colors.black),
          ),
          Icon(
            Icons.visibility_off,
            color: Colors.black,
            size: 16,
          ),
        ],
      );
    }
  }

  void messageHandler(int talkerId, String text) {
    if (text == '') return;

    String bubbleStyle;
    String talkerUsername;

    if (talkerId == -1) {
      bubbleStyle = "system_message";
    } else if (talkerId == -2) {
      bubbleStyle = "system_gifticon";
    } else if (talkerId == _myUserId) {
      bubbleStyle = "me";
      talkerUsername = _myUsername;
    } else {
      bubbleStyle = "somebody";
      talkerUsername = _chatUserInfo.username;
    }

    String _companyLogoUrl = SERVER_URL + COMPANY_LOGO_API;

    if (initQA.questionerId == _myUserId) {
      _companyLogoUrl =
          _companyLogoUrl + '${initQA.answererCompanyId.toString()}/';
    } else {
      _companyLogoUrl =
          Singleton().remoteConfig.getString('thebehind_logo_url');
    }

    ChatMessage message = ChatMessage(
        bubbleStyle: bubbleStyle,
        content: text,
        chatUsername: talkerUsername,
        companyLogoUrl: _companyLogoUrl);

    setState(() {
      _messages.add(message);
      _shouldScrollToBottom = true;
//      _messages.insert(0, message);
//      _messages.insert(0, message);
    });
  }

  void deductChatTimer() {
    if (_chatController.text == '') {
      _isTyping = false;
      cancelChatTimer();
    } else if (_isTyping == false) {
      _isTyping = true;
      startChatTimer();
    }
  }

  String twoDigits(int n) {
    if (n >= 10) return "$n";
    return "0$n";
  }

  void startChatTimer() {
    _chatTimer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
      if (_shouldIUpdateTimeLeft) {
        if (_timeLeft > Duration(seconds: 0)) {
          _timeLeft = _timeLeft - Duration(seconds: 1);
          if (_timeLeft == Duration(seconds: 0)) {
            stopChatRoom();
          }
          _chatRoomChannel.sink.add(jsonEncode({
            "type": "chat_timer",
            "time_left":
                '00:${twoDigits(_timeLeft.inMinutes.remainder(60))}:${twoDigits(_timeLeft.inSeconds.remainder(60))}'
          }));
        } else {
          timer.cancel();
          stopChatRoom();
        }
      }
      _chatRoomChannel.sink
          .add(jsonEncode({"type": "chat_state", "state": "TYPING"}));
    });
  }

  void cancelChatTimer() {
    _chatTimer.cancel();
  }

  Future<void> stopChatRoom() async {
    parent._queServerTimer(LastServerCall.stopChatRoom);
    cancelChatTimer();

    String url =
        SERVER_URL + CHAT_ROOM_API + initQA.chatRoomId.toString() + '/';
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key')
    };

    String stopChatRoomContent = jsonEncode({
      "state": 2,
    });
    Response response =
        await patch(url, headers: headers, body: stopChatRoomContent)
            .catchError((error) {
      Singleton().noInternetDialog(context).show();
    });

    int statusCode = response.statusCode;
    var data = json.decode(utf8.decode(response.bodyBytes));

    if (statusCode == 200) {
      parent._dequeServerTimer();
//      debugPrint('[chatscreen] stop chat room - OK!');
      getGifticonThenMessage();
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
      parent._dequeServerTimer();
//      debugPrint('[chatscreen] stop chat room - FAIL');
      if (data["purchase"] == 'Already transacted points to answerer.') {
        getGifticonThenMessage();
      }
    }
  }

  void getChatRoomDetailsThenStartListening() async {
    parent._queServerTimer(LastServerCall.enterChatRoom);

    String url =
        SERVER_URL + CHAT_ROOM_API + initQA.chatRoomId.toString() + '/';
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key')
    };
    Response response = await get(
      url,
      headers: headers,
    );

    int statusCode = response.statusCode;
    var data = json.decode(utf8.decode(response.bodyBytes));

    if (statusCode == 200) {
//      debugPrint('[chatscreen] get chatroom info - OK!');
      _chatRoomDetails = data;
      parent._dequeServerTimer();

      _timeLeft = parseDuration(_chatRoomDetails["time_left"]);
      for (var participant in _chatRoomDetails["participants"]) {
        if (participant["user"]["id"] != _myUserId) {
          _chatUserInfo = new UserInfo(
            id: participant["user"]["id"],
            username: participant["user"]["username"],
            seekerIntro: participant["user"]["job_seeker_intro"],
            employeeIntro: participant["user"]["employee_intro"],
          );
          if (initQA.questionerIntro == '') {
            initQA.questionerIntro = _chatUserInfo.seekerIntro;
          }
          break;
        }
      }

      // add init shit
      messageHandler(
          -1,
          Singleton()
              .remoteConfig
              .getString('chat_init_system_message')
              .replaceAll('\\n', '\n'));
      messageHandler(
          initQA.questionerId,
          Singleton()
                  .remoteConfig
                  .getString('chat_seeker_intro_bubble_label')
                  .replaceAll('\\n', '\n') +
              initQA.questionerIntro);
      messageHandler(
          initQA.answererId,
          Singleton()
                  .remoteConfig
                  .getString('chat_employee_intro_bubble_label')
                  .replaceAll('\\n', '\n') +
              initQA.answererIntro);
      messageHandler(initQA.questionerId, initQA.initQ);
      messageHandler(initQA.answererId, initQA.initA);
      // get previous messages
      for (var message in _chatRoomDetails["messages"]) {
        messageHandler(message["user"], message["message"]);
      }
      if (_chatRoomDetails["state"] == 2) {
        messageHandler(
            -1,
            Singleton()
                .remoteConfig
                .getString('chat_end_system_message')
                .replaceAll('\\n', '\n'));
        getGifticonThenMessage();
        setState(() {
          _isChatRoomStopped = true;
        });
      } else {
        String chatChannelURL =
            CHAT_SERVER_URL + initQA.chatRoomId.toString() + '/';

        _chatRoomChannel = IOWebSocketChannel.connect(chatChannelURL, headers: {
          "Authorization":
              'Token ' + singleton.sharedPrefs.getString('user_key')
        });

        // show newly received messages
        _chatRoomChannel.stream.listen((received) {
          Map<String, dynamic> data = jsonDecode(received);
          if (data["message"] != null) {
            messageHandler(data["user_id"], data["message"]);
            _shouldScrollToBottom = true;
          }
          if (data["time_left"] != null) {
            _checkLastTimerPing.cancel();
            _shouldIUpdateTimeLeft = false;
            _checkLastTimerPing =
                Timer.periodic(Duration(seconds: 1), (Timer timer) {
              _shouldIUpdateTimeLeft = true;
            });

            setState(() {
              _timeLeft = parseDuration(data["time_left"]);
              if (_timeLeft == Duration(seconds: 0)) {
                _isChatRoomStopped = true;
                messageHandler(
                    -1,
                    Singleton()
                        .remoteConfig
                        .getString('chat_end_system_message')
                        .replaceAll('\\n', '\n'));
                _chatRoomChannel.sink.close();
                stopChatRoom();
              }
            });
          }
          if (data["state"] != null) {
            if (data["state"] == "CONNECTED") {
              if (data["user_id"] != _myUserId && _isSomebodyOnline != true) {
                setState(() {
                  _isSomebodyOnline = true;
                });
                _chatRoomChannel.sink.add(
                    jsonEncode({"type": "chat_state", "state": "CONNECTED"}));
              }
            } else if (data["state"] == "DISCONNECTED") {
              if (data["user_id"] != _myUserId) {
                setState(() {
                  _isSomebodyOnline = false;
                });
              }
            }
            if (data["state"] == "TYPING") {
              if (data["user_id"] != _myUserId) {
                setState(() {
                  _isSomebodyTyping = true;
                });
                _checkSomebodyTypingTimer.cancel();
                _checkSomebodyTypingTimer = Timer(
                    Duration(
                        seconds: int.parse(Singleton()
                            .remoteConfig
                            .getString('chat_is_typing_buffer_sec'))), () {
                  setState(() {
                    _isSomebodyTyping = false;
                  });
                });
              }
            }
          }
        });
      }
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[chatscreen] get chatroom info - FAILED [${statusCode.toString()}]');
      parent._dequeServerTimer();
    }
  }

  Future<void> getGifticonThenMessage() async {
    if (initQA.answererId != _myUserId) return;

    String url = SERVER_URL + PURCHASES_ALL_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key')
    };
    Response response = await get(
      url,
      headers: headers,
    );

    int statusCode = response.statusCode;
    var data = json.decode(utf8.decode(response.bodyBytes));

    if (statusCode == 200) {
//      debugPrint('[chatscreen] get purchase history to get gifticon - OK!');

      var purchases = data;
      int chatRoomId = _chatRoomDetails["answer"];
      int gifticonId = -1;

      int i = 0;
      for (i = 0; i < (purchases as List<dynamic>).length; i++) {
        if (purchases[i]["item_type"] == "answer") {
          if (purchases[i]["item_id"] == chatRoomId) {
            gifticonId = purchases[i - 1]["item_id"];
            String gifticonUrl =
                SERVER_URL + GIFTICON_API + '${gifticonId.toString()}/';
            messageHandler(-2, gifticonUrl);
            break;
          }
        }
      }
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint('[chatscreen] get purchase history to get gifticon - FAIL');
    }
  }

  String durationToMinSecString(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  Duration parseDuration(String s) {
    int hours = 0;
    int minutes = 0;
    int micros;
    List<String> parts = s.split(':');
    if (parts.length > 2) {
      hours = int.parse(parts[parts.length - 3]);
    }
    if (parts.length > 1) {
      minutes = int.parse(parts[parts.length - 2]);
    }
    micros = (double.parse(parts[parts.length - 1]) * 1000000).round();
    return Duration(hours: hours, minutes: minutes, microseconds: micros);
  }
}

class NewChatScreenWidget extends StatefulWidget {
  final UserWidgetState parent;
  final InitQA initQA;

  NewChatScreenWidget(this.parent, this.initQA);

  @override
  State createState() => new NewChatScreenWidgetState(parent, initQA);
}

class NewChatScreenWidgetState extends State<NewChatScreenWidget> {
  final UserWidgetState parent;
  final InitQA initQA;
  bool _endChatTrigger;
  bool _isChatStopped;

  NewChatScreenWidgetState(this.parent, this.initQA);

  @override
  initState() {
    super.initState();
    _endChatTrigger = false;
    _isChatStopped = true;
    getChatRoomState();
  }

  @override
  Widget build(BuildContext context) {
    Widget titleWidget;
    Widget endChatWidget;

    int userId = Singleton().sharedPrefs.getInt('user_id');
    if (initQA.answererId == userId) {
      titleWidget = Text(
        'behind',
        style: TextStyle(
          fontSize: 28,
          color: Colors.black,
          fontFamily: 'Zurich BdXCn BT',
        ),
      );
    } else {
      titleWidget = Text(
        '${initQA.answererCompany} - ${initQA.answererJob}',
        style: TextStyle(
          fontSize: 16,
          color: Colors.black,
        ),
        overflow: TextOverflow.ellipsis,
      );
    }

    if (initQA.questionerId == userId && !_isChatStopped) {
      endChatWidget = Container(
          padding: EdgeInsets.all(8),
          child: FlatButton(
            child: Text(
                Singleton().remoteConfig.getString('end_chat_dialog_button'),
                style: TextStyle(color: Colors.white)),
            color: Colors.red[300],
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) => EndChatDialog(onSubmit: () {
                        _endChatTrigger = true;
                      }));
            },
          ));
    } else {
      endChatWidget = Container();
    }

    return Scaffold(
        key: chatScreenScaffoldKey,
        appBar: AppBar(
          elevation: 0.0,
          title: titleWidget,
          centerTitle: true,
          backgroundColor: Colors.white,
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
            );
          }),
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            endChatWidget,
          ],
        ),
        body: Container(
            color: Colors.white,
            child: ChatScreenWidget(parent, this, initQA)));
  }

  void getChatRoomState() async {
    parent._queServerTimer(LastServerCall.enterChatRoom);

    String url =
        SERVER_URL + CHAT_ROOM_API + initQA.chatRoomId.toString() + '/';
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + Singleton().sharedPrefs.getString('user_key')
    };
    Response response = await get(
      url,
      headers: headers,
    );

    int statusCode = response.statusCode;
    var data = json.decode(utf8.decode(response.bodyBytes));

    if (statusCode == 200) {
      debugPrint('[chatscreen] get chatroom state - OK!');
      parent._dequeServerTimer();
      setState(() {
        _isChatStopped = data["state"] == 2;
      });
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
      debugPrint(
          '[chatscreen] get chatroom state - FAILED [${statusCode.toString()}]');
      parent._dequeServerTimer();
    }
  }
}

void newChatScreen(
    BuildContext context, UserWidgetState parent, InitQA initQA) {
  Navigator.push(
      context,
      MaterialPageRoute(
          settings: RouteSettings(name: 'ChatPage'),
          builder: (BuildContext context) {
            return NewChatScreenWidget(parent, initQA);
          }));
}

class EndChatDialog extends StatefulWidget {
  final Function onSubmit;

  EndChatDialog({this.onSubmit});

  @override
  EndChatDialogState createState() => EndChatDialogState();
}

class EndChatDialogState extends State<EndChatDialog> {
  bool _didUserAgreeToEndChat;

  @override
  initState() {
    super.initState();
    _didUserAgreeToEndChat = false;
  }

  @override
  Widget build(BuildContext context) {
    Color deleteAccountConfirmButtonColor;

    if (!_didUserAgreeToEndChat) {
      deleteAccountConfirmButtonColor = Colors.grey[300];
    } else {
      deleteAccountConfirmButtonColor = Colors.orange[300];
    }
    return AlertDialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Singleton().remoteConfig.getString('alert_title'),
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32)),
        ],
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(Singleton()
              .remoteConfig
              .getString('end_chat_message')
              .replaceAll('\\n', '\n')),
          Container(height: 16),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Checkbox(
                value: _didUserAgreeToEndChat,
                onChanged: (bool value) {
                  setState(() {
                    _didUserAgreeToEndChat = value;
                  });
                },
              ),
              Text(Singleton().remoteConfig.getString('confirm_end_chat'),
                  style: TextStyle(color: Colors.grey, fontSize: 12)),
            ],
          ),
          Container(height: 16),
          DialogButton(
            child: Text(
              Singleton().remoteConfig.getString('end_chat_button'),
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
            onPressed: () {
              if (_didUserAgreeToEndChat) {
                Navigator.pop(context);
                widget.onSubmit();
              }
            },
            width: 120,
            color: deleteAccountConfirmButtonColor,
          )
        ],
      ),
    );
  }
}
