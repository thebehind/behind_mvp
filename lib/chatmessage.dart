/// 2019.10.03
/// 1. payment button bubble
/// 2. convert to stateful widget for payment button
///
/// 2019.09.27
/// 1. all the nips face the latest direction
///
/// 2019.09.11
/// ychoi
/// referenced https://medium.com/@peterekeneeze/flutter-build-a-chat-app-2630171f92f2
/// combined with flutter package: bubble

part of 'main.dart';

class ChatMessage extends StatefulWidget {
  String bubbleStyle;
  String content;
  String chatUsername;
  String companyLogoUrl;
  var startChatRoom;

  ChatMessage(
      {this.bubbleStyle,
      this.content,
      this.chatUsername,
      this.companyLogoUrl,
      this.startChatRoom});

  @override
  ChatMessageState createState() => ChatMessageState(
      bubbleStyle, content, chatUsername, companyLogoUrl, startChatRoom);
}

class ChatMessageState extends State<ChatMessage> {
  String bubbleStyle;
  String content;
  String chatUsername;
  String companyLogoUrl;
  var startChatRoom;
  String _gifticonUrl;

  ChatMessageState(this.bubbleStyle, this.content, this.chatUsername,
      this.companyLogoUrl, this.startChatRoom);

  String _chatStartButtonText;

  @override
  initState() {
    super.initState();
    _chatStartButtonText =
        Singleton().remoteConfig.getString('chat_start_button');

    _gifticonUrl = '';
    if (bubbleStyle == "system_gifticon") {
      getGifticonImage();
    }
  }

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;
    double _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    double _bubbleElevation = 1 / _pixelRatio;

    switch (bubbleStyle) {
      case "me":
        return myBubble(_screenWidth, _bubbleElevation);
        break;
      case "somebody":
        return somebodyBubble(_screenWidth, _bubbleElevation);
        break;
      case "payment":
        return paymentBubble(_screenWidth, _bubbleElevation);
        break;
      case "system_message":
        return systemMessageBubble(_screenWidth, _bubbleElevation);
      case "system_gifticon":
        return systemGifticonBubble(_screenWidth, _bubbleElevation);
        break;
    }
    return Container();
  }

  Future<void> getGifticonImage() async {
    HttpClient client = new HttpClient();
    client.getUrl(Uri.parse(content)).then((HttpClientRequest request) {
      // Optionally set up headers...
      // Optionally write to the request object...
      // Then call close.
      request.headers.add(HttpHeaders.authorizationHeader,
          'Token ' + Singleton().sharedPrefs.getString('user_key'));
      request.followRedirects = false;
      return request.close();
    }).then((HttpClientResponse response) {
      _gifticonUrl = response.headers["location"].toString();
      setState(() {
        _gifticonUrl = _gifticonUrl.substring(1, _gifticonUrl.length - 1);
      });
    });
  }

  Widget systemGifticonBubble(double screenWidth, double bubbleElevation) {
    String fullname = Singleton().sharedPrefs.getString("user_fullname");
    return Column(
      children: <Widget>[
        Bubble(
          alignment: Alignment.center,
          elevation: bubbleElevation,
          color: Colors.blue[100],
          margin: BubbleEdges.only(top: 4.0),
          nip: BubbleNip.no,
          child: Text(
            '$fullname님, ${Singleton().remoteConfig.getString('gifticon_thank_you_message').replaceAll('\\n', '\n')}',
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 4.0, left: 32, right: 32),
          child: Bubble(
            alignment: Alignment.center,
            elevation: bubbleElevation,
            color: Colors.blue[100],
            margin: BubbleEdges.only(top: 4.0),
            nip: BubbleNip.no,
            child: Container(
              width: screenWidth * 0.9,
              height: screenWidth,
              margin: EdgeInsets.only(top: 4.0, right: 4),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new NetworkImage(_gifticonUrl),
                  fit: BoxFit.fitWidth,
                ),
//            border: new Border.all(
//              color: Colors.black,
//              width: 1.0,
//            ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget systemMessageBubble(double screenWidth, double bubbleElevation) {
    return Container(
      margin: EdgeInsets.only(top: 4.0, left: 32, right: 32),
      width: screenWidth * 0.9,
      child: Bubble(
        alignment: Alignment.center,
        elevation: bubbleElevation,
        color: Colors.blue[100],
        margin: BubbleEdges.only(top: 4.0),
        nip: BubbleNip.no,
        child: Text(
          content,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget myBubble(double screenWidth, double bubbleElevation) {
    return Container(
      margin: EdgeInsets.only(top: 4.0),
      width: screenWidth * 0.9,
      child: Bubble(
        alignment: Alignment.topRight,
        elevation: bubbleElevation,
        color: Colors.yellow[100],
        margin: BubbleEdges.only(left: 50.0),
        nip: BubbleNip.rightBottom,
//        nip: BubbleNip.no,
        child: Text(content),
      ),
    );
  }

  Widget somebodyBubble(double screenWidth, double bubbleElevation) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              width: 32,
              height: 32,
              margin: EdgeInsets.only(top: 4.0, right: 4),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new NetworkImage(companyLogoUrl),
                  fit: BoxFit.contain,
                ),
                shape: BoxShape.circle,
                border: new Border.all(
                  color: Colors.black,
                  width: 1.0,
                ),
              ),
            ),
            Text(chatUsername, style: TextStyle(fontSize: 16)),
          ],
        ),
        Container(
          margin: EdgeInsets.only(top: 4.0, left: 32.0),
          width: screenWidth * 0.9,
          child: Bubble(
            alignment: Alignment.topLeft,
            elevation: bubbleElevation,
            color: Colors.white,
            margin: BubbleEdges.only(right: 50.0),
            nip: BubbleNip.leftTop,
            child: Text(content),
          ),
        )
      ],
    );
  }

  Widget paymentBubble(double screenWidth, double bubbleElevation) {
    Widget startChatButton = Container();
    if (!Singleton().isAppReviewState) {
      startChatButton = Opacity(
          opacity: 0.95,
          child: FlatButton(
            padding: EdgeInsets.all(12.0),
            color: Colors.red[300],
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            child: Text(_chatStartButtonText,
                style: TextStyle(fontSize: 14, color: Colors.white)),
            onPressed: () {
              if (_chatStartButtonText ==
                  Singleton().remoteConfig.getString('chat_start_button')) {
                setState(() {
                  _chatStartButtonText =
                      "${NumberFormat("#,###").format(int.parse(Singleton().remoteConfig.getString('chat_price')))} P /"
                      " ${int.parse(Singleton().remoteConfig.getString('chat_duration_min'))} min";
                });
              } else {
                startChatRoom();
              }
            },
          ));
    }

    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              width: 32,
              height: 32,
              margin: EdgeInsets.only(top: 4.0, right: 4),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new NetworkImage(companyLogoUrl),
                  fit: BoxFit.contain,
                ),
                shape: BoxShape.circle,
                border: new Border.all(
                  color: Colors.black,
                  width: 1.0,
                ),
              ),
            ),
            Text(chatUsername, style: TextStyle(fontSize: 16)),
          ],
        ),
        Container(
          margin: EdgeInsets.only(top: 4.0, left: 32.0),
          width: screenWidth * 0.9,
          height: 64,
          child: Stack(
            children: <Widget>[
              Bubble(
                alignment: Alignment.topLeft,
                elevation: bubbleElevation,
                color: Colors.white,
                margin: BubbleEdges.only(right: 50.0),
                nip: BubbleNip.leftTop,
                child: Text(content),
              ),
              Column(
                children: <Widget>[
                  Container(
                      width: screenWidth * 0.6,
                      margin: EdgeInsets.only(top: 8, left: 22),
                      child: startChatButton),
                ],
              ),
            ],
          ),
        )
      ],
    );
  }
}
