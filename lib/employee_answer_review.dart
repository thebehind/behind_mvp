/// 2019.09.22
/// Youngjun choi
/// this one is accessed by employee
/// this shows the initial question/answer and the current status of this answer
/// is "waiting for the confirmation" from the seeker

part of 'main.dart';

void employeeRespCheckPage(BuildContext context, InitQA initQA) {
  String initQ = initQA.initQ;
  String initA = initQA.initA;

  Widget respCheckSection() {
    return ListView(
      children: <Widget>[
        Container(height: 30),
        Container(
//          decoration: BoxDecoration(
//            border: Border.all(color: Colors.grey),
//            borderRadius: BorderRadius.circular(4.0),
//          ),
          padding: EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Flexible(
                      fit: FlexFit.tight,
                      flex: 2,
                      child: Container(
                        padding: EdgeInsets.only(left: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.all(8.0),
                                child: Text(
                                    Singleton()
                                        .remoteConfig
                                        .getString('employee_a_review_q_label')
                                        .replaceAll('\\n', '\n'),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold))),
                          ],
                        ),
                      )),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                            child: Text(
                                '${Singleton().remoteConfig.getString('employee_a_review_q_req_date')}: ${reformatDateTime(initQA.questionDate)}',
                                style: TextStyle(fontSize: 14))),
                        Container(
                            child: Text(
                                '${Singleton().remoteConfig.getString('employee_a_review_q_req_company')}: ${initQA.targetCompany}'
                                '\n${Singleton().remoteConfig.getString('employee_a_review_q_req_job')}: ${initQA.targetJob}',
                                textAlign: TextAlign.right,
                                style: TextStyle(fontSize: 12))),
                      ],
                    ),
                  ),
                ],
              ),
              Container(height: 8),
              Column(
                children: <Widget>[Text('"$initQ"')],
              )
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(4.0),
          ),
          padding: EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(children: <Widget>[
                Row(
                  children: [
                    Flexible(
                        fit: FlexFit.tight,
                        flex: 3,
                        child: Container(
                          padding: EdgeInsets.only(left: 8),
                          child: Container(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                  Singleton()
                                      .remoteConfig
                                      .getString('employee_a_review_a_label'),
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold))),
                        )),
                    Flexible(
                        fit: FlexFit.tight,
                        flex: 1,
                        child: Container(
                            padding: EdgeInsets.only(left: 8),
                            child: Icon(
                              Icons.rate_review,
                              color: Colors.grey,
                            ))),
                  ],
                ),
              ]),
              Container(height: 8),
              Column(
                children: <Widget>[Text('"$initA"')],
              )
            ],
          ),
        ),
        Container(height: 16),
      ],
    );
  }

  Navigator.push(context, MaterialPageRoute(settings: RouteSettings(name: 'EmployeeAnswerReviewPage'),builder: (BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          title: Text(
            Singleton().remoteConfig.getString('employee_a_review_page_title'),
            style: TextStyle(fontSize: 24, color: Colors.black),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.maybePop(context);
              },
            );
          }),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Container(
            color: Colors.white,
            padding: EdgeInsets.only(left: 32, right: 32),
            child: respCheckSection()));
  }));
}
