/// 2019.10.04
/// 1. show employee(self) intro
/// 2. init A minimum 20 chars
///
/// 2019.09.28
/// 1. show user that their introduction will be shown to the questioner
///
/// 2019.09.19
/// Youngjun choi
///
/// the page where employee answers initially
/// api/v1/answers/ POST - 질문에 대한 답변하기

part of 'main.dart';

const String answerAPI = 'api/v1/answers/';
final GlobalKey<ScaffoldState> employeeInitAScaffoldKey =
    GlobalKey<ScaffoldState>();

void initAnswerPage(
    BuildContext context, UserWidgetState parent, InitQA initQA) {
  TextEditingController initialAnswerController =
      parent.initialAnswerController;

  void _postInitAnswer() async {
    await Singleton().firebaseAnalytics.logEvent(
      name: 'post_init_question',
      parameters: <String, dynamic>{
        'company': Singleton().sharedPrefs.getString("user_company"),
        'job': Singleton().sharedPrefs.getString("user_job"),
      },
    );

    if (initialAnswerController.text == '') {
      return;
    }
    parent._queServerTimer(LastServerCall._postInitAnswer);

    String url = SERVER_URL + answerAPI;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + parent.singleton.sharedPrefs.getString('user_key')
    };

//    String answerInfo =
//        '{"content": "${initialAnswerController.text}", "question_id": "${chatInfo.id}"}';

    String answerInfo = jsonEncode({
      "content": initialAnswerController.text,
      "question_id": initQA.questionId.toString()
    });

    Response response =
        await post(url, headers: headers, body: answerInfo).catchError((error) {
      Singleton().noInternetDialog(context).show();
    });
    int statusCode = response.statusCode;

    if (statusCode == 201) {
//      debugPrint('[employee_init_answer] answer question - OK!');
      initialAnswerController.clear();
      parent._dequeServerTimer();

//      parent._currNavIndex =
//          parent.singleton._sharedPrefs.getInt('user_role') == 2 ? 2 : 1;
      parent._currNavIndex = 2;
      parent._isSeekerChatMode = false;
      parent._getMyAnswers(true);
      Navigator.maybePop(context);
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint('[employee_init_answer] answer question - FAILED');
//      debugPrint("status code: " + statusCode.toString());
      parent._dequeServerTimer();
    }
  }

  Widget initAnswerSection() {
    String employeeIntro =
        Singleton().sharedPrefs.getString("user_employee_intro");
    if (employeeIntro.isEmpty) {
      employeeIntro =
          '(${Singleton().remoteConfig.getString('employee_init_a_no_intro_message')})';
    }
    return Column(
      children: <Widget>[
        Container(
//          decoration: BoxDecoration(
//            border: Border.all(color: Colors.grey),
//            borderRadius: BorderRadius.circular(4.0),
//          ),
          padding: EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                      child: Text(
                          '${Singleton().remoteConfig.getString('employee_init_a_q_date_label')}: ${reformatDateTime(initQA.questionDate)}',
                          style: TextStyle(fontSize: 14))),
                  Container(
                      child: Text(
                          '${Singleton().remoteConfig.getString('employee_init_a_q_company_label')}: ${initQA.targetCompany}'
                          '\n${Singleton().remoteConfig.getString('employee_init_a_q_job_label')}: ${initQA.targetJob}',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 12))),
                ],
              ),
              Container(height: 10),
              Column(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.all(8.0),
                      child: Text('"${initQA.initQ}"',
                          style: TextStyle(fontSize: 16))),
                ],
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(4.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                  Singleton()
                      .remoteConfig
                      .getString('employee_init_a_intro_message'),
                  style: TextStyle(color: Colors.grey)),
              Row(
                children: <Widget>[
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 3,
                    child: Column(
                      children: <Widget>[
                        Bubble(
                            alignment: Alignment.center,
                            color: Colors.yellow[100],
                            margin: BubbleEdges.only(top: 4.0),
                            nip: BubbleNip.rightBottom,
                            child: Text(employeeIntro,
                                style: TextStyle(color: Colors.black))),
                      ],
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        FlatButton(
                            padding: EdgeInsets.only(top: 4, bottom: 4),
                            color: Colors.grey[300],
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10.0)),
                            onPressed: () {
                              parent._currNavIndex = 3;
                              parent._selectedPage = UserMyPageWidget(parent);
                              Navigator.maybePop(context);
                            },
                            child: Text(
                                Singleton()
                                    .remoteConfig
                                    .getString(
                                        'employee_init_a_edit_intro_button')
                                    .replaceAll('\\n', '\n'),
                                style: TextStyle(
                                    fontWeight: FontWeight.w400, fontSize: 12)))
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        TextField(
          controller: initialAnswerController,
          keyboardType: TextInputType.multiline,
          maxLines: 5,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0),
              ),
              hintText: Singleton()
                  .remoteConfig
                  .getString('employee_init_a_content_placeholder')
                  .replaceAll('\\n', '\n'),
              hintStyle: TextStyle(fontSize: 14)),
        ),
        Container(height: 30),
        Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Wrap(
              direction: Axis.horizontal,
              alignment: WrapAlignment.center,
              children: <Widget>[
                Text(
                  Singleton()
                      .remoteConfig
                      .getString('employee_init_a_noti_reward'),
                  style: TextStyle(color: Colors.red[300]),
                )
              ],
            )),
        Container(height: 30),
        Container(
            height: 48,
            padding: EdgeInsets.only(left: 40, right: 40),
            child: FlatButton(
                color: Colors.red[300],
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0)),
                onPressed: () {
                  if (initialAnswerController.text.length <
                      int.parse(Singleton()
                          .remoteConfig
                          .getString('employee_init_a_content_min_length'))) {
                    employeeInitAScaffoldKey.currentState.showSnackBar(SnackBar(
                        content: Text(Singleton().remoteConfig.getString(
                            'employee_init_a_sb_content_min_length'))));
                  } else {
                    Navigator.pop(context);
                    _postInitAnswer();
                  }
                },
                child: Text(
                    Singleton()
                        .remoteConfig
                        .getString('employee_init_a_button'),
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                        fontSize: 16)))),
        Container(height: 30),
      ],
    );
  }

  Navigator.push(
      context,
      MaterialPageRoute(
          settings: RouteSettings(name: 'EmployeeInitAnswerPage'),
          builder: (
            BuildContext context,
          ) {
            return Scaffold(
              key: employeeInitAScaffoldKey,
              appBar: AppBar(
                elevation: 0.0,
                title: Text(
                  Singleton()
                      .remoteConfig
                      .getString('employee_init_a_page_title'),
                  style: TextStyle(fontSize: 24, color: Colors.black),
                ),
                centerTitle: true,
                backgroundColor: Colors.white,
                leading: Builder(builder: (BuildContext context) {
                  return IconButton(
                    icon: const Icon(Icons.arrow_back_ios),
                    onPressed: () {
                      Navigator.maybePop(context);
                    },
                  );
                }),
                iconTheme: IconThemeData(color: Colors.black),
              ),
              body: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: ListView(
                    padding: EdgeInsets.only(left: 32, right: 32),
                    children: <Widget>[initAnswerSection()],
                  )),
            );
          }));
}
