/// 2019.10.09
/// 1. points sync to server
///   - get balance
///   - use points prior to opening a chat room
///
/// 2019.10.04
/// 1. redesign this page to chat room appearance
///
/// 2019.09.22
/// Youngjun choi
/// this one is accessed by seeker
/// on this page the seeker checks the initial answer and employee intro,
/// then confirms to pay to have chat session

part of 'main.dart';

const String newChatRoomAPI = 'api/v1/chat_rooms/';

final GlobalKey<ScaffoldState> seekerRespCheckScaffoldKey =
    GlobalKey<ScaffoldState>();

void seekerAnswerCheckPage(
    BuildContext context, UserWidgetState parent, InitQA initQA) {
  String answerId = initQA.answerId.toString();
  String answererId = initQA.answererId.toString();
  String myId = parent.singleton.sharedPrefs.getInt("user_id").toString();
  int _purchasedChatRoom = 0;
//  String username = Singleton().sharedPrefs.getString("user_username");

  Future<void> deductUserBalanceForChatRoom() async {
    String url = SERVER_URL + PURCHASES_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + Singleton().sharedPrefs.getString('user_key')
    };

    String purchaseInfo =
        jsonEncode({"item_type": "answer", "item_id": initQA.answerId});

    Response response = await post(url, headers: headers, body: purchaseInfo)
        .catchError((error) {
      Singleton().noInternetDialog(context).show();
    });

    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    // if success, store the key
    // using the key, get the user info
    if (statusCode == 201) {
      debugPrint('[seeker_resp_check] deduct user points for chat room - OK!');
      await Singleton().updateUserBalance();
      parent.updateUserBalanceUI();
      _purchasedChatRoom = 1;
    }
    else if(statusCode == 502){
      Singleton().noInternetDialog(context).show();
    }else {
      if (data["purchase"] == "Already paid for chatting with answerer.") {
        debugPrint(
            '[seeker_resp_check] already paid for the chat room - FAILED [${statusCode.toString()}]');
        _purchasedChatRoom = 1;
      }
      else if(data["balance"] == "Not enough points."){
        // double check if we ever get here (we shouldn't! this is checked beforehand)
        _purchasedChatRoom = -1;
      }
      else {
        debugPrint(
            '[seeker_resp_check] deduct user points for chat room - FAILED [${statusCode.toString()}]');
      }
    }
  }

  void startChatRoom() async {
    // don't start chat if no point
    Singleton().updateUserBalance();
    int userBalance = Singleton().sharedPrefs.getInt("user_balance");
    if (userBalance <
        int.parse(Singleton().remoteConfig.getString('chat_price'))) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10.0)),
              title: Text(
                Singleton()
                    .remoteConfig
                    .getString('seeker_no_point_dialog_title'),
                textAlign: TextAlign.center,
              ),
              content: Column(mainAxisSize: MainAxisSize.min, children: [
                Container(
                  child: Text(
                    Singleton()
                        .remoteConfig
                        .getString('seeker_no_point_dialog_content'),
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                Container(height: 20),
                FlatButton(
                    padding: EdgeInsets.all(8),
                    color: Colors.red[300],
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0)),
                    onPressed: () {
                      Navigator.pop(context);
                      puchasePage(context, parent);
                    },
                    child: Text(
                        Singleton()
                            .remoteConfig
                            .getString('seeker_no_point_dialog_button'),
                        style: TextStyle(color: Colors.white, fontSize: 18))),
              ]),
            );
          });
      return;
    }

    // don't start if not paid for
    await deductUserBalanceForChatRoom();
    if (_purchasedChatRoom != 1) {
      String chatRoomFailedDialogMessage =  Singleton()
          .remoteConfig
          .getString('seeker_start_chat_failed_dialog');
      if(_purchasedChatRoom == -1){
        chatRoomFailedDialogMessage =  Singleton()
            .remoteConfig
            .getString('seeker_start_chat_failed_not_enough_points');
      }
      Alert(
        style: AlertStyle(
          animationType: AnimationType.grow,
          animationDuration: const Duration(milliseconds: 500),
          isCloseButton: false,
          alertBorder: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
            side: BorderSide(
              color: Colors.grey,
            ),
          ),
        ),
        context: context,
        title: Singleton().remoteConfig.getString('alert_title'),
        type: AlertType.info,
        desc: chatRoomFailedDialogMessage,
        buttons: [
          DialogButton(
            child: Text(
              Singleton()
                  .remoteConfig
                  .getString('seeker_start_chat_failed_dialog_button'),
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
            width: 120,
            color: Colors.grey,
          )
        ],
      ).show();
      return;
    }

    parent._queServerTimer(LastServerCall.createChatRoom);
    String url = SERVER_URL + newChatRoomAPI;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + parent.singleton.sharedPrefs.getString('user_key')
    };

    String newChatRoomInfo = jsonEncode({
      "participant_ids": [myId, answererId],
      "answer_id": answerId,
      "time_left": "00:20:00"
    });

    Response response = await post(url, headers: headers, body: newChatRoomInfo)
        .catchError((error) {
      Singleton().noInternetDialog(context).show();
    });
    int statusCode = response.statusCode;
    var data = json.decode(utf8.decode(response.bodyBytes));

    if (statusCode == 201) {
      debugPrint('[seeker_resp_check] create a new chatroom - OK!');
      parent._dequeServerTimer();
      parent._getMyQuestions(true);
      initQA.chatRoomId = data["id"];
      parent.exitSeekerRespCheckEnterNewChatroom(context, initQA);
    }
    else if(statusCode == 502){
      Singleton().noInternetDialog(context).show();
    }else {
      debugPrint(
          '[seeker_resp_check] create a new chatroom - FAILED [${statusCode.toString()}]');
      parent._dequeServerTimer();
    }
  }

  Widget seekerAnswerCheckSection() {
    List<ChatMessage> _messages = <ChatMessage>[];
    String _companyLogoUrl = SERVER_URL + COMPANY_LOGO_API;

    String companyLogoUrl =
        _companyLogoUrl = _companyLogoUrl + '${initQA.answererCompanyId.toString()}/';

    _messages.add(ChatMessage(
        bubbleStyle: "me",
        content: initQA.initQ,
        chatUsername: initQA.questionerUsername,
        companyLogoUrl: ""));
    _messages.add(ChatMessage(
        bubbleStyle: "somebody",
        content: initQA.answererIntro,
        chatUsername: initQA.answererUsername,
        companyLogoUrl: companyLogoUrl));
    _messages.add(ChatMessage(
        bubbleStyle: "payment",
        content: initQA.initA,
        chatUsername: initQA.answererUsername,
        companyLogoUrl: companyLogoUrl,
        startChatRoom: startChatRoom));

//    _messages.add(ChatMessage(
//        bubbleStyle: "system",
//        content: Singleton()
//            .remoteConfig
//            .getString('seeker_answer_check_system_message')
//            .replaceAll('\\n', '\n'),
//        chatUsername: "system",
//        companyLogoUrl: companyLogoUrl));

    return ListView(
      padding: new EdgeInsets.all(8.0),
      children: <Widget>[
        for (ChatMessage chatMessage in _messages) chatMessage
      ],
    );
  }

  Navigator.push(context, MaterialPageRoute(settings: RouteSettings(name: 'SeekerAnswerCheckPage'),builder: (BuildContext context) {
    String tabTitle = '${initQA.answererCompany} - ${initQA.answererJob}';

    return Scaffold(
        key: seekerRespCheckScaffoldKey,
        appBar: AppBar(
          elevation: 0.0,
          title: Text(
            tabTitle,
            style: TextStyle(fontSize: 20, color: Colors.black),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.maybePop(context);
              },
            );
          }),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body:
            Container(color: Colors.white, child: seekerAnswerCheckSection()));
  }));
}
