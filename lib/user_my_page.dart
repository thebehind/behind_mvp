/// 2019.10.02
/// 1. notification changed to icon toggle
///
/// 2019.10.01
/// 1. notification setting to shared preference
///
/// 2019.09.28
/// 1. update introductions
///
/// 2019.09.26
/// 1. reduce depth on subcategories
///
/// 2019.09.11
/// 1. password change
/// 2. sign out
///
/// 2019.09.10
/// 1. image to fit inside the circle with black border and white bg
/// 2. username change implemented
///
/// 2019.09.07
/// 1. account page
/// 2. Notification page
/// 3. Payment page
///
/// 2019.09.06
/// ychoi
/// separated from "user.dart"
/// 1. help page
/// 2. about page

part of 'main.dart';

const String USER_INFO_API = 'api/v1/users/user/';
const String PASSWORD_CHANGE_API = 'api/v1/users/password/change/';
const String PUSH_NOTIFICATION_API = 'api/v1/users/push-notification-settings/';

final alphanumeric = RegExp(r'^[a-zA-Z0-9]+$');

class UserMyPageWidget extends StatefulWidget {
  final UserWidgetState parent;
  UserMyPageWidget(this.parent);

  @override
  _UserMyPageWidgetState createState() => _UserMyPageWidgetState(parent);
}

class _UserMyPageWidgetState extends State<UserMyPageWidget> {
  final UserWidgetState parent;
  _UserMyPageWidgetState(this.parent);
  Singleton singleton;
  String _userUsername;
  bool _seekerIntroEditEnabled;
  bool _employeeIntroEditEnabled;
  FocusNode _seekerIntroFocusNode;
  FocusNode _employeeIntroFocusNode;

  bool _notificationSwitchOn;
  IconButton _notificationIconButton;

  int _point;

  TextEditingController newUsernameController = new TextEditingController();
  TextEditingController currPasswordController = new TextEditingController();
  TextEditingController newPassword1Controller = new TextEditingController();
  TextEditingController newPassword2Controller = new TextEditingController();
  TextEditingController seekerIntroController = new TextEditingController();
  TextEditingController employeeIntroController = new TextEditingController();

  @override
  initState() {
    super.initState();
    singleton = Singleton();
    _userUsername = singleton.sharedPrefs.getString('user_username');
    seekerIntroController.text =
        singleton.sharedPrefs.getString('user_seeker_intro');
    employeeIntroController.text =
        singleton.sharedPrefs.getString('user_employee_intro');
    _seekerIntroEditEnabled = false;
    _employeeIntroEditEnabled = false;

    _notificationSwitchOn = true;
    getPushNotificationSettings();
    _notificationIconButton = IconButton(
      icon: Icon(
        Icons.notifications,
        color: Colors.blue[300],
      ),
      onPressed: () {
        setState(() {
          _notificationSwitchOn = !_notificationSwitchOn;
        });
      },
    );

    if (singleton.sharedPrefs.getInt('user_balance') != null) {
      Singleton().updateUserBalance();
      _point = singleton.sharedPrefs.getInt('user_balance');
    } else {
      _point = 0;
      singleton.sharedPrefs.setInt('user_balance', _point);
    }

    _seekerIntroFocusNode = FocusNode();
    _employeeIntroFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    _seekerIntroFocusNode.dispose();
    _employeeIntroFocusNode.dispose();

    newUsernameController.dispose();
    currPasswordController.dispose();
    newPassword1Controller.dispose();
    newPassword2Controller.dispose();
    seekerIntroController.dispose();
    employeeIntroController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.only(left: 36, right: 36),
      children: <Widget>[
        Container(height: 16),
        OutlineButton(
            padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
            borderSide: BorderSide(color: Colors.blue[300]),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    settings: RouteSettings(name: 'InvitationPage'),
                    builder: (context) => InvitationWidget()),
              );
            },
            child: Text(
                Singleton().remoteConfig.getString('invite_to_behind_button'),
                style: TextStyle(color: Colors.blue[300], fontSize: 16))),
        Container(height: 4),
        accountSection(),
        Container(height: 4),
        notificationSection(),
        Container(height: 4),
        purchaseSection(),
        seekerIntroSection(),
        employeeIntroSection(),
        Container(height: 32),
        Container(
          padding: EdgeInsets.only(left: 8, right: 8),
          child: Row(
            children: <Widget>[
              Flexible(fit: FlexFit.tight, flex: 3, child: helpDialog()),
              Flexible(fit: FlexFit.tight, flex: 1, child: Container()),
              Flexible(fit: FlexFit.tight, flex: 3, child: aboutDialog()),
            ],
          ),
        ),
        Container(height: 16),
        FlatButton(
            padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
            color: Colors.red[300],
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            onPressed: () {
              onSignOut();
            },
            child: Text(
                Singleton().remoteConfig.getString('mypage_logout_button'),
                style: TextStyle(color: Colors.white, fontSize: 16))),
        Container(height: 16),
        FlatButton(
            padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
            color: Colors.orange[300],
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) =>
                      DeleteAccountDialog(onSubmit: deleteUser));
            },
            child: Text(
                Singleton()
                    .remoteConfig
                    .getString('mypage_delete_account_button'),
                style: TextStyle(color: Colors.white, fontSize: 16))),
        Container(height: 24)
      ],
    );
  }

  Future<void> passwordChange() async {
    // check if the user is a moron
    if (currPasswordController.text == '') {
      userScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(Singleton()
              .remoteConfig
              .getString('mypage_pw_change_sb_check_pw'))));
      currPasswordController.clear();
      newPassword1Controller.clear();
      newPassword2Controller.clear();

      return;
    }
    if (newPassword1Controller.text == '' ||
        newPassword2Controller.text == '' ||
        newPassword1Controller.text != newPassword2Controller.text) {
      userScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(Singleton()
              .remoteConfig
              .getString('mypage_pw_change_sb_check_new_pw'))));
      currPasswordController.clear();
      newPassword1Controller.clear();
      newPassword2Controller.clear();

      return;
    }
    String url = SERVER_URL + PASSWORD_CHANGE_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key'),
    };

    String newPasswordInfo = jsonEncode({
      "old_password": currPasswordController.text,
      "new_password1": newPassword1Controller.text,
      "new_password2": newPassword2Controller.text
    });

    Response response =
        await post(url, headers: headers, body: newPasswordInfo);
    // check if server is online * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // check the status code for the result
    int statusCode = response.statusCode;
    var data = json.decode(response.body);

    currPasswordController.clear();
    newPassword1Controller.clear();
    newPassword2Controller.clear();

    if (statusCode == 200) {
//      debugPrint('[user_my_page] passwoord change - OK!');
      userScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: 1),
          content: Text(
              Singleton().remoteConfig.getString('mypage_pw_change_sb_ok'))));
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[user_my_page] passwoord change - FAILED [${statusCode.toString()}]');
      if (data['new_password2'] != null) {
        if (data['new_password2'][0] ==
            'The password is too similar to the email.') {
          userScaffoldKey.currentState.showSnackBar(SnackBar(
              duration: Duration(seconds: SNACKBAR_DURATION),
              content: Text(
                  '${Singleton().remoteConfig.getString('mypage_pw_change_sb_failed')}\n'
                  '${Singleton().remoteConfig.getString('mypage_pw_change_sb_simlar_to_email')}')));
        }
        if (data['new_password2'][0] ==
            'The password is too similar to the username.') {
          userScaffoldKey.currentState.showSnackBar(SnackBar(
              duration: Duration(seconds: SNACKBAR_DURATION),
              content: Text(
                  '${Singleton().remoteConfig.getString('mypage_pw_change_sb_failed')}\n'
                  '${Singleton().remoteConfig.getString('mypage_pw_change_sb_similar_to_username')}')));
        }
      }
      userScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(Singleton()
              .remoteConfig
              .getString('mypage_pw_change_sb_failed'))));
    }
  }

  Future<void> _spUpdateUsername(String newUsername) async {
    await singleton.sharedPrefs.setString('user_username', newUsername);
    setState(() {
      _userUsername = newUsername;
    });
  }

  Future<void> onUsernameChangeRequest() async {
    // check if the user is a moron
    if (newUsernameController.text == '' ||
        newUsernameController.text ==
            singleton.sharedPrefs.getString('user_username')) {
      newUsernameController.clear();
//      Navigator.pop(context);
      return;
    }
    if (!alphanumeric.hasMatch(newUsernameController.text)) {
      userScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(Singleton()
              .remoteConfig
              .getString('mypage_username_change_sb_username_alphanumeric'))));
      newUsernameController.clear();
//      Navigator.pop(context);
      return;
    }

    String url = SERVER_URL + USER_INFO_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key'),
    };

    String usernameInfo = jsonEncode({
      "username": newUsernameController.text,
      "new_password2": newPassword2Controller.text
    });
    Response response = await patch(url, headers: headers, body: usernameInfo);

    int statusCode = response.statusCode;
    var data = json.decode(utf8.decode(response.bodyBytes));

    if (statusCode == 200) {
//      debugPrint('[user_my_page] username change - OK!');
      _spUpdateUsername(data["username"]);
      newUsernameController.clear();
      userScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: 1),
          content: Text(Singleton()
              .remoteConfig
              .getString('mypage_username_change_sb_ok'))));
      Navigator.maybePop(context);
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[user_my_page] username change - FAILED ${statusCode.toString()}');
      Navigator.pop(context);
      userScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(Singleton()
              .remoteConfig
              .getString('mypage_username_change_sb_failed'))));
    }
  }

  Future<void> _spUpdateSeekerIntro(String intro) async {
    await singleton.sharedPrefs.setString('user_seeker_intro', intro);
  }

  Future<void> _spUpdateEmployeeIntro(String intro) async {
    await singleton.sharedPrefs.setString('user_employee_intro', intro);
  }

  Future<void> onSeekerIntroChange() async {
    // check if the user is a moron
    if (seekerIntroController.text ==
        singleton.sharedPrefs.getString('seeker_intro')) {
      return;
    }

    String url = SERVER_URL + USER_INFO_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key'),
    };

    String introduction =
        jsonEncode({"job_seeker_intro": seekerIntroController.text});

    Response response = await patch(url, headers: headers, body: introduction);

    int statusCode = response.statusCode;
//    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[user_my_page] seeker intro change - OK!');
      _spUpdateSeekerIntro(seekerIntroController.text);
      userScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: 1),
          content: Text(Singleton()
              .remoteConfig
              .getString('mypage_intro_change_sb_ok'))));
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[user_my_page] seeker intro change - FAILED ${statusCode.toString()}');
      userScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(Singleton()
              .remoteConfig
              .getString('mypage_intro_change_sb_failed'))));
    }
  }

  Future<void> onEmployeeIntroChange() async {
    // check if the user is a moron
    if (employeeIntroController.text ==
        singleton.sharedPrefs.getString('employee_intro')) {
      return;
    }

    String url = SERVER_URL + USER_INFO_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key'),
    };

    String introduction =
        jsonEncode({"employee_intro": employeeIntroController.text});

    Response response = await patch(url, headers: headers, body: introduction);

    int statusCode = response.statusCode;
//    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[user_my_page] employee intro change - OK!');
      _spUpdateEmployeeIntro(employeeIntroController.text);
      userScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: 1),
          content: Text(Singleton()
              .remoteConfig
              .getString('mypage_employee_intro_change_sb_ok'))));
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[user_my_page] employee intro change - FAILED ${statusCode.toString()}');
      userScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(Singleton()
              .remoteConfig
              .getString('mypage_employee_intro_change_sb_failed'))));
    }
  }

  Future<void> onSignOut() async {
    String url = SERVER_URL + SIGN_OUT_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + Singleton().sharedPrefs.getString('user_key'),
    };
    Response response = await post(url, headers: headers);
    int statusCode = response.statusCode;

    if (statusCode == 200) {
//      debugPrint('[user_my_page] sign out - OK!');
      Singleton().sharedPrefs.clear();
      Singleton().reInitAppFunc();
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint('[user_my_page] sign out - FAILED ${statusCode.toString()}');
    }
  }

  Future<void> deleteUser() async {
    String url = SERVER_URL + DELETE_USER_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + Singleton().sharedPrefs.getString('user_key'),
    };
    Response response = await delete(url, headers: headers);
    int statusCode = response.statusCode;

    if (statusCode == 204) {
//      debugPrint('[user_my_page] delete user - OK!');
      Singleton().sharedPrefs.clear();
      Singleton().reInitAppFunc();
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[user_my_page] delete user - FAILED ${statusCode.toString()}');
    }
  }

  Future<void> getPushNotificationSettings() async {
    String url = SERVER_URL + PUSH_NOTIFICATION_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key'),
    };
    Response response = await get(url, headers: headers);
    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[user_my_page] get push notification settings - OK!');
      // this setstate triggers error if the user exits my page before this response is reached
      setState(() {
//      "chat": true,
//      "answered": true,
//      "asked": true
        if (data["chat"] == true) {
          _notificationSwitchOn = true;
        } else {
          _notificationSwitchOn = false;
        }
      });
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[user_my_page] get push notification settings - FAILED ${statusCode.toString()}');
    }
  }

  Future<void> setPushNotificationSettings(bool pushNotificationOn) async {
    String url = SERVER_URL + PUSH_NOTIFICATION_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key'),
    };

    String pushNotificationInfo = jsonEncode({
      "chat": pushNotificationOn,
      "answered": pushNotificationOn,
      "asked": pushNotificationOn
    });

    Response response =
        await patch(url, headers: headers, body: pushNotificationInfo);
    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[user_my_page] set push notification settings - OK!');
//      "chat": true,
//      "answered": true,
//      "asked": true
      setState(() {
        if (data["chat"] == true) {
          _notificationSwitchOn = true;
        } else {
          _notificationSwitchOn = false;
        }
      });
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[user_my_page] set push notification settings - FAILED ${statusCode.toString()}');
    }
  }

  Widget usernameChangeDialogContent(String userUsername) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
            Singleton()
                .remoteConfig
                .getString('mypage_username_change_content'),
            style: TextStyle(color: Colors.grey, fontSize: 14)),
        Row(
          children: [
            Flexible(
              fit: FlexFit.loose,
              flex: 2,
              child: Container(
                  padding: EdgeInsets.only(right: 20),
                  child: TextField(
                    controller: newUsernameController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                        hintText: userUsername,
                        hintStyle: TextStyle(color: Colors.grey)),
                  )),
            ),
            Flexible(
                fit: FlexFit.loose,
                flex: 1,
                child: FlatButton(
                    color: Colors.black12,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0)),
                    onPressed: () {
                      onUsernameChangeRequest();
                      Navigator.pop(context);
                    },
                    child: Text(
                        Singleton()
                            .remoteConfig
                            .getString('mypage_username_change_button'),
                        style: TextStyle(
                            fontWeight: FontWeight.w400, fontSize: 14)))),
          ],
        )
      ],
    );
  }

  Widget passwordChangeDialogContent() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Flexible(
            fit: FlexFit.tight,
            flex: 2,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                    padding: EdgeInsets.only(right: 20),
                    child: TextField(
                      obscureText: true,
                      controller: currPasswordController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: Singleton().remoteConfig.getString(
                              'mypage_pw_change_curr_pw_placeholder')),
                    )),
                Container(
                    padding: EdgeInsets.only(right: 20),
                    child: TextField(
                      obscureText: true,
                      controller: newPassword1Controller,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: Singleton().remoteConfig.getString(
                              'mypage_pw_change_new_pw1_placeholder')),
                    )),
                Container(
                    padding: EdgeInsets.only(right: 20),
                    child: TextField(
                      obscureText: true,
                      controller: newPassword2Controller,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: Singleton().remoteConfig.getString(
                              'mypage_pw_change_new_pw2_placeholder')),
                    )),
              ],
            )),
        Flexible(
            fit: FlexFit.tight,
            flex: 1,
            child: FlatButton(
                color: Colors.black12,
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0)),
                onPressed: () {
                  passwordChange();
                  Navigator.pop(context);
                },
                child: Text(
                    Singleton()
                        .remoteConfig
                        .getString('mypage_pw_change_dialog_button'),
                    style:
                        TextStyle(fontWeight: FontWeight.w400, fontSize: 14)))),
      ],
    );
  }

  Widget accountSection() {
    String companyLogoUrl;
    String userCompany = singleton.sharedPrefs.getString('user_company');
    int userCompanyId = singleton.sharedPrefs.getInt('user_company_id');
    String userJob = singleton.sharedPrefs.getString('user_job');
    String userEmail = singleton.sharedPrefs.getString('user_email');
    String userFullname = singleton.sharedPrefs.getString('user_fullname');
    String _companyLogoUrl = SERVER_URL + COMPANY_LOGO_API;
    _companyLogoUrl = _companyLogoUrl + '${userCompanyId.toString()}/';
    companyLogoUrl = _companyLogoUrl;

    Widget companyInfo() {
      if (singleton.sharedPrefs.getInt("user_role") == 1) return Container();
      return Row(
        children: <Widget>[
          Flexible(
            fit: FlexFit.tight,
            flex: 1,
            child: new Container(
              width: 128,
              height: 128,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new NetworkImage(companyLogoUrl),
                  fit: BoxFit.contain,
                ),
                shape: BoxShape.circle,
                border: new Border.all(
                  color: Colors.black,
                  width: 1.0,
                ),
              ),
            ),
          ),
          Flexible(
              fit: FlexFit.tight,
              flex: 2,
              child: Column(
                children: <Widget>[
                  Text(
                    userCompany,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Container(height: 5),
                  Text(
                    userJob,
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              )),
        ],
      );
    }

    return Column(children: [
      companyInfo(),
      Container(height: 16),
      Row(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                Singleton().remoteConfig.getString('mypage_fullname_label'),
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              Container(height: 4),
              Text(
                userFullname,
                style: TextStyle(fontSize: 16),
              ),
            ],
          ),
        ],
      ),
      Container(height: 8),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Flexible(
            fit: FlexFit.loose,
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  Singleton().remoteConfig.getString('mypage_email_label'),
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Container(height: 4),
                Text(
                  userEmail,
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
          ),
          Flexible(
              fit: FlexFit.loose,
              flex: 1,
              child: FlatButton(
                  color: Colors.black12,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0)),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10.0)),
                            title: Text(
                                Singleton()
                                    .remoteConfig
                                    .getString('mypage_pw_change_dialog_title'),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                            content: passwordChangeDialogContent(),
                          );
                        });
                  },
                  child: Text(
                      Singleton()
                          .remoteConfig
                          .getString('mypage_pw_change_button')
                          .replaceAll('\\n', '\n'),
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 14)))),
        ],
      ),
      Container(height: 8),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Flexible(
            fit: FlexFit.loose,
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  Singleton().remoteConfig.getString('mypage_username_label'),
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Container(height: 4),
                Text(
                  _userUsername,
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
          ),
          Flexible(
              fit: FlexFit.loose,
              flex: 1,
              child: FlatButton(
                  color: Colors.black12,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0)),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10.0)),
                            title: Text(
                                Singleton().remoteConfig.getString(
                                    'mypage_username_change_dialog_title'),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                            content: usernameChangeDialogContent(_userUsername),
                          );
                        });
                  },
                  child: Text(
                      Singleton()
                          .remoteConfig
                          .getString('mypage_username_change_dialog_button')
                          .replaceAll('\\n', '\n'),
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 14)))),
        ],
      ),
    ]);
  }

  Widget seekerIntroSection() {
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            Singleton().remoteConfig.getString('mypage_intro_label'),
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Visibility(
            maintainSize: false,
            child: FlatButton(
                color: Colors.red[300],
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0)),
                onPressed: () {
                  onSeekerIntroChange();
                  setState(() {
                    _seekerIntroEditEnabled = !_seekerIntroEditEnabled;
                  });
                },
                child: Text(
                    Singleton().remoteConfig.getString('mypage_intro_save'),
                    style: TextStyle(color: Colors.white))),
            visible: _seekerIntroEditEnabled,
          ),
          Visibility(
            maintainSize: false,
            child: IconButton(
                onPressed: () {
                  setState(() {
                    _seekerIntroEditEnabled = !_seekerIntroEditEnabled;
                  });
                  FocusScope.of(context).requestFocus(_seekerIntroFocusNode);
                },
                icon: Icon(Icons.edit)),
            visible: !_seekerIntroEditEnabled,
          )
        ],
      ),
      Container(height: 4),
      TextField(
        controller: seekerIntroController,
        keyboardType: TextInputType.multiline,
        enabled: _seekerIntroEditEnabled,
        focusNode: _seekerIntroFocusNode,
        maxLines: 5,
        decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
            ),
            hintText: Singleton()
                .remoteConfig
                .getString('mypage_intro_placeholder')
                .replaceAll('\\n', '\n'),
            hintStyle: TextStyle(fontSize: 14)),
      ),
    ]);
  }

  Widget employeeIntroSection() {
    if (Singleton().sharedPrefs.getInt("user_role") == 1) {
      return Container();
    }
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            Singleton().remoteConfig.getString('mypage_employee_intro_label'),
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Visibility(
            maintainSize: false,
            child: FlatButton(
                color: Colors.red[300],
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0)),
                onPressed: () {
                  onEmployeeIntroChange();
                  setState(() {
                    _employeeIntroEditEnabled = !_employeeIntroEditEnabled;
                  });
                },
                child: Text(
                    Singleton()
                        .remoteConfig
                        .getString('mypage_employee_intro_save'),
                    style: TextStyle(color: Colors.white))),
            visible: _employeeIntroEditEnabled,
          ),
          Visibility(
            maintainSize: false,
            child: IconButton(
                onPressed: () {
                  setState(() {
                    _employeeIntroEditEnabled = !_employeeIntroEditEnabled;
                  });
                  FocusScope.of(context).requestFocus(_employeeIntroFocusNode);
                },
                icon: Icon(Icons.edit)),
            visible: !_employeeIntroEditEnabled,
          )
        ],
      ),
      Container(height: 4),
      TextField(
        controller: employeeIntroController,
        keyboardType: TextInputType.multiline,
        enabled: _employeeIntroEditEnabled,
        focusNode: _employeeIntroFocusNode,
        maxLines: 5,
        decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
            ),
            hintText: Singleton()
                .remoteConfig
                .getString('mypage_employee_intro_placeholder')
                .replaceAll('\\n', '\n'),
            hintStyle: TextStyle(fontSize: 14)),
      ),
    ]);
  }

  Widget notificationSection() {
    Icon notificationIcon = Icon(Icons.notification_important);

    if (_notificationSwitchOn == false) {
      notificationIcon = Icon(Icons.notifications_off, color: Colors.grey);
    } else {
      notificationIcon = Icon(
        Icons.notifications,
        color: Colors.blue[300],
      );
    }

    _notificationIconButton = IconButton(
      icon: notificationIcon,
      onPressed: () {
        setState(() {
          setPushNotificationSettings(!_notificationSwitchOn);
        });
      },
    );

    return Column(children: [
      Container(
        child: Row(children: [
          Flexible(
            fit: FlexFit.tight,
            flex: 3,
            child: Text(
              Singleton().remoteConfig.getString('mypage_noti_label'),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Flexible(fit: FlexFit.tight, flex: 1, child: _notificationIconButton),
        ]),
      )
    ]);
  }

  Widget purchaseSection() {
    if (Singleton().isAppReviewState) {
      return Container();
    }
    return Row(
      children: <Widget>[
        Flexible(
          fit: FlexFit.tight,
          flex: 3,
          child: Container(
              padding: EdgeInsets.all(3),
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                border: new Border.all(
                  color: Colors.black,
                  width: 1.0,
                ),
              ),
              child: Text(
                  '${Singleton().remoteConfig.getString('mypage_point_label')}: ' +
                      NumberFormat("#,###").format(_point),
                  style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18))),
        ),
        Flexible(
            fit: FlexFit.loose,
            flex: 1,
            child: FlatButton(
                color: Colors.red[300],
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(2.0)),
                onPressed: () {
                  puchasePage(context, parent);
                },
                child: Text(
                    Singleton().remoteConfig.getString('mypage_point_button'),
                    style: TextStyle(color: Colors.white, fontSize: 16)))),
      ],
    );
  }

  Widget helpDialog() {
    return FlatButton(
        padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
        color: Colors.black12,
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0)),
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0)),
                  title: Text(Singleton()
                      .remoteConfig
                      .getString('mypage_cs_dialog_title')),
                  content: Column(mainAxisSize: MainAxisSize.min, children: [
                    Text(
                      '${Singleton().remoteConfig.getString('support_email')}',
                      style: TextStyle(fontSize: 14),
                    ),
                  ]),
                );
              });
        },
        child: Text(Singleton().remoteConfig.getString('mypage_cs_button'),
            style: TextStyle(color: Colors.black, fontSize: 14)));
  }

  Widget aboutDialog() {
    return FlatButton(
        padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
        color: Colors.black12,
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0)),
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0)),
                  content: Column(mainAxisSize: MainAxisSize.min, children: [
                    Container(height: 40),
                    Container(
                      padding: EdgeInsets.only(left: 40, right: 40),
                      child: Image.asset(
                          'assets/images/behind-logo-whitebg.png',
                          fit: BoxFit.cover),
                    ),
                    Text(
                      'all copyrights reserved',
                      style: TextStyle(fontSize: 12),
                    ),
                    Container(height: 20),
                    Text(
                        '${Singleton().remoteConfig.getString('mypage_info_curr_ver_label')}: ${parent.parent._versionName}'),
                    Text(
                        '${Singleton().remoteConfig.getString('mypage_info_latest_ver_label')}: ${parent.parent._latestVersionName}'),
                    FlatButton(
                        onPressed: () {
                          launchURL(Singleton()
                              .remoteConfig
                              .getString('terms_of_use_url'));
                        },
                        child: Text(
                          Singleton()
                              .remoteConfig
                              .getString('terms_of_use_button'),
                        )),
                    FlatButton(
                        onPressed: () {
                          launchURL(Singleton()
                              .remoteConfig
                              .getString('privacy_policy_url'));
                        },
                        child: Text(Singleton()
                            .remoteConfig
                            .getString('privacy_policy_button'))),
                  ]),
                );
              });
        },
        child: Text(Singleton().remoteConfig.getString('mypage_info_button'),
            style: TextStyle(color: Colors.black, fontSize: 14)));
  }
}

class DeleteAccountDialog extends StatefulWidget {
  final Function onSubmit;

  DeleteAccountDialog({this.onSubmit});

  @override
  DeleteAccountDialogState createState() => DeleteAccountDialogState();
}

class DeleteAccountDialogState extends State<DeleteAccountDialog> {
  bool _didUserAgreeToDeleteAccount;

  @override
  initState() {
    super.initState();
    _didUserAgreeToDeleteAccount = false;
  }

  @override
  Widget build(BuildContext context) {
    Color deleteAccountConfirmButtonColor;

    if (!_didUserAgreeToDeleteAccount) {
      deleteAccountConfirmButtonColor = Colors.grey[300];
    } else {
      deleteAccountConfirmButtonColor = Colors.orange[300];
    }
    return AlertDialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Singleton().remoteConfig.getString('alert_title'),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 32
              )),
        ],
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            Singleton()
                .remoteConfig
                .getString('mypage_delete_account_message')
                .replaceAll('\\n', '\n'),
          ),
          Container(height: 16),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Checkbox(
                value: _didUserAgreeToDeleteAccount,
                onChanged: (bool value) {
                  setState(() {
                    _didUserAgreeToDeleteAccount = value;
                  });
                },
              ),
              Text(Singleton().remoteConfig.getString('confirm_delete_account'),
                  style: TextStyle(color: Colors.grey, fontSize: 12)),
            ],
          ),
          Container(height: 16),
          DialogButton(
            child: Text(
              Singleton()
                  .remoteConfig
                  .getString('mypage_delete_account_button'),
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
            onPressed: () {
              if (_didUserAgreeToDeleteAccount) {
                Navigator.pop(context);
                widget.onSubmit();
              }
            },
            width: 120,
            color: deleteAccountConfirmButtonColor,
          )
        ],
      ),
    );
  }
}
