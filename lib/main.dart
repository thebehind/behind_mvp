/// 2019.10.03
/// 1. firebase messaging (notification)
///
/// 2019.10.02
/// 1. firebase analytics
///
/// 2019.09.14
/// auto-login logic moved to sign_in_up.dart
///
/// 2019.09.12
/// singleton bomb - no more parent.parent.paren.tenroaento
///
/// 2019.09.09
/// /android/app/src/main/AndroidManifest.xml modified
///   - <uses-permission android:name="android.permission.INTERNET" /> added
///
/// 2019.09.08
/// 1. user info class - UserInfo
///   - currently a global variable
///     - implement communication with server first!
///
/// 2019.09.06
/// enum name changed "userRole" -> "userMode"
/// both seeker & helper navigates to UserWidget of "user.dart"
///
/// 2019.09.03
/// - clean up Dart Analysis warnings
///   - @immutable | final ~ stuff, lowerCamelCase Stuff)
/// - move separate classes into stateful widget as widgets
/// - splash screen effect
///   - switch widget with timer at init
/// - screen switch sign-in to main
/// - sign-in/sign-up shit moved to sign_in_up.dart
/// 6. seeker main page
///
/// I know I can and should factor out the "repetitions" present in all these
/// screens, but don't want to do that for this mvp.
/// This process is to get to know dart/flutter in a dirty way
///
/// I think I will re-write the whole project with better design for V1.0
///
/// 2019.09.02
/// linked to github: https://github.com/teamsalad/behind-mvp.git
/// MVP for behind
/// 3. sign-up screen
///   - radio button widget for seeker/helper: UserRoleRadioButtonWidget
/// 4. Private/Company Mail Check screen
/// 5. sign-up screen 2 for helpers
///   - check company email
///
/// 2019.09.01
/// ychoi
/// MVP for behind
/// 1. splash screen
///   - SplashScreen
/// 2. sign-in screen
///   - SignInScreen

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:package_info/package_info.dart';
import 'package:http/http.dart';
import 'package:flutter/widgets.dart';
import 'package:bubble/bubble.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:web_socket_channel/io.dart';
//import 'package:web_socket_channel/status.dart' as status;
import 'package:intl/intl.dart';
//import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/foundation.dart';
import 'package:version/version.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:timezone/timezone.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_sms/flutter_sms.dart';

part 'constants.dart';
part 'sign_in_up.dart';
part 'user.dart';
part 'user_my_page.dart';
part 'chatscreen.dart';
part 'chatmessage.dart';
part 'employee_init_answer.dart';
part 'seeker_answer_check.dart';
part 'employee_answer_review.dart';
part 'payment_page.dart';
part 'invitation_page.dart';

enum MainAppState { splash, onboarding, signInUp, main }
enum UserMode { unknown, seeker, helper }

// https://stackoverflow.com/questions/12649573/how-do-you-build-a-singleton-in-dart
class Singleton {
  SharedPreferences sharedPrefs;
  RemoteConfig remoteConfig;
  Function reInitAppFunc;
  bool isAppReviewState = false;

  double height;

  FirebaseAnalytics firebaseAnalytics;
  FirebaseAnalyticsObserver firebaseAnalyticsObserver;
  FirebasePerformance _performance;
  FirebaseMessaging _firebaseMessaging;

  Singleton._privateConstructor();

  static final Singleton _instance = Singleton._privateConstructor();

  factory Singleton() {
    return _instance;
  }

  Future<void> updateUserBalance() async {
    String url = SERVER_URL + BALANCE_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + Singleton().sharedPrefs.getString('user_key')
    };

    Response response = await get(url, headers: headers).catchError((error) {
//      print("check app version-----");
//      print(error.toString());
    });

    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    // if success, store the key
    // using the key, get the user info
    if (statusCode == 200) {
//      debugPrint('[SINGLETON] get user points - OK!');
      await Singleton().sharedPrefs.setInt("user_balance", data["balance"]);
    } else {
//      debugPrint(
//          '[SINGLETON] get user points - FAILED [${statusCode.toString()}]');
    }
  }

  bool noInternetDialogOn = false;
  Alert noInternetDialog(BuildContext context) {
    if (noInternetDialogOn) {
      return null;
    }

    noInternetDialogOn = true;
    return Alert(
      style: AlertStyle(
        animationType: AnimationType.grow,
        animationDuration: const Duration(milliseconds: 500),
        isOverlayTapDismiss: false,
        isCloseButton: false,
        alertBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
          side: BorderSide(
            color: Colors.grey,
          ),
        ),
      ),
      context: context,
      title: Singleton().remoteConfig.getString('alert_title'),
      type: AlertType.warning,
      desc: NO_INTERNET_MESSAGE,
      buttons: [
        DialogButton(
          child: Text(
            TRY_APP_RESTART,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            noInternetDialogOn = false;
            reInitAppFunc();
          },
          width: 120,
          color: Colors.orange[300],
        ),
      ],
    );
  }
}

Future<void> launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

void setupTimeZone() async {
  var byteData = await rootBundle.load('assets/timezone/data/2019c.tzf');
  initializeDatabase(byteData.buffer.asUint8List());
}

String UTC2LOCAL(String utc) {
  return DateTime.parse(utc).toLocal().toString();
}

String reformatDateTime(String raw) {
  return raw.substring(0, 10) + ' (' + raw.substring(11, 19) + ') ';
}

class UserInfo {
  String email;
  String fullname;
  String username;
  String company;
  int companyId;
  String job;
  String seekerIntro;
  String employeeIntro;
  int id;
  int role;

  UserInfo(
      {this.email = '', // to be handled by DB
      this.fullname = '',
      this.username = '',
      this.company = 'NA',
      this.companyId = -1,
      this.job = 'NA',
      this.seekerIntro = '',
      this.employeeIntro = '',
      this.id = -1,
      this.role = 0});
}

Future main() async {
  if (!kReleaseMode) {
    SERVER_URL = 'http://dev-api.thebehind.com/';
    CHAT_SERVER_URL = 'ws://dev-api.thebehind.com/ws/v1/chat_rooms/';
  }

  await DotEnv().load('.env');

  setupTimeZone();

  // Set `enableInDevMode` to true to see reports while in debug mode
  // This is only to be used for confirming that reports are being
  // submitted as expected. It is not intended to be used for everyday
  // development.
  Crashlytics.instance.enableInDevMode = true;

  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  runApp(MyApp());

//  runZoned<Future<void>>(() async {
//  runApp(MyApp());
//  }, onError: Crashlytics.instance.recordError);
}

class MyApp extends StatelessWidget {
  Future<RemoteConfig> setupRemoteConfig() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    // Enable developer mode to relax fetch throttling
    remoteConfig.setConfigSettings(RemoteConfigSettings(debugMode: true));
    remoteConfig.setDefaults(remoteConfigDefaults);

    return remoteConfig;
  }

  @override
  Widget build(BuildContext context) {
    Singleton().firebaseAnalytics = FirebaseAnalytics();
    Singleton().firebaseAnalyticsObserver =
        FirebaseAnalyticsObserver(analytics: Singleton().firebaseAnalytics);

    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
      ),
//      debugShowCheckedModeBanner: false,
      navigatorObservers: <NavigatorObserver>[
        Singleton().firebaseAnalyticsObserver
      ],
      initialRoute: 'home',
      title: 'behind',
      home: FutureBuilder<RemoteConfig>(
        future: setupRemoteConfig(),
        builder: (BuildContext context, AsyncSnapshot<RemoteConfig> snapshot) {
          if (snapshot.hasData) {
            Singleton().remoteConfig = snapshot.data;
            return MainApp();
          } else {
            return Container();
          }
        },
      ),
    );
  }
}

class MainApp extends StatefulWidget {
  MainApp({Key key}) : super(key: key);

  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  _MainAppState();

  MainAppState _myAppState;
  UserMode _userMode;
  String _versionName;
  String _latestVersionName;
  Key _appKey = new UniqueKey();

  int _onboardingSlideState;
  Image _onboardingImage1 = Image.asset(
      'assets/images/onboardingSlides/Onboarding1_revised.png',
      fit: BoxFit.contain);
  Image _onboardingImage2 = Image.asset(
      'assets/images/onboardingSlides/Onboarding2_revised.png',
      fit: BoxFit.contain);
  Image _onboardingImage3 = Image.asset(
      'assets/images/onboardingSlides/Onboarding3_revised.png',
      fit: BoxFit.contain);
  Image _splashImage = Image.asset('assets/images/behind-splash-whitebg.png',
      fit: BoxFit.contain);

  @override
  initState() {
    debugPrint("----------[main.dart]----------");
    super.initState();

    Singleton().reInitAppFunc = reInitApp;
    reInitApp();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    precacheImage(_onboardingImage1.image, context);
    precacheImage(_onboardingImage2.image, context);
    precacheImage(_onboardingImage3.image, context);
    precacheImage(_splashImage.image, context);
  }

  @override
  Widget build(BuildContext context) {
    Singleton().height = MediaQuery.of(context).size.height;

    Widget _mainBodyWidget;

    switch (_myAppState) {
      case MainAppState.splash:
        _mainBodyWidget = splashWidget();
        break;
      case MainAppState.onboarding:
        _mainBodyWidget = onboardingSlides();
        break;
      case MainAppState.signInUp:
        _mainBodyWidget = SignInUpWidget(this);
        break;
      case MainAppState.main:
        _mainBodyWidget = UserWidget(this);
        break;
    }

    return Container(
      key: _appKey,
      child: _mainBodyWidget,
    );
  }

  void setMainAppState(MainAppState newState) {
    setState(() {
      _myAppState = newState;
    });
  }

  void setUserMode(UserMode userMode) {
    setState(() {
      _userMode = userMode;
    });
  }

  void runSplashThenStartApp() {
    setState(() {
      _myAppState = MainAppState.splash;
    });

    // run splash screen then switch
    Timer(Duration(seconds: SPLASH_DURATION), () {
      checkAppVersion();
    });
  }

  Future<void> initFirebase() async {
//    debugPrint('[main] firebase remote config - ');
    try {
      // Using default duration to force fetching from remote server.
      await Singleton()
          .remoteConfig
          .fetch(expiration: const Duration(seconds: 0));
      await Singleton().remoteConfig.activateFetched();
    } on FetchThrottledException catch (exception) {
      // Fetch throttled.
//      print('[main] fetch firebase remote config exception\n$exception');
    } catch (exception) {
      Singleton().noInternetDialog(context).show();
//      print('Unable to fetch remote config. Cached or default values will be '
//          'used');
    }

//    debugPrint('[main] firebase messasging - ');
    Singleton()._firebaseMessaging = FirebaseMessaging();

    if (Platform.isIOS) {
      Singleton()._firebaseMessaging.requestNotificationPermissions(
          IosNotificationSettings(sound: true, badge: true, alert: true));
      Singleton()
          ._firebaseMessaging
          .onIosSettingsRegistered
          .listen((IosNotificationSettings settings) {
//        print("[main] ios settings registered: $settings");
      });
    }
    Singleton()._firebaseMessaging.getToken().then((token) {
//      print(token);
    });
    Singleton()._firebaseMessaging.configure(
      // TODO re-direct the app screen accordingly
      // and refresh the list as needed
      onMessage: (Map<String, dynamic> message) async {
//        debugPrint('[main] firebase messaging on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
//        debugPrint('[main] firebase messaging on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
//        debugPrint('[main] firebase messaging on launch $message');
      },
    );

//    debugPrint('[main] firebase performance - ');
    Singleton()._performance = FirebasePerformance.instance;
    await Singleton()._performance.setPerformanceCollectionEnabled(true);
  }

  Future<void> reInitApp() async {
    Navigator.maybePop(context);

    _onboardingSlideState = 1;
    _onboardingImage1 =
        Image.asset('assets/images/onboardingSlides/Onboarding1_revised.png');
    _onboardingImage2 =
        Image.asset('assets/images/onboardingSlides/Onboarding2_revised.png');
    _onboardingImage3 =
        Image.asset('assets/images/onboardingSlides/Onboarding3_revised.png');
    _splashImage = Image.asset('assets/images/behind-splash-whitebg.png');

    _userMode = UserMode.unknown;

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      _versionName = packageInfo.version;
    });

    initFirebase();

    setState(() {
      _appKey = new UniqueKey();
    });
    Singleton().sharedPrefs = await SharedPreferences.getInstance();
    setState(() {
      if (Singleton().sharedPrefs.getString("user_email") == null) {
        _myAppState = MainAppState.onboarding;
      } else {
        runSplashThenStartApp();
      }
    });
  }

  Future<void> launchAppStoreLink() async {
    String url = Singleton().remoteConfig.getString('app_store_link');
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> checkAppVersion() async {
    String url = SERVER_URL + APP_VERSION_API;
    if (Platform.isIOS) url = url + "ios/";
    if (Platform.isAndroid) url = url + "android/";

    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };

    Response response = await get(
      url,
      headers: headers,
    ).catchError((error) {
//      print('check app version-----${error.toString()}');
//      print('url: $url');
      Singleton().noInternetDialog(context).show();
    });

    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[main] get app version - OK!');
      _latestVersionName = data["name"];

      // check if version in review
      if (Version.parse(_versionName) == Version.parse(_latestVersionName)) {
        if (data["release_state"] == 2 || data["release_state"] == 3) {
          Singleton().isAppReviewState = true;
        }
      }

      // check for updates
      if (Version.parse(_versionName) < Version.parse(_latestVersionName)) {
        switch (data["update_state"]) {
          case 1:
            // no dialog
            setState(() {
              _myAppState = MainAppState.signInUp;
            });
            break;
          case 2:
            // soft dialog - suggest update - option to store
            Alert(
              style: AlertStyle(
                animationType: AnimationType.grow,
                animationDuration: const Duration(milliseconds: 500),
                isCloseButton: false,
                alertBorder: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  side: BorderSide(
                    color: Colors.grey,
                  ),
                ),
              ),
              context: context,
              title: Singleton().remoteConfig.getString('alert_title'),
              type: AlertType.success,
              desc: Singleton()
                  .remoteConfig
                  .getString('update_version_dialog_message'),
              buttons: [
                DialogButton(
                  child: Text(
                    Singleton()
                        .remoteConfig
                        .getString('update_version_dialog_button'),
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                    launchAppStoreLink();
                  },
                  width: 120,
                  color: Colors.green[300],
                ),
              ],
            ).show();
            setState(() {
              _myAppState = MainAppState.signInUp;
            });
            break;
          case 3:
            // hard dialog - force update - direct to store
            Alert(
              style: AlertStyle(
                animationType: AnimationType.grow,
                animationDuration: const Duration(milliseconds: 500),
                isOverlayTapDismiss: false,
                isCloseButton: false,
                alertBorder: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  side: BorderSide(
                    color: Colors.grey,
                  ),
                ),
              ),
              context: context,
              title: Singleton().remoteConfig.getString('alert_title'),
              type: AlertType.info,
              desc: Singleton()
                  .remoteConfig
                  .getString('update_version_dialog_message'),
              buttons: [
                DialogButton(
                  child: Text(
                    Singleton()
                        .remoteConfig
                        .getString('update_version_dialog_button'),
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                    launchAppStoreLink();
                  },
                  width: 120,
                  color: Colors.blue[300],
                ),
              ],
            ).show();
            break;
          default:
            setState(() {
              _myAppState = MainAppState.signInUp;
            });

            break;
        }
      } else {
        setState(() {
          _myAppState = MainAppState.signInUp;
        });
      }
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint('[main] get app version - FAILED [${statusCode.toString()}]');
    }
  }

  Widget splashWidget() {
    return Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 80, right: 80),
        child: _splashImage);
  }

  Widget onboardingSlides() {
    Widget onboardingWidget;

    Image onboardingImage;
    switch (_onboardingSlideState) {
      case 1:
        onboardingImage = _onboardingImage1;
        break;
      case 2:
        onboardingImage = _onboardingImage2;
        break;
      case 3:
        onboardingImage = _onboardingImage3;
        break;
    }
    onboardingWidget = GestureDetector(
        onTap: () {
          if (_onboardingSlideState == 3) {
            runSplashThenStartApp();
          }
        },
        onHorizontalDragEnd: (details) {
          if (details.primaryVelocity.round() < -10) {
            if (_onboardingSlideState < 3) {
              setState(() {
                _onboardingSlideState++;
              });
            } else {
              runSplashThenStartApp();
            }
          } else if (details.primaryVelocity.round() > 10) {
            if (_onboardingSlideState > 1) {
              setState(() {
                _onboardingSlideState--;
              });
            }
          }
        },
        child: onboardingImage);

    return Container(color: Colors.white, child: onboardingWidget);
  }
}
