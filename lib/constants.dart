part of 'main.dart';

const int SPLASH_DURATION = 1; // seconds
const int SERVER_TIMEOUT_DURATION = 30; // seconds
const int SNACKBAR_DURATION = 3; // seconds
String SERVER_URL = 'https://api.thebehind.com/';
String CHAT_SERVER_URL = 'wss://api.thebehind.com/ws/v1/chat_rooms/';

const String APP_VERSION_API = 'api/v1/app-version/';
const String NO_INTERNET_MESSAGE = "인터넷 연결을 확인해주세요.";
const String TRY_APP_RESTART = "재시도";
const String LOGIN_API = 'api/v1/users/login/';
const String USER_API = 'api/v1/users/user/';
const String SIGNUP_API = 'api/v1/users/registration/';
const String REGISTER_FCM_API = 'api/v1/devices/';
const String BALANCE_API = 'api/v1/purchases/balance/';
const String PASSWORD_RESET_API = 'api/v1/users/password/reset/';
const String SIGN_OUT_API = 'api/v1/users/logout/';
const String DELETE_USER_API = 'api/v1/users/deactivate/';
const String JOB_HIST_API = 'api/v1/user-job-histories/';
const String JOB_LIST_API = 'api/v1/jobs/';
const String COMPANY_LIST_API = 'api/v1/companies/';
const String QUESTION_API = 'api/v1/questions/';
const String ANSWER_API = 'api/v1/answers/';
const String QUESTION_FEED_API = 'api/v1/questions/feed/';
const String CHAT_ROOM_API = 'api/v1/chat_rooms/';
const String COMPANY_LOGO_API = 'api/v1/objects/images/company-logos/';
const String GIFTICON_API = 'api/v1/objects/images/gifticons/';
const String PURCHASES_API = 'api/v1/purchases/';
const String PURCHASES_ALL_API = 'api/v1/purchases/?cursor=all';
const String TOSS_DEPOSIT_API = 'https://toss.im/transfer-web/linkgen-api/link';

Map<String, dynamic> remoteConfigDefaults = {
  'support_email': 'support@thebehind.com',
  'bank_account_info': 'KB 국민은행\n921601-01-345143\n더비하인드 주식회사',
  'server_timeout_message': '서버와의 통신이 원활하지 않습니다',
  'call_server_again_button': '재시도',
  'app_store_link': 'https://thebehind.com',
  'update_version_dialog_message': '최신 버전으로 업데이트',
  'update_version_dialog_button': '업데이트',
  'login_sb_check_email_pw': '이메일과 비밀번호를 정확히 입력해주세요.',
  'login_sb_verify_email': '이메일을 인증해주세요.',
  'signup_sb_check_fullname': '이름을 입력해 주세요.',
  'signup_sb_check_email_pw': '이메일과 비밀번호를 입력해주세요.',
  'signup_sb_pw_mismatch': '비밀번호가 일치하지 않습니다.\n비밀번호를 정학히 입력해주세요.',
  'signup_sb_pw_min8': '비밀번호는 최소 8자로 입력해주세요.',
  'signup_sb_pw_alphabet_req': '비밀번호에 영문자를 포함해주세요.',
  'signup_sb_email_duplicate': '회원가입에 실패하였습니다.\n이미 가입된 이메일입니다.',
  'signup_sb_failed': '회원가입에 실패하였습니다.\n이메일과 비밀번호를 확인해주세요.',
  'resetpw_sb_check_email': '이메일을 정확히 입력해주세요.',
  'signin_email_placeholder': '계정 (이메일)',
  'signin_pw_placeholder': '비밀번호',
  'signin_button': '로그인',
  'signin_forgot_pw': '비밀번호 찾기',
  'signup_message': '계정이 없으세요?',
  'signup_button': '회원가입',
  'signup_fullname_placeholder': '실명 (노출 되지 않습니다)',
  'signup_email_placeholder': '계정 (개인 이메일)',
  'signup_pw_placeholder': '비밀번호 (영문 + 숫자, 최소 8자)',
  'signup_pw2_placeholder': '비밀번호 확인',
  'signin_message': '계정이 있으신가요?',
  'signup_verify_email_message': '가입하신 이메일로 확인 링크를 보내드렸습니다.\n메일함을 확인해주세요.',
  'resetpw_message': '가입시 입력하신 이메일 주소로 비밀번호 재설정 링크를 보내드립니다.',
  'resetpw_email_paceholder': '계정 (이메일)',
  'resetpw_button': '비밀번호 재설정',
  'signup_employee_email_message': '회사 이메일이 아닌 개인 이메일을 기입해주세요',
  'job_seeker': '구직자',
  'employee': '재직자',
  'signup_comveri_sb_unknown_domain': '축하합니다!\n해당 회사에서의 첫 가입자이십니다.'
      '\n번거로우시겠지만 등록하신 회사이메일을 통해 support@thebehind.com로 회사명과 계정이메일을 기입하여 확인 이메일 발송 부탁드립니다.'
      '\n이용에 불편을끼쳐드려 죄송합니다.',
  'signup_comveri_email_sent_message': '인증 이메일을 보내드렸습니다.'
      '\n등록하신 회사 메일함을 확인해주세요.'
      '\n\n[Behind] 아퀴나스가 결재 올립니다!\n\n해당 이메일 수신 혹은 인증 실패시'
      '\nsupport@thebehind.com로 문의 부탁드립니다',
  'question_company_not_found': '회사명을 한글로 검색 후 리스트에서 선택 해 주시기 바랍니다.',
  'question_company_not_selected': '죄송합니다 회사를 선택해 주시기 바랍니다.',
  'tab_title_my_page': '마이페이지',
  'tab_title_answer': '답변하기',
  'answer_question_to_answer': '답변하기',
  'answer_question_mine': '내질문',
  'answer_question_answered': '답변함',
  'answer_no_question_on_air': '질문이 없습니다.',
  'tab_title_question': '질문하기',
  'question_company_label': '회사',
  'questions_company_placeholder': '회사명을 한글로 검색 후 아래에서 선택해 주세요.',
  'question_job_label': '직무',
  'question_content_label': '질문내용',
  'question_content_placeholder':
      '채팅 전 재직자님께 질문할 내용입니다.\n원하시는 주제, 조건을 간략히 작성해주세요.',
  'question_button': '질문하기',
  'tab_title_chat': '채팅내역',
  'tab_subtitle_questions_button': '질문내역',
  'tab_subtitle_answers_button': '답변내역',
  'chat_no_session_message': '진행중인 채팅세션이 없습니다.',
  'signup_comveri_email_message': '재직중이신 회사 이메일을 인증해주세요.',
  'signup_comveri_email_label': '재직중이신 회사',
  'signup_comveri_email_placeholder': '본인의 회사 이메일',
  'signup_comveri_job': '직무',
  'signup_comveri_sb_check_email': '회사 이메일을 정확히 입력해주세요.',
  'signup_comveri_button': '인증하기',
  'signup_comveri_cs_message': '문의 - support@thebehind.com',
  'chat_start_button': '채팅하기',
  'chat_price': '10000',
  'chat_duration_min': '20',
  'chat_start_confirm_price':
      '${NumberFormat("#,###").format(10000)} P / 20 min',
  'chat_end_system_message': '채팅이 종료되었습니다.\n유익한 세션이 되셨기 바랍니다!',
  'logo_text': 'behind',
  'alert_title': 'behind',
  'chat_is_typing_buffer_sec': '2',
  'chat_time_left_label': '남은시간',
  'chat_bubble_sb_copied': '메세지가 복사되었습니다.',
  'chat_input_placeholder': '메세지를 입력하세요',
  'chat_is_typing_label': ' is typing',
  'chat_is_online_label': ' is online',
  'chat_is_offline_label': ' is offline',
  'chat_init_system_message': '참여자가 글을 작성하는 동안 우측 상단의 시간이 차감됩니다.'
      '\n시간이 전부 소진되면 메세지 전송이 불가하지만, 본 채팅내역은 열람 하실 수 있습니다.',
  'chat_seeker_intro_bubble_label': '[자기소개]\n',
  'chat_employee_intro_bubble_label': '[자기소개]\n',
  'chat_title_as_employee': '답변하기',
  'employee_init_a_no_intro_message': '작성된 재직자 자기소개가 없습니다.',
  'employee_init_a_q_date_label': '요청일',
  'employee_init_a_q_company_label': '요청회사',
  'employee_init_a_q_job_label': '요청직무',
  'employee_init_a_intro_message': '아래의 재직자 자기소개가 답변과 같이 전달됩니다.',
  'employee_init_a_edit_intro_button': '자기소개\n수정하기',
  'employee_init_a_content_placeholder':
      '채팅 전 구직자님께 전달될 답변입니다.\n구직자님께서 채택시 채팅연결이 됩니다.\n(최소 20자)',
  'employee_init_a_content_min_length': '20',
  'employee_init_a_sb_content_min_length': '답변을 20자 이상 입력해주세요.',
  'employee_init_a_button': '답변하기',
  'employee_init_a_page_title': '구직자님의 질문',
  'employee_a_review_q_label': '구직자님의\n질문',
  'employee_a_review_q_req_date': '요청일',
  'employee_a_review_q_req_company': '요청회사',
  'employee_a_review_q_req_job': '요청직무',
  'employee_a_review_a_label': '나의 답변',
  'employee_a_review_page_title': '답변확인',
  'payment_dialog_subtitle': '계좌이체',
  'payment_dialog_content':
      '입금시 확인을 위해 예금주명을 본인명의로 부탁드립니다. 입금 확인 후 포인트 반영에 2 ~ 3분 소요됩니다.',
  'payment_dialog_fullname_label': '등록된 본인명',
  'payment_dialog_notification':
      '서비스 초기 안정화 기간동안 입금 프로세스는 더비하인드 직원분들의 확인을 거쳐야하여'
          '새벽시간대에는 (밤 11시 ~ 아침 8시) 포인트 반영 프로세스가 지연될 수 있습니다.',
  'payment_dialog_sb_info_copied': '더비하인드의 계좌정보가 클립보드로 복사되었습니다.',
  'payment_dialog_info_copy_button': '계좌번호 복사',
  'payment_balance_label': '보유 포인트',
  'payment_dialog_title': '포인트 충전',
  'payment_page_dialog_button': '충전',
  'payment_history': '포인트 이용내역',
  'payment_page_title': '포인트 관리',
  'seeker_no_point_dialog_title': '포인트 충전',
  'seeker_no_point_dialog_content': '포인트가 부족합니다.',
  'seeker_no_point_dialog_button': '충전하기',
  'seeker_start_chat_failed_dialog': '채팅방 생성에 실패하였습니다. 다시 시도해 주세요.',
  'seeker_start_chat_failed_dialog_button': '확인',
  'employee_init_a_noti_reward':
      '답변에 대한 보상은 질문해주신 구직자님께서 답변을 채택 후 채팅완료시에 받게됩니다.',
  'seeker_answer_check_system_message': '재직자님의 소개와 답변이 도착했습니다.'
      '\n채팅을 바로 시작하세요!\n\n채팅 시작시 회원님의 자기소개도 재직자님께 전달됩니다.',
  'mypage_logout_button': '로그아웃',
  'mypage_pw_change_sb_check_pw': '비밀번호를 다시 확인해주세요.',
  'mypage_pw_change_sb_check_new_pw': '새 비밀번호를 확인해주세요.',
  'mypage_pw_change_sb_ok': '비밀번호를 성공적으로 변경하였습니다.',
  'mypage_pw_change_sb_failed': '비밀번호 변경을 실패하였습니다.',
  'mypage_pw_change_sb_simlar_to_email': '비밀번호가 이메일과 너무 비슷합니다.',
  'mypage_pw_change_sb_similar_to_username': '비밀번호가 유저네임과 너무 비슷합니다.',
  'mypage_username_change_sb_username_alphanumeric': '유저네임은 영문과 숫자의 조합만 가능합니다.',
  'mypage_username_change_sb_ok': '유저네임을 성공적으로 변경하였습니다.',
  'mypage_username_change_sb_failed': '비밀번호 변경을 실패하였습니다.',
  'mypage_intro_change_sb_ok': '자기소개를 성공적으로 변경하였습니다.',
  'mypage_intro_change_sb_failed': '자기소개 변경을 실패하였습니다.',
  'mypage_employee_intro_change_sb_ok': '재직자 자기소개를 성공적으로 변경하였습니다.',
  'mypage_employee_intro_change_sb_failed': '재직자 자기소개 변경을 실패하였습니다.',
  'mypage_username_change_content': '유저네임은 영문과 숫자의 조합만 가능합니다.',
  'mypage_username_change_button': '변경',
  'mypage_pw_change_curr_pw_placeholder': '현재 비밀번호',
  'mypage_pw_change_new_pw1_placeholder': '새 비밀번호',
  'mypage_pw_change_new_pw2_placeholder': '새 비밀번호 확인',
  'mypage_pw_change_dialog_button': '변경',
  'mypage_fullname_label': '성명',
  'mypage_email_label': '이메일',
  'mypage_pw_change_dialog_title': '비밀번호 변경',
  'mypage_pw_change_button': '비밀번호\n변경',
  'mypage_username_label': '유저네임',
  'mypage_username_change_dialog_title': '유저네임 변경',
  'mypage_username_change_dialog_button': '유저네임\n변경',
  'mypage_intro_label': '자기소개',
  'mypage_intro_save': '저장하기',
  'mypage_intro_placeholder': '답변채택시 해당 재직자님께 전달될 내용입니다.'
      '\n이력 및 관심분야 등을 미리 알려주시면 보다 수월한 채팅 세션이 될 수 있습니다.',
  'mypage_employee_intro_label': '재직자 자기소개',
  'mypage_employee_intro_save': '저장하기',
  'mypage_employee_intro_placeholder': '답변시 해당 구직자님께 전달될 내용입니다.'
      '\n이력, 직급 및 전문분야 등을 미리 알려주시면 보다 수월한 채팅 세션이 될 수 있습니다.',
  'mypage_noti_label': '알림',
  'mypage_point_label': '포인트',
  'mypage_point_button': '관리',
  'mypage_cs_dialog_title': '고객지원',
  'mypage_cs_button': '고객지원',
  'mypage_info_curr_ver_label': '현재 버전',
  'mypage_info_latest_ver_label': '최신 버전',
  'mypage_info_button': '정보',
  'signin_timeout_dialog_cancel_button': '취소',
  'terms_of_use_url': 'https://thebehind.com/terms-of-use',
  'terms_of_use_button': '이용약관',
  'privacy_policy_url': 'https://thebehind.com/privacy-policy',
  'privacy_policy_button': '개인정보처리방침',
  'thebehind_logo_url':
      'https://api.thebehind.com/api/v1/objects/images/company-logos/2913/',
  'signup_comveri_company_label': '회사명',
  'number_of_companies_to_list': '20',
  'signup_comvery_company_name_placeholder': '회사명을 입력해주세요',
  'signup_comveri_sb_wrong_email': '회사 이메일이 등록된 도메인과 일치하지 않습니다.',
  'signup_comveri_agreement_label': '이용약관과 개인정보처리방침에 동의합니다',
  'mypage_delete_account_button': '회원탈퇴',
  'mypage_delete_account_message':
      '회원탈퇴시 계정 복구가 불가능하며, 보유하신 포인트는 전부 소멸됩니다.\n그래도 회원탈퇴를 하시겠습니까?',
  'gifticon_thank_you_message': '수고많으셨습니다.\n스타벅스 기프티콘 보내드립니다!',
  'end_chat_message':
      '채팅을 정말 종료하시겠습니까?\n채팅 종료시 남은시간이 전부 소진되며, 재직자님께 보상이 전달됩니다.',
  'end_chat_dialog_button': '종료',
  'end_chat_button': '채팅종료',
  'end_chat_cancel_dialog_button': '취소',
  'payment_page_gifticon_label': '기프티콘',
  'payment_page_gifticon_price': '4600',
  'toss_deposit_button': '토스로 입금',
  'toss_deposit_fail_dialog_button': '확인',
  'seeker_start_chat_failed_not_enough_points': '포인트가 부족하여 채팅방 생성에 실패하였습니다.',
  'invite_to_behind_button': '비하인드로 초대',
  'invite_to_behind_page_title': '비하인드 초대',
  'invite_message':'비하인드 어플 통해서 취뽀 쌉가능, ㅎㅇㅌ!',
  'link_to_play_store':'bit.ly/thebehind',
  'link_to_app_store':'thebehind.com',
  'invite_individual_sms_button':'초대하기',
  'invite_mms_button':'선택 일괄초대',
  'question_jobs_only':'직무만 선택하기',
  'question_search_company':'회사 검색하기',
  'confirm_delete_account':'네, 회원탈퇴를 하겠습니다.',
  'confirm_end_chat':'네, 채팅을 종료하겠습니다.',

  'chat_room_ended':'채팅이 종료되었습니다.',
};
