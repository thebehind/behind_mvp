/// 2019.10.03
/// 1. device authorization to Robin server when new login
///
/// 2019.09.25
/// 1. when calling API server, store the latest action in _queServerTimer()
///   - so when timed out we know which API call to "retry"
///
/// 2019.09.24
/// 1. enter to the main page directly after splash screen if auto login
///
/// 2019.09.17
/// 1. server reqeust timer - timeout
///
/// 2019.09.15
/// employee sign up shit moved out to user.dart file because it is handled within main user page now
/// model spinner to wait for server
///   - referenced: https://codingwithjoe.com/flutter-how-to-build-a-modal-progress-indicator/
///
/// 2019.09.14
/// auto-login stuff handled here
/// if user role is 1, enter the main page
/// if user role is 2, check company verification status first
///
/// 2019.09.09
/// 1. sign-in/sign-up process feedback to user via snackbar
/// 2. auto-login process
///   use package:shared_preferences
///
/// 2019.09.08
/// 1. communicate with server /user/
/// 2. communicate with server /registration/
///
/// 2019.09.07
/// 1. communicate with server /login/
///
/// 2019.09.05
///  1. add for got email/password sections
/// 2019.09.03
/// ychoi
/// Sign-in, sign-up shit separated from main.dart
/// 1. sign-in screen
/// 2. sign-up screen
/// 3. private email check message screen
/// 4. sign-up for company email screen
/// 5. company email check message screen
///
/// - now can read user input (textfield) into system via text editing
/// controller and pass the value around other widgets
///

part of 'main.dart';

// following to moved to main.dart
// to pass around the values among different widgets
//TextEditingController emailController = new TextEditingController();
//TextEditingController passwordController = new TextEditingController();

enum SignInUpScreenState {
  init,
  signIn,
  signUp,
  checkPrivateEmail,
//  signUpForHelper,
//  checkCompanyEmail,
  forgotPassword
}

enum LastServerCall {
  // queued from sign_in_up
  getLoginToken,
  getUserInfo,
  signOut,
  trySignUp,
  registerFcmDevice,
  resetPassword,

  // queued from user
  _getJobList,
  _getCompanyList,
  sendCompanyEmailVerification,
  _checkUserCompanyInfo,
  _postNewQuestion,
  _getMyQuestions,
  _getMyAnswers,
  _getQuestionsOnAir,

  _postInitAnswer,
  createChatRoom,
  enterChatRoom,
  stopChatRoom,
}

final GlobalKey<ScaffoldState> signInScaffoldKey = GlobalKey<ScaffoldState>();

int signUpRole = 1;

class SignInUpWidget extends StatefulWidget {
  final _MainAppState parent;
  SignInUpWidget(this.parent);

  @override
  _SignInUpWidgetState createState() => _SignInUpWidgetState(parent);
}

class _SignInUpWidgetState extends State<SignInUpWidget> {
  final _MainAppState parent;
  _SignInUpWidgetState(this.parent);
  SignInUpScreenState _signInUpScreenState;
  bool _waitQue;
  Timer _serverTimer;
  bool _serverTimedOut;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _didUserAgree;
  Color _signUpButtonColor;

// following two set in sign_in_up.dart
  TextEditingController signinEmailController = new TextEditingController();
  TextEditingController signinPasswordController = new TextEditingController();
  TextEditingController fullNameController = new TextEditingController();
  TextEditingController signupEmailController = new TextEditingController();
  TextEditingController signupPassword1Controller = new TextEditingController();
  TextEditingController signupPassword2Controller = new TextEditingController();
  TextEditingController resetPasswordEmailController =
      new TextEditingController();

  @override
  void dispose() {
    BackButtonInterceptor.remove(backButtonInterceptor);
    // Clean up the controller when the widget is removed from the
    // widget tree.
    fullNameController.dispose();
    signinEmailController.dispose();
    signinPasswordController.dispose();
    signupEmailController.dispose();
    signupPassword1Controller.dispose();
    signupPassword2Controller.dispose();
    resetPasswordEmailController.dispose();
    super.dispose();
  }

  @override
  initState() {
    debugPrint("----------[sign_in_up.dart]----------");
    super.initState();
    BackButtonInterceptor.add(backButtonInterceptor);
    _signInUpScreenState = SignInUpScreenState.init;
    _waitQue = false;
    _serverTimedOut = false;
    _didUserAgree = false;
    _signUpButtonColor = Colors.grey[300];

    if (Singleton().sharedPrefs != null) {
      signinEmailController.text =
          Singleton().sharedPrefs.getString('user_email');
      if (_serverTimedOut == false) {
        if (Singleton().sharedPrefs.getString('user_key') != null) {
          if (Singleton().sharedPrefs.getString('user_key') != '') {
            getUserInfo();
          }
        } else {
          _signInUpScreenState = SignInUpScreenState.signUp;
        }
      }
    }
  }

  List<Widget> _buildForm(BuildContext context) {
    Widget _returnObject;
    switch (_signInUpScreenState) {
      case SignInUpScreenState.init:
        _returnObject = Column(
          children: <Widget>[
            Container(
              height: 64,
            ),
          ],
        );

        break;
      case SignInUpScreenState.signIn:
        _returnObject = signInSection();
        break;
      case SignInUpScreenState.signUp:
        _returnObject = signUpSection();
        break;
      case SignInUpScreenState.checkPrivateEmail:
        _returnObject = checkPrivateEmailSection();
        break;
      case SignInUpScreenState.forgotPassword:
        _returnObject = forgotPasswordSection();
        break;
    }

    Container container = Container(
        color: Colors.white,
        child: ListView(children: [
          Container(
            height: 16,
          ),
          Container(
            padding: EdgeInsets.only(left: 80, right: 80),
            child: Image.asset('assets/images/behind-logo-whitebg.png',
                fit: BoxFit.cover),
          ),
          _returnObject
        ]));

    var l = new List<Widget>();
    l.add(container);
    if (_waitQue) {
      var modal = new Stack(
        children: [
          new Opacity(
            opacity: 0.3,
            child: const ModalBarrier(dismissible: false, color: Colors.grey),
          ),
          new Center(
            child: new CircularProgressIndicator(),
          ),
        ],
      );
      l.add(modal);
    }
    return l;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: signInScaffoldKey,
      body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Stack(children: _buildForm(context))),
    );
  }

  bool backButtonInterceptor(bool stopDefaultButtonEvent) {
//    print("BACK BUTTON!"); // Do some stuff.
    Navigator.maybePop(context);
    return true;
  }

  void _queServerTimer(LastServerCall lastServerCall) {
    if (_serverTimer != null) _serverTimer.cancel();
    setState(() {
      _waitQue = true;
    });
    _serverTimer = Timer(Duration(seconds: SERVER_TIMEOUT_DURATION), () {
//      debugPrint('[sign in up] server timed out while $lastServerCall');
      setState(() {
        _waitQue = false;
        _serverTimedOut = true;
      });
      Alert(
        style: AlertStyle(
          animationType: AnimationType.grow,
          animationDuration: const Duration(milliseconds: 500),
          isCloseButton: false,
          alertBorder: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
            side: BorderSide(
              color: Colors.grey,
            ),
          ),
        ),
        context: context,
        title: Singleton().remoteConfig.getString('alert_title'),
        type: AlertType.info,
        desc: Singleton().remoteConfig.getString('server_timeout_message'),
        buttons: [
          DialogButton(
            child: Text(
              Singleton().remoteConfig.getString('call_server_again_button'),
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
            onPressed: () {
              Singleton().reInitAppFunc();
            },
            width: 116,
            color: Colors.grey,
          ),
          DialogButton(
            child: Text(
              Singleton()
                  .remoteConfig
                  .getString('signin_timeout_dialog_cancel_button'),
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
            onPressed: () {
              Navigator.pop(context);
              setState(() {
                _signInUpScreenState = SignInUpScreenState.signIn;
              });
            },
            width: 116,
            color: Colors.grey,
          )
        ],
      ).show();
    });
  }

  void _dequeServerTimer() {
    setState(() {
      _waitQue = false;
    });
    _serverTimer.cancel();
  }

  _spSetAutorizationKey(String key) async {
    await Singleton().sharedPrefs.setString('user_key', key);
  }

  _spSetUserInfo(UserInfo userInfo) async {
    await Singleton().sharedPrefs.setString('user_email', userInfo.email);
    await Singleton().sharedPrefs.setString('user_username', userInfo.username);
    await Singleton().sharedPrefs.setString('user_fullname', userInfo.fullname);
    await Singleton().sharedPrefs.setString('user_company', userInfo.company);
    await Singleton().sharedPrefs.setString('user_job', userInfo.job);
    await Singleton()
        .sharedPrefs
        .setString('user_seeker_intro', userInfo.seekerIntro);
    await Singleton()
        .sharedPrefs
        .setString('user_employee_intro', userInfo.employeeIntro);
    await Singleton().sharedPrefs.setInt('user_role', userInfo.role);
    await Singleton().sharedPrefs.setInt('user_id', userInfo.id);
  }

  // reference: https://medium.com/swlh/how-to-make-http-requests-in-flutter-d12e98ee1cef
  void getLoginToken() async {
    // check if the user is a moron
    if (signinEmailController.text == '' ||
        signinPasswordController.text == '') {
      signInScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(
              Singleton().remoteConfig.getString('login_sb_check_email_pw'))));
      return;
    }

    _queServerTimer(LastServerCall.getLoginToken);

    // set up POST request arguments
    String url = SERVER_URL + LOGIN_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json'
    };
    Singleton().sharedPrefs.setString("user_email", signinEmailController.text);
    String loginInfo = jsonEncode({
      "email": signinEmailController.text,
      "password": signinPasswordController.text
    });

    // make POST request
    Response response =
        await post(url, headers: headers, body: loginInfo).catchError((error) {
//      debugPrint('post new question-----${error.toString()}');
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();
    });
    // check the status code for the result

    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    // if success, store the key
    // using the key, get the user info
    if (statusCode == 200) {
      _spSetAutorizationKey(data['key']);

//      debugPrint('[sign in up] get login token - OK!');
      _dequeServerTimer();

      // clear login stuff from memory as soon as token fetched
      signinEmailController.clear();
      signinPasswordController.clear();

      await getUserInfo();
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[sign in up] get login token - FAILED [${statusCode.toString()}]');
      _dequeServerTimer();

      if (data["non_field_errors"][0] == "E-mail is not verified.") {
//        debugPrint('--- email verification');

        signInScaffoldKey.currentState.showSnackBar(SnackBar(
            duration: Duration(seconds: SNACKBAR_DURATION),
            content: Text(
                Singleton().remoteConfig.getString('login_sb_verify_email'))));
      } else {
        signInScaffoldKey.currentState.showSnackBar(SnackBar(
            duration: Duration(seconds: SNACKBAR_DURATION),
            content: Text(Singleton()
                .remoteConfig
                .getString('login_sb_check_email_pw'))));
      }
    }
  }

  Future<void> registerFcmDevice() async {
    String name = '';
    String registrationId = '';
    String deviceId;
    String type = "web";
    if (Platform.isIOS) type = "ios";
    if (Platform.isAndroid) type = "android";
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    switch (type) {
      case "ios":
        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        deviceId = iosInfo.identifierForVendor;
        name =
            'system version: ${iosInfo.systemVersion}, identifier for vendor: ${iosInfo.identifierForVendor}, kernel version: ${iosInfo.utsname.version}]';
        break;
      case "android":
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        deviceId = androidInfo.androidId;
        name =
            'release version: ${androidInfo.version.release}, sdk version: ${androidInfo.version.sdkInt},'
            ' manufacturer: ${androidInfo.manufacturer}, model: ${androidInfo.model}';
        break;
      default:
    }

    // registration_id is the key
    String url = SERVER_URL + REGISTER_FCM_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + Singleton().sharedPrefs.getString('user_key')
    };

    _queServerTimer(LastServerCall.registerFcmDevice);

    Response response = await get(url, headers: headers).catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();
    });
    int statusCode = response.statusCode;
    var fcmRegisterData = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[sign in up] Get FCM Device info - OK!');
      _firebaseMessaging.getToken().then((token) async {
        registrationId = token;

        Map<String, String> headers = {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader:
              'Token ' + Singleton().sharedPrefs.getString('user_key')
        };

        String fcmDeviceInfo = jsonEncode({
          "name": name,
          "registration_id": registrationId,
          "device_id": deviceId,
          "type": type
        });

        if (fcmRegisterData.isEmpty) {
          // if nothing, POST
          _queServerTimer(LastServerCall.registerFcmDevice);
          Response response =
              await post(url, headers: headers, body: fcmDeviceInfo)
                  .catchError((error) {
            Singleton().noInternetDialog(context).show();
            _dequeServerTimer();
          });
          int statusCode = response.statusCode;
//          var data = json.decode((utf8.decode(response.bodyBytes)));

          if (statusCode == 201) {
//            debugPrint('[sign in up] Register FCM Device - OK!');
          } else {
//            debugPrint(
//                '[sign in up] Register FCM Device - FAILED [${statusCode.toString()}]');
          }
        } else {
          // if anything there, then PATCH
          url = url + '${fcmRegisterData[0]["registration_id"]}/';
          _queServerTimer(LastServerCall.registerFcmDevice);
          Response response =
              await put(url, headers: headers, body: fcmDeviceInfo)
                  .catchError((error) {
            Singleton().noInternetDialog(context).show();
            _dequeServerTimer();
          });
          int statusCode = response.statusCode;
//          var data = json.decode((utf8.decode(response.bodyBytes)));

          if (statusCode == 200) {
//            debugPrint('[sign in up] Update FCM Device - OK!');
          } else if (statusCode == 502) {
            Singleton().noInternetDialog(context).show();
          } else {
//            debugPrint(
//                '[sign in up] Update FCM Device - FAILED [${statusCode.toString()}]');
          }
        }
      });
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[sign in up] Get FCM Device info - FAILED [${statusCode.toString()}]');
    }
  }

  Future<void> getUserInfo() async {
    final Trace trace = Singleton()._performance.newTrace("get_user_info");
    await trace.start();
    await registerFcmDevice();

    String url = SERVER_URL + USER_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + Singleton().sharedPrefs.getString('user_key')
    };

    _queServerTimer(LastServerCall.getUserInfo);
    Response response = await get(
      url,
      headers: headers,
    ).catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();
    });

    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
      await trace.stop();

      Singleton().updateUserBalance();
      await _spSetUserInfo(UserInfo(
          email: data['email'],
          fullname: data['full_name'],
          username: data['username'],
          seekerIntro: data['job_seeker_intro'],
          employeeIntro: data['employee_intro'],
          id: data['id'],
          role: data['role']));
//      debugPrint('[sign in up] fetch user info - OK!');
      _dequeServerTimer();

      if (data['role'] == 1) {
        parent.setUserMode(UserMode.seeker);
        parent.setMainAppState(MainAppState.main);
      } else if (data['role'] == 2) {
        parent.setUserMode(UserMode.helper);
        parent.setMainAppState(MainAppState.main);
//        checkUserCompanyInfo();   // to block entering main page if not verified
      }

      await Singleton()
          .firebaseAnalytics
          .setUserId(Singleton().sharedPrefs.getString("user_username"));
      await Singleton().firebaseAnalytics.setUserProperty(
          name: 'role',
          value: Singleton().sharedPrefs.getInt("user_role").toString());
      await Singleton().firebaseAnalytics.logLogin();
    } else if (statusCode == 502) {
      await trace.stop();

      Singleton().noInternetDialog(context).show();
    } else {
      await trace.stop();

      _dequeServerTimer();

//      debugPrint('getUserInfo: ' + data.toString());
      if (data["detail"] == 'Invalid token.') {
        Singleton().sharedPrefs.clear();
      }
//      debugPrint(
//          '[sign in up] fetch user info - FAILED [${statusCode.toString()}]');

      setState(() {
        _signInUpScreenState = SignInUpScreenState.signIn;
      });
    }
  }

  bool isStringNumbersOnly(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  void trySignUp() async {
    await Singleton().firebaseAnalytics.logEvent(
      name: 'sign_up',
      parameters: <String, dynamic>{
        'role': signUpRole,
      },
    );

    // check if the user is a moron
    if (fullNameController.text == '') {
      signInScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(
              Singleton().remoteConfig.getString('signup_sb_check_fullname'))));
      return;
    } else if (signupEmailController.text == '' ||
        signupPassword1Controller.text == '' ||
        signupPassword2Controller.text == '') {
      signInScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(
              Singleton().remoteConfig.getString('signup_sb_check_email_pw'))));
      return;
    } else if (signupPassword1Controller.text !=
        signupPassword2Controller.text) {
      signInScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(Singleton()
              .remoteConfig
              .getString('signup_sb_pw_mismatch')
              .replaceAll('\\n', '\n'))));
      return;
    } else if (signupPassword1Controller.text.length < 8) {
      signInScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content:
              Text(Singleton().remoteConfig.getString('signup_sb_pw_min8'))));
      return;
    } else if (isStringNumbersOnly(signupPassword1Controller.text)) {
      signInScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(Singleton()
              .remoteConfig
              .getString('signup_sb_pw_alphabet_req'))));
      return;
    }

    _queServerTimer(LastServerCall.trySignUp);

    String url = SERVER_URL + SIGNUP_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json'
    };

    String signupInfo = jsonEncode({
      "full_name": fullNameController.text,
      "email": signupEmailController.text,
      "password1": signupPassword1Controller.text,
      "password2": signupPassword2Controller.text,
      "role": signUpRole,
      "terms_of_use": _didUserAgree,
      "privacy_policy": _didUserAgree,
      "marketing_information_reception": _didUserAgree,
    });

    Response response =
        await post(url, headers: headers, body: signupInfo).catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();

      return;
    });
    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    // if success, store the key
    // using the key, get the user info
    if (statusCode == 201) {
      _spSetAutorizationKey(data['key']);
//      debugPrint('[sign in up] fetch signup (login) token - OK!');

      setState(() {
        _signInUpScreenState = SignInUpScreenState.checkPrivateEmail;
      });
      _dequeServerTimer();

      // clear login stuff from memory as soon as token fetched
      signinEmailController.text = signupEmailController.text;
      fullNameController.clear();
      signupEmailController.clear();
      signupPassword1Controller.clear();
      signupPassword2Controller.clear();
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[sign in up] fetch signup (login) token - FAILED [${statusCode.toString()}]');

      _dequeServerTimer();

      if (data["email"][0] ==
          "A user is already registered with this e-mail address.") {
        signInScaffoldKey.currentState.showSnackBar(SnackBar(
            duration: Duration(seconds: SNACKBAR_DURATION),
            content: Text(Singleton()
                .remoteConfig
                .getString('signup_sb_email_duplicate')
                .replaceAll('\\n', '\n'))));
      } else {
        signInScaffoldKey.currentState.showSnackBar(SnackBar(
            duration: Duration(seconds: SNACKBAR_DURATION),
            content: Text(Singleton()
                .remoteConfig
                .getString('signup_sb_failed')
                .replaceAll('\\n', '\n'))));
      }
    }
  }

  void signOut() async {
    _queServerTimer(LastServerCall.signOut);

    String url = SERVER_URL + SIGN_OUT_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + Singleton().sharedPrefs.getString('user_key'),
    };
    Response response = await post(url, headers: headers).catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();
    });
    int statusCode = response.statusCode;
//    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[sign in up] sign out - OK!');

      _dequeServerTimer();

      Singleton().sharedPrefs.clear();
      parent.reInitApp();
      Navigator.maybePop(context);
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint('[sign in up] sign out - FAILED [${statusCode.toString()}]');
//      _dequeServerTimer();
    }
  }

  Future<void> resetPassword() async {
    if (resetPasswordEmailController.text == '') {
      signInScaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: SNACKBAR_DURATION),
          content: Text(
              Singleton().remoteConfig.getString('resetpw_sb_check_email'))));
      return;
    }

    String url = SERVER_URL + PASSWORD_RESET_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };

    String resetPasswordInfo = jsonEncode({
      "email": resetPasswordEmailController.text,
    });

    _queServerTimer(LastServerCall.resetPassword);
    Response response =
        await post(url, headers: headers, body: resetPasswordInfo)
            .catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();
    });

    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[sign in up] password reset - OK!');
      _dequeServerTimer();
      _signInUpScreenState = SignInUpScreenState.checkPrivateEmail;
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[sign in up] password reset - FAILED [${statusCode.toString()}]');
      if (data["email"][0] == 'Enter a valid email address.') {
        signInScaffoldKey.currentState.showSnackBar(SnackBar(
            duration: Duration(seconds: SNACKBAR_DURATION),
            content: Text(
                Singleton().remoteConfig.getString('resetpw_sb_check_email'))));
      }
      _dequeServerTimer();
    }
  }

  Widget signInSection() {
    return Column(
      children: [
        Container(
          height: 40,
        ),
        Container(
            padding: EdgeInsets.only(left: 80, right: 80),
            child: TextField(
              controller: signinEmailController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  hintText: Singleton()
                      .remoteConfig
                      .getString('signin_email_placeholder'),
                  hintStyle: TextStyle(fontSize: 12)),
            )),
        Container(
            padding: EdgeInsets.only(left: 80, right: 80),
            child: TextField(
              obscureText: true,
              controller: signinPasswordController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  hintText: Singleton()
                      .remoteConfig
                      .getString('signin_pw_placeholder'),
                  hintStyle: TextStyle(fontSize: 12)),
            )),
        Container(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
              padding: EdgeInsets.zero,
              child: Text(
                  Singleton().remoteConfig.getString('signin_forgot_pw'),
                  style: TextStyle(color: Colors.grey)),
              onPressed: () {
                setState(() {
                  _signInUpScreenState = SignInUpScreenState.forgotPassword;
                });
              },
            ),
            FlatButton(
                color: Colors.black12,
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0)),
                onPressed: () {
                  getLoginToken();
                },
                child: Text(Singleton().remoteConfig.getString('signin_button'),
                    style:
                        TextStyle(fontWeight: FontWeight.w400, fontSize: 16))),
          ],
        ),
        Container(
          height: 16,
        ),
        Column(children: [
          Container(
              child: Text(Singleton().remoteConfig.getString('signup_message'),
                  style: TextStyle(color: Colors.grey[200]))),
          Container(
              child: FlatButton(
                  onPressed: () {
                    if (signinEmailController.text != '') {
                      signupEmailController.text = signinEmailController.text;
                    }
                    setState(() {
                      _signInUpScreenState = SignInUpScreenState.signUp;
                    });
                  },
                  child: Text(
                      Singleton().remoteConfig.getString('signup_button'),
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.blue[200],
                          fontSize: 16)))),
        ]),
      ],
    );
  }

  Widget signUpSection() {
    return Column(
      children: [
        Container(
          child: UserRoleRadioButtonWidget(),
        ),
        Container(
            padding: EdgeInsets.only(left: 80, right: 80),
            child: TextField(
              controller: fullNameController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  hintText: Singleton()
                      .remoteConfig
                      .getString('signup_fullname_placeholder'),
                  hintStyle: TextStyle(fontSize: 12)),
            )),
        Container(
            padding: EdgeInsets.only(left: 80, right: 80),
            child: TextField(
              controller: signupEmailController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  hintText: Singleton()
                      .remoteConfig
                      .getString('signup_email_placeholder'),
                  hintStyle: TextStyle(fontSize: 12)),
            )),
        Container(
            padding: EdgeInsets.only(left: 80, right: 80),
            child: TextField(
              obscureText: true,
              controller: signupPassword1Controller,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  hintText: Singleton()
                      .remoteConfig
                      .getString('signup_pw_placeholder'),
                  hintStyle: TextStyle(fontSize: 12)),
            )),
        Container(
            padding: EdgeInsets.only(left: 80, right: 80),
            child: TextField(
              obscureText: true,
              controller: signupPassword2Controller,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  hintText: Singleton()
                      .remoteConfig
                      .getString('signup_pw2_placeholder'),
                  hintStyle: TextStyle(fontSize: 12)),
            )),
        Container(
          height: 16,
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 80),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  launchURL(
                      Singleton().remoteConfig.getString('terms_of_use_url'));
                },
                child: Text(
                    Singleton().remoteConfig.getString('terms_of_use_button'),
                    style: TextStyle(color: Colors.grey, fontSize: 12)),
              ),
              Container(
                width: 8,
              ),
              GestureDetector(
                onTap: () {
                  launchURL(
                      Singleton().remoteConfig.getString('privacy_policy_url'));
                },
                child: Text(
                    Singleton().remoteConfig.getString('privacy_policy_button'),
                    style: TextStyle(color: Colors.grey, fontSize: 12)),
              ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Checkbox(
              value: _didUserAgree,
              onChanged: (bool value) {
                setState(() {
                  _didUserAgree = value;
                  if (_didUserAgree) {
                    _signUpButtonColor = Colors.blue[300];
                  } else {
                    _signUpButtonColor = Colors.grey[300];
                  }
                });
              },
            ),
            Text(
                Singleton()
                    .remoteConfig
                    .getString('signup_comveri_agreement_label'),
                style: TextStyle(color: Colors.grey, fontSize: 12)),
          ],
        ),
        Container(
            padding: EdgeInsets.only(left: 60, right: 60),
            child: FlatButton(
                color: _signUpButtonColor,
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0)),
                onPressed: () {
                  if (_didUserAgree) {
                    Singleton().sharedPrefs.clear();
                    trySignUp();
                  }
                },
                child: Text(Singleton().remoteConfig.getString('signup_button'),
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                        fontSize: 16)))),
        Container(
          height: 16,
        ),
        Container(
            child: Text(Singleton().remoteConfig.getString('signin_message'),
                style: TextStyle(color: Colors.black38))),
        Column(children: [
          Container(
              child: FlatButton(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0)),
                  onPressed: () {
                    signinPasswordController.clear();
                    setState(() {
                      _signInUpScreenState = SignInUpScreenState.signIn;
                    });
                  },
                  child: Text(
                      Singleton().remoteConfig.getString('signin_button'),
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.blue[200],
                          fontSize: 16)))),
        ])
      ],
    );
  }

  Widget checkPrivateEmailSection() {
    return Column(
      children: [
        Container(
          height: 128,
        ),
        Container(
          padding: EdgeInsets.only(left: 24, right: 24),
          child: Text(
              Singleton()
                  .remoteConfig
                  .getString('signup_verify_email_message')
                  .replaceAll('\\n', '\n'),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18)),
        ),
        Container(
          height: 100,
        ),
        Column(children: [
          Container(
              child: FlatButton(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0)),
                  padding: EdgeInsets.all(8.0),
                  onPressed: () {
                    signinPasswordController.clear();

                    setState(() {
                      _signInUpScreenState = SignInUpScreenState.signIn;
                    });
                  },
                  child: Text(
                      Singleton().remoteConfig.getString('signin_button'),
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.blue[200],
                          fontSize: 16)))),
        ])
      ],
    );
  }

  Widget forgotPasswordSection() {
    return Container(
        padding: EdgeInsets.only(left: 48, right: 48),
        child: Column(children: [
          Container(
            height: 60,
          ),
          Text(Singleton().remoteConfig.getString('resetpw_message'),
              textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
          Container(
            height: 16,
          ),
          Container(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: TextField(
              controller: resetPasswordEmailController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  hintText: Singleton()
                      .remoteConfig
                      .getString('resetpw_email_paceholder')),
            ),
          ),
          Container(
            height: 16,
          ),
          FlatButton(
              color: Colors.black12,
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10.0)),
              onPressed: () {
                setState(() {
                  resetPassword();
                });
              },
              child: Text(Singleton().remoteConfig.getString('resetpw_button'),
                  style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16))),
          Container(
            height: 40,
          ),
          Container(
              child: FlatButton(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0)),
                  onPressed: () {
                    setState(() {
                      _signInUpScreenState = SignInUpScreenState.signIn;
                    });
                  },
                  child: Text(
                      Singleton().remoteConfig.getString('signin_button'),
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.blue[200],
                          fontSize: 16)))),
        ]));
  }
}

// radio button referenced: https://api.flutter.dev/flutter/material/Radio-class.html
//enum userRole { seeker, helper }  // moved to top
class UserRoleRadioButtonWidget extends StatefulWidget {
  UserRoleRadioButtonWidget({Key key}) : super(key: key);

  @override
  _UserRoleRadioButtonWidgetState createState() =>
      _UserRoleRadioButtonWidgetState();
}

class _UserRoleRadioButtonWidgetState extends State<UserRoleRadioButtonWidget> {
  UserMode _userMode = UserMode.seeker;

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Column(
          children: [
            if (_userMode == UserMode.helper)
              Bubble(
                alignment: Alignment.center,
                color: Colors.blue[100],
                nip: BubbleNip.no,
                child: Text(
                    Singleton()
                        .remoteConfig
                        .getString('signup_employee_email_message'),
                    style: TextStyle(color: Colors.black38)),
              )
            else
              Text('\n')
          ],
        ),
        Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Radio(
            value: UserMode.seeker,
            groupValue: _userMode,
            onChanged: (UserMode value) {
              signUpRole = 1;
              setState(() {
                _userMode = value;
              });
            },
          ),
          Text(Singleton().remoteConfig.getString('job_seeker')),
          Radio(
            value: UserMode.helper,
            groupValue: _userMode,
            onChanged: (UserMode value) {
              signUpRole = 2;
              setState(() {
                _userMode = value;
              });
            },
          ),
          Text(Singleton().remoteConfig.getString('employee')),
        ]),
      ],
    );
  }
}

/// end of sign-in, sign-up shit
///
