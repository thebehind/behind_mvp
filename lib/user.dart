/// 2019.10.11
/// 1. pagination for init q, a
///
/// 2019.10.04
/// 1. show current position (points) on top right, click to payment page
///
/// 2019.09.28
/// 1. get the introductions of the chat members
/// 2. filter already answerd reqOnAir
///
/// 2019.09.24
/// 1. class for init Q/A
/// 2. rewrite reqChatRow() with the new class
/// 3. rewrite respChatRow() with the new class
/// 4. factor out separate parts from this humongous "user.dart" dump
///
/// 2019.09.22
/// 1. seeker side chat list (reqchatrow) to show the answerer's company and job
///
/// 2019.09.17
/// 1. server reqeust timer - timeout
///
/// 2019.09.16
/// 1. seeker to post question
/// 2. seeker to see my questions list
/// 3. pull down to refresh my questions list
///   - https://codingwithjoe.com/flutter-implementing-pull-to-refresh/
/// 4. employee see my questions list
/// 5. employee questions on air list
/// 6. employee my responses list
///
///
/// 2019.09.15
/// 1. employee sign up shit moved in to this dart file from sign_in_up.dart,
///   because it is handled within main user page now
/// 2. chat room list now has subordinate optional responses list
/// 3. employee sign up flow changed
///   - need to put in email (for company) as before
///   - when email typed in, corresponding job list will be created
/// 4. seeker chat room ui changed
///   - shows first Q and current status
/// 5. modal spinner to wait for server
///   - referenced: https://codingwithjoe.com/flutter-how-to-build-a-modal-progress-indicator/
///
/// 2019.09.11
/// 1. chat input method
///
/// 2019.09.06
/// ********** file name changed "seeker.dart" -> "user.dart"
/// ********** to be used by helper as well :)
/// if userMode is seeker, hide helper stuff
/// 1. mypage - separate dart file ("user_my_page.dart")
///
/// 2019.09.05
/// 1. chat history page
///   - set up the 'row' template that can read out json
///
/// 2019.09.04
/// 1. setup the main (request) page
/// 2. setup the top bar (appBar)
/// 3. setup the navigation bar (bottom app bar)
///   - set the entire page as a stateful widget
///   tap indices
///     0: request
///     1: chat
///     2: account
/// 4. pages other than request hides 'behind' logo on top left
///   - change the string to ''
///
/// 2019.09.03
/// ychoi
/// seeker part of behind app
/// create the wireframe

part of 'main.dart';

class InitQA {
  int questionId;
  int questionerId;
  String initQ;
  String targetCompany;
  String targetJob;
  String questionDate;
  String questionerIntro;
  String questionerUsername;

  int answerId;
  String initA;
  int answererId;
  String answererUsername;
  String answererCompany;
  int answererCompanyId;
  String answererJob;
  String answerDate;
  String answererIntro;

  int chatRoomId;
  int chatRoomStatus;

  InitQA(
      {this.questionId = -1,
      this.questionerId = -1,
      this.initQ = "seeker's initial question",
      this.targetCompany = "question's target company",
      this.targetJob = "question's target job",
      this.questionerIntro = "",
      this.questionerUsername = "questioner",
      this.questionDate = "question post date",
      this.answerId = -1,
      this.initA = "employee's initial answer",
      this.answererId = -1,
      this.answererUsername = "employee's username",
      this.answererCompany = "employee's company",
      this.answererCompanyId = -1,
      this.answererJob = "employee's job",
      this.answererIntro = "employee's intro",
      this.answerDate = "answer post date",
      this.chatRoomId = -1,
      this.chatRoomStatus = 0});
}

class ChatInfo {
  int id;
  String company;
  String job;
  String status;
  String date;
  String initQ;
  String username;
  String userType;
  List<dynamic> answers;
  ChatInfo(
      {this.id = -1, // to be handled by DB
      this.company = "타겟 기업",
      this.job = "타겟 직업",
      this.initQ = "첫 질문",
      this.username = '구직자',
      this.status = "pending",
      this.userType = "seeker",
      this.date = "옛날옛적"});
}

enum CompanyVerificationState { emailNotSubmitted, emailNotVerified, verified }

final GlobalKey<ScaffoldState> userScaffoldKey = GlobalKey<ScaffoldState>();

class UserWidget extends StatefulWidget {
  final _MainAppState parent;
  UserWidget(this.parent);

  @override
  UserWidgetState createState() => UserWidgetState(parent);
}

class UserWidgetState extends State<UserWidget> {
  final _MainAppState parent;
  UserWidgetState(this.parent);
  Singleton singleton;
  bool _waitQue;
  Timer _serverTimer;

  UserMode _userMode;
  int _currNavIndex;
  Widget _selectedPage;
  String _logoText;
  String _tabTitle;
  var _questionChatData;
  var _respChatData;
  var _reqOnAirData;
  bool _isSeekerChatMode;
  CompanyVerificationState _companyVerificationState;
  String _jobForRegistration;
  String _jobForQuestion;
  List<String> _jobList;
  Map<String, int> _jobMap;
  List<String> _companyList;
  Map<String, int> _companyMap;
  List<Widget> _searchCompanyButtonList;
  int _userPoint;
  bool _questionJobsOnlySelected;

  String _getMyQuestionsNextCursorUrl;
  bool _getMyQuestionsCalled;
  String _getMyAnswersNextCursorUrl;
  bool _getMyAnswersCalled;
  String _getQuestionsOnAirNextCursorUrl;
  bool _getQuestionsOnAirCalled;

  TextEditingController questionCompanyController = new TextEditingController();
  TextEditingController jobController = new TextEditingController();
  TextEditingController initialQuestionController = new TextEditingController();
  TextEditingController companyEmailController = new TextEditingController();
  TextEditingController companyNameController = new TextEditingController();
  TextEditingController initialAnswerController = new TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    BackButtonInterceptor.remove(backButtonInterceptor);
    questionCompanyController.dispose();
    jobController.dispose();
    initialQuestionController.dispose();
    companyEmailController.dispose();
    companyNameController.dispose();
    initialAnswerController.dispose();
    super.dispose();
  }

  @override
  initState() {
//    debugPrint("----------[user.dart]----------");
    super.initState();
    BackButtonInterceptor.add(backButtonInterceptor);
    _waitQue = false;

    _currNavIndex = 0;
    singleton = Singleton();
//    _userMode = singleton._sharedPrefs.getInt('user_role') == 2 ? userMode.helper : userMode.seeker;
    _userMode = parent._userMode;
    _userPoint = 0;

    _questionChatData = List<dynamic>();
    _respChatData = List<dynamic>();
    _reqOnAirData = List<dynamic>();
    _isSeekerChatMode = true;
    _jobForRegistration = '';
    _jobForQuestion = '';
    _jobList = List<String>();
    _jobMap = Map<String, int>();
    _companyList = List<String>();
    _companyMap = Map<String, int>();
    _searchCompanyButtonList = List<Widget>();

    _getMyQuestionsNextCursorUrl = null;
    _getMyQuestionsCalled = false;
    _getMyAnswersNextCursorUrl = null;
    _getMyAnswersCalled = false;
    _getQuestionsOnAirNextCursorUrl = null;
    _getQuestionsOnAirCalled = false;
    _questionJobsOnlySelected = false;

    _companyVerificationState = CompanyVerificationState.verified;
    updateUserBalanceUI();
    _getJobList();
    _getCompanyList();

    if (_userMode == UserMode.helper) {
      _checkUserCompanyInfo();
      _getQuestionsOnAir(true);
      _selectedPage = questionOnAirListPage();
      _isSeekerChatMode = false;
    } else if (_userMode == UserMode.seeker) {
      _selectedPage = questionPage();
    }
  }

  List<Widget> _buildForm(BuildContext context) {
    Container container = Container(color: Colors.white, child: _selectedPage);
    var l = new List<Widget>();
    l.add(container);
    if (_waitQue) {
      var modal = new Stack(
        children: [
          new Opacity(
            opacity: 0.3,
            child: const ModalBarrier(dismissible: false, color: Colors.grey),
          ),
          new Center(
            child: new CircularProgressIndicator(),
          ),
        ],
      );
      l.add(modal);
    }
    return l;
  }

  @override
  Widget build(BuildContext context) {
    Widget pointButton = Container();
    if (!Singleton().isAppReviewState) {
      pointButton = FlatButton(
          onPressed: () {
            puchasePage(context, this);
          },
          child: Text("${NumberFormat("#,###").format(_userPoint)} P",
              style: TextStyle(color: Colors.black, fontSize: 14)));
    }

    return Scaffold(
      key: userScaffoldKey,
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          _tabTitle,
          style: TextStyle(fontSize: 24, color: Colors.black),
        ),
        centerTitle: true,
        flexibleSpace: Container(
          margin: EdgeInsets.only(bottom: 10, left: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    _logoText,
                    style: TextStyle(
                        fontFamily: 'Zurich BdXCn BT',
                        fontSize: 32,
                        color: Colors.black),
                  ),
                  pointButton,
                ],
              ),
            ],
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Stack(children: _buildForm(context))),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        elevation: 0,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: <BottomNavigationBarItem>[
          if (_userMode == UserMode.helper)
            BottomNavigationBarItem(
              title: Text('answer'),
              icon: Icon(Icons.show_chart),
            ),
          BottomNavigationBarItem(
            title: Text('question'),
            icon: Icon(Icons.border_color),
          ),
          BottomNavigationBarItem(
            title: Text('chat'),
            icon: Icon(Icons.chat_bubble_outline),
          ),
          BottomNavigationBarItem(
            title: Text('my page'),
            icon: Icon(Icons.person_outline),
          ),
        ],
        currentIndex: _currNavIndex,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.black12,
        onTap: _onNavTapped,
      ),
    );
  }

  bool backButtonInterceptor(bool stopDefaultButtonEvent) {
//    debugPrint("BACK BUTTON!"); // Do some stuff.
    Navigator.maybePop(context);
    return true;
  }

  void updateUserBalanceUI() async {
    Singleton().updateUserBalance();
    setState(() {
      if (Singleton().sharedPrefs.getInt("user_balance") != null) {
        _userPoint = Singleton().sharedPrefs.getInt("user_balance");
      }
    });
  }

  void _queServerTimer(LastServerCall lastServerCall) {
    if (_serverTimer != null) _serverTimer.cancel();
    if (lastServerCall !=
        LastServerCall.enterChatRoom) // this function is called during build
    {
      setState(() {
        _waitQue = true;
      });
    }
    _serverTimer = Timer(Duration(seconds: SERVER_TIMEOUT_DURATION), () {
//      debugPrint('[user] server timed out');
      setState(() {
        _waitQue = false;
      });
      Alert(
        style: AlertStyle(
          animationType: AnimationType.grow,
          animationDuration: const Duration(milliseconds: 500),
          isCloseButton: false,
          alertBorder: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
            side: BorderSide(
              color: Colors.grey,
            ),
          ),
        ),
        context: context,
        title: Singleton().remoteConfig.getString('alert_title'),
        type: AlertType.info,
        desc: Singleton().remoteConfig.getString('server_timeout_message'),
        buttons: [
          DialogButton(
            child: Text(
              Singleton().remoteConfig.getString('call_server_again_button'),
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () {
              Singleton().reInitAppFunc();
            },
            width: 120,
            color: Colors.grey,
          )
        ],
      ).show();
    });
  }

  void _dequeServerTimer() {
    setState(() {
      _waitQue = false;
    });
    _serverTimer.cancel();
  }

  void _getJobList() async {
    _queServerTimer(LastServerCall._getJobList);

    String url = SERVER_URL + JOB_LIST_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };
    Response response = await get(
      url,
      headers: headers,
    ).catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();

      return;
    });

    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[user] fetch job list - OK!');
      _jobList.clear();
      _jobMap.clear();
      for (var job in data) {
        _jobList.add(job['title']);
        _jobMap[job['title']] = job['id'];
      }
      _jobForRegistration = _jobList[0];
      _jobForQuestion = _jobList[0];
      setState(() {
        if (_userMode == UserMode.helper) {
          _selectedPage = questionOnAirListPage();
        } else if (_userMode == UserMode.seeker) {
          _selectedPage = questionPage();
        }
      });
      _dequeServerTimer();
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint('[user] fetch Job list - FAILED [${statusCode.toString()}]');
      _dequeServerTimer();
    }
  }

  Future<void> _getCompanyList() async {
    _queServerTimer(LastServerCall._getCompanyList);

    String url = SERVER_URL + COMPANY_LIST_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };
    Response response = await get(
      url,
      headers: headers,
    ).catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();
      return;
    });

    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[user] fetch company list - OK!');
      _companyList.clear();
      _companyMap.clear();
      for (var company in data) {
        _companyList.add(company['name']);
        _companyMap[company['name']] = company['id'];
      }
      setState(() {
        if (_userMode == UserMode.helper) {
          _selectedPage = questionOnAirListPage();
        } else if (_userMode == UserMode.seeker) {
          _selectedPage = questionPage();
        }
      });
      _dequeServerTimer();
    } else {
//      debugPrint(
//          '[user] fetch company list - FAILED [${statusCode.toString()}]');
      _dequeServerTimer();
    }
  }

  void _showLeftTopLogo(bool enabled) {
    if (enabled)
      _logoText = Singleton().remoteConfig.getString('logo_text');
    else
      _logoText = '';
  }

  void sendCompanyEmailVerification() async {
    _queServerTimer(LastServerCall.sendCompanyEmailVerification);

    String url = SERVER_URL + JOB_HIST_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key')
    };
    // job_id: 1 ~ 13
    // confirmation_method: 1 - email; 2 - business card; 3 - proof of employment
    String signupInfo = jsonEncode({
      "job_id": _jobMap[_jobForRegistration],
      "confirmation_method": "1",
      "company_email": companyEmailController.text,
      "company_name": companyNameController.text
    });

    Response response =
        await post(url, headers: headers, body: signupInfo).catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();
      return;
    });

    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 201) {
//      debugPrint('[user] send company email verification - OK!');
      companyEmailController.clear();
      companyNameController.clear();
      setState(() {
        _companyVerificationState = CompanyVerificationState.emailNotVerified;
        _selectedPage = questionOnAirListPage();
      });
      _dequeServerTimer();
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
      _dequeServerTimer();

//      debugPrint(
//          '[user] send company email verification - FAILED [${statusCode.toString()}]');
      if (data["company_email"] == '') {
        Alert(
          style: AlertStyle(
            animationType: AnimationType.grow,
            animationDuration: const Duration(milliseconds: 500),
            isCloseButton: false,
            alertBorder: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
              side: BorderSide(
                color: Colors.grey,
              ),
            ),
          ),
          context: context,
          title: Singleton().remoteConfig.getString('alert_title'),
          type: AlertType.success,
          desc: Singleton()
              .remoteConfig
              .getString('signup_comveri_sb_unknown_domain')
              .replaceAll('\\n', '\n'),
          buttons: [
            DialogButton(
              child: Text(
                Singleton().remoteConfig.getString('call_server_again_button'),
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                sendCompanyEmailVerification();
//                Singleton().reInitAppFunc();
              },
              width: 120,
              color: Colors.grey,
            )
          ],
        ).show();
      }
      switch (data["company_email"][0]) {
        case 'Enter a valid email address.':
          userScaffoldKey.currentState.showSnackBar(SnackBar(
              duration: const Duration(seconds: SNACKBAR_DURATION),
              content: Text(Singleton()
                  .remoteConfig
                  .getString('signup_comveri_sb_check_email'))));
          break;
        case 'Wrong company email.':
          userScaffoldKey.currentState.showSnackBar(SnackBar(
              duration: const Duration(seconds: SNACKBAR_DURATION),
              content: Text(Singleton()
                  .remoteConfig
                  .getString('signup_comveri_sb_wrong_email'))));
          break;
      }
    }
  }

  _spSetUserCompanyInfo(UserInfo userInfo) async {
    await singleton.sharedPrefs.setString('user_company', userInfo.company);
    await singleton.sharedPrefs.setInt('user_company_id', userInfo.companyId);
    await singleton.sharedPrefs.setString('user_job', userInfo.job);
  }

  void _checkUserCompanyInfo() async {
    _queServerTimer(LastServerCall._checkUserCompanyInfo);

    String url = SERVER_URL + JOB_HIST_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key')
    };
    Response response = await get(
      url,
      headers: headers,
    ).catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();

      return;
    });
    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[user] check user company info - OK!');
      if (data.isEmpty) {
//        debugPrint(
//            '[user] company email verification - FAIL: email not submitted [${statusCode.toString()}]');
        _companyVerificationState = CompanyVerificationState.emailNotSubmitted;
        setState(() {
          _selectedPage = questionOnAirListPage();
        });
        _dequeServerTimer();
      } else {
        if (data[0]['confirmed'] == false) {
//          debugPrint(
//              '[user] company email verification - FAIL: email not verified [${statusCode.toString()}]');
          _companyVerificationState = CompanyVerificationState.emailNotVerified;
          setState(() {
            _selectedPage = questionOnAirListPage();
          });
          _dequeServerTimer();
        } else {
//          debugPrint('[user] company email verification - OK!');

          _spSetUserCompanyInfo(UserInfo(
              company: data[0]['company']['name'],
              companyId: data[0]['company']['id'],
              job: data[0]['job']['title']));
          _companyVerificationState = CompanyVerificationState.verified;
          setState(() {
            _selectedPage = questionOnAirListPage();
          });
          _dequeServerTimer();
        }
      }
      // move to chat room
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[user] check user company info - FAILED [${statusCode.toString()}]');
      _dequeServerTimer();
    }
  }

  Future<void> updateSearchCompanyList(
      [TextEditingController controller]) async {
    List<String> searchedCompanyList = List<String>();
    _searchCompanyButtonList.clear();
    if (controller.text != '') {
      int i = 0;
      for (var company in _companyList) {
        if (company.contains(controller.text)) {
          if (i++ >
              int.parse(Singleton()
                  .remoteConfig
                  .getString('number_of_companies_to_list'))) {
            break;
          }
          searchedCompanyList.add(company);
          _searchCompanyButtonList.add(OutlineButton(
              borderSide: BorderSide(color: Colors.black),
              onPressed: () {
                _searchCompanyButtonList.clear();
                controller.text = company;
                _searchCompanyButtonList.clear();
                setState(() {
                  if (controller == questionCompanyController) {
                    _selectedPage = questionPage();
                  } else if (controller == companyNameController) {
                    _selectedPage = questionOnAirListPage();
                  }
                });
              },
              child: Text(company,
                  style:
                      TextStyle(fontWeight: FontWeight.w400, fontSize: 16))));
        }
      }
    }

    setState(() {
      if (controller == questionCompanyController) {
        _selectedPage = questionPage();
      } else if (controller == companyNameController) {
        _selectedPage = questionOnAirListPage();
      }
    });
  }

  void _postNewQuestion() async {
    await Singleton().firebaseAnalytics.logEvent(
      name: 'post_new_question',
      parameters: <String, dynamic>{
        'company': questionCompanyController.text,
        'job': _jobForQuestion,
      },
    );

    // miss-click check
    if (initialQuestionController.text == '' ||
        questionCompanyController.text == '') {
      return;
    }
//    if (_searchCompanyConfirmed == false) {
//      userScaffoldKey.currentState.showSnackBar(SnackBar(
//          duration: const Duration(seconds: SNACKBAR_DURATION),
//          content: Text(Singleton()
//              .remoteConfig
//              .getString('question_company_not_selected'))));
//      return;
//    }

    _queServerTimer(LastServerCall._postNewQuestion);

    String url = SERVER_URL + QUESTION_API;
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key')
    };

    String questionInfo = jsonEncode({
      "content": initialQuestionController.text,
      "job_id": _jobMap[_jobForQuestion].toString(),
      "company_name": questionCompanyController.text.toString()
    });
    Response response = await post(url, headers: headers, body: questionInfo)
        .catchError((error) {
//      debugPrint('post new question-----${error.toString()}');
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();
    });
    int statusCode = response.statusCode;
//    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 201) {
//      debugPrint('[user] user post question - OK!');
      questionCompanyController.clear();
      initialQuestionController.clear();
      await _getMyQuestions();
      setState(() {
        _currNavIndex = singleton.sharedPrefs.getInt('user_role') == 2 ? 2 : 1;
        _selectedPage = chatListPage();
        _isSeekerChatMode = true;
      });
      _dequeServerTimer();
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[user] user post question - FAILED [${statusCode.toString()}]');
      _dequeServerTimer();
    }
  }

  Future<void> _getMyQuestions([bool init = false]) async {
    if (!init &&
        (_questionChatData.isNotEmpty &&
            _getMyQuestionsNextCursorUrl == null)) {
      return;
    }
    if (_getMyQuestionsCalled) {
      return;
    }
    _getMyQuestionsCalled = true;

    String url = SERVER_URL + QUESTION_API;
    if (_getMyQuestionsNextCursorUrl != null && !init) {
      url = _getMyQuestionsNextCursorUrl;
    }

    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key')
    };

    _queServerTimer(LastServerCall._getMyQuestions);
    Response response = await get(url, headers: headers).catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();
    });

    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[user] get my questions - OK!');

      setState(() {
        if (init ||
            (_questionChatData.isEmpty &&
                _getMyQuestionsNextCursorUrl == null)) {
          _questionChatData = data["results"];
        } else {
          _questionChatData = [..._questionChatData, ...data["results"]];
        }
        _getMyQuestionsNextCursorUrl = data["next"];
        _getMyQuestionsCalled = false;
        _selectedPage = chatListPage();
      });

      _dequeServerTimer();
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint('[user] get my questions - FAILED [${statusCode.toString()}]');
      _getMyQuestionsCalled = false;

      _dequeServerTimer();
    }
  }

  Future<void> _getMyAnswers([bool init = false]) async {
    if (!init &&
        (_respChatData.isNotEmpty && _getMyAnswersNextCursorUrl == null)) {
      return;
    }
    if (_getMyAnswersCalled) {
      return;
    }
    _getMyAnswersCalled = true;

    String url = SERVER_URL + ANSWER_API;
    if (_getMyAnswersNextCursorUrl != null && !init) {
      url = _getMyAnswersNextCursorUrl;
    }

    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key')
    };
    _queServerTimer(LastServerCall._getMyAnswers);

    Response response = await get(url, headers: headers).catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();

      return;
    });
    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[user] get my responses - OK!');
      setState(() {
        if (init ||
            (_respChatData.isEmpty && _getMyAnswersNextCursorUrl == null)) {
          _respChatData = data["results"];
        } else {
          _respChatData = [..._respChatData, ...data["results"]];
        }
        _getMyAnswersNextCursorUrl = data["next"];
        _getMyAnswersCalled = false;
        _selectedPage = chatListPage();
      });
      _dequeServerTimer();
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint('[user] get my responses - FAILED [${statusCode.toString()}]');
      _getMyAnswersCalled = false;

      _dequeServerTimer();
    }
  }

  void _getQuestionsOnAir([bool init = false]) async {
    if (!init &&
        (_reqOnAirData.isNotEmpty && _getQuestionsOnAirNextCursorUrl == null)) {
      return;
    }
    if (_getQuestionsOnAirCalled) {
      return;
    }
    _getQuestionsOnAirCalled = true;

    String url = SERVER_URL + QUESTION_FEED_API;
    if (_getQuestionsOnAirNextCursorUrl != null && !init) {
      url = _getQuestionsOnAirNextCursorUrl;
    }

    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader:
          'Token ' + singleton.sharedPrefs.getString('user_key')
    };

    _queServerTimer(LastServerCall._getQuestionsOnAir);
    Response response = await get(url, headers: headers).catchError((error) {
      Singleton().noInternetDialog(context).show();
      _dequeServerTimer();
      return;
    });
    int statusCode = response.statusCode;
    var data = json.decode((utf8.decode(response.bodyBytes)));

    if (statusCode == 200) {
//      debugPrint('[user] get questions on air - OK!');

      setState(() {
        if (init ||
            (_reqOnAirData.isEmpty &&
                _getQuestionsOnAirNextCursorUrl == null)) {
          _reqOnAirData = data["results"];
        } else {
          _reqOnAirData = [..._reqOnAirData, ...data["results"]];
        }
        _getQuestionsOnAirNextCursorUrl = data["next"];
        _getQuestionsOnAirCalled = false;
        _selectedPage = questionOnAirListPage();
      });

      _dequeServerTimer();
    } else if (statusCode == 502) {
      Singleton().noInternetDialog(context).show();
    } else {
//      debugPrint(
//          '[user] get questions on air - FAILED [${statusCode.toString()}]');
      setState(() {
        _selectedPage = questionOnAirListPage();
      });
      _dequeServerTimer();
    }
  }

  void exitSeekerRespCheckEnterNewChatroom(
      BuildContext context, InitQA initQA) {
    Navigator.pop(context);
    newChatScreen(context, this, initQA);
  }

  void _onNavTapped(int tappedNavIndex) {
    updateUserBalanceUI();
    if (_userMode == UserMode.helper) {
      switch (tappedNavIndex) {
        case 0:
          _checkUserCompanyInfo();
          _getQuestionsOnAirCalled = false;
          _getQuestionsOnAir(true);
          _selectedPage = questionOnAirListPage();
          break;
        case 1:
          _selectedPage = questionPage();
          break;
        case 2:
          _getMyQuestionsCalled = false;
          _getMyAnswersCalled = false;
          _getMyQuestions(true);
          _getMyAnswers(true);
          _selectedPage = chatListPage();
          break;
        case 3:
          _showLeftTopLogo(false);
          _tabTitle = Singleton().remoteConfig.getString('tab_title_my_page');
          _selectedPage = UserMyPageWidget(this);
          break;
      }
    } else if (_userMode == UserMode.seeker) {
      switch (tappedNavIndex) {
        case 0:
          _selectedPage = questionPage();
          break;
        case 1:
          _getMyQuestionsCalled = false;
          _getMyQuestions(true);
          _selectedPage = chatListPage();
          break;
        case 2:
          _showLeftTopLogo(false);
          _tabTitle = Singleton().remoteConfig.getString('tab_title_my_page');
          _selectedPage = UserMyPageWidget(this);
          break;
      }
    }
    setState(() {
      _currNavIndex = tappedNavIndex;
    });
  }

  Widget questionOnAirListPage() {
    _showLeftTopLogo(true);
//    _tabTitle = Singleton().remoteConfig.getString('tab_title_answer');

    Widget filterReqOnAir(var chatData) {
      int myUserId = singleton.sharedPrefs.getInt("user_id");
      String questionStatus = 'onAir';

      Text questionTypeText = Text(
        Singleton().remoteConfig.getString('answer_question_to_answer'),
        style: TextStyle(color: Colors.red),
      );

      if (chatData["questioner"] == myUserId) {
        questionStatus = 'mine';
        questionTypeText = Text(
          Singleton().remoteConfig.getString('answer_question_mine'),
          style: TextStyle(color: Colors.grey),
        );
      }

      for (var answer in chatData["answers"]) {
        if (answer["answerer"]["id"] == myUserId) {
          questionStatus = 'answered';
          questionTypeText = Text(
            Singleton().remoteConfig.getString('answer_question_answered'),
            style: TextStyle(color: Colors.blue),
          );
          break;
        }
      }

      InitQA initQA = InitQA(
          questionId: chatData["id"],
          questionerId: chatData["questioner"],
          targetCompany: chatData["company"]["name"],
          targetJob: chatData["job"]["title"],
          questionDate: UTC2LOCAL(chatData["created_at"]),
          initQ: chatData["content"]);

      return FlatButton(
        padding: EdgeInsets.zero,
        onPressed: () {
          if (questionStatus == 'onAir') {
            initAnswerPage(context, this, initQA);
          } else if (questionStatus == 'answered') {
            _currNavIndex = 2;
            _isSeekerChatMode = false;
            _getMyAnswers(true);
          } else if (questionStatus == 'mine') {
            _currNavIndex = 2;
            _isSeekerChatMode = true;
            _getMyQuestions(true);
          }
        },
        child: Row(
          children: <Widget>[
            Flexible(
              fit: FlexFit.tight,
              flex: 1,
              child: Column(
                children: <Widget>[
                  Text(
                    reformatDateTime(initQA.questionDate),
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
            ),
            Flexible(
              fit: FlexFit.tight,
              flex: 3,
              child: Text(
                initQA.initQ,
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 16),
                overflow: TextOverflow.fade,
                maxLines: 2,
              ),
            ),
            questionTypeText,
            Container(
                padding: EdgeInsets.only(right: 4),
                child: Icon(Icons.arrow_forward_ios)),
          ],
        ),
      );
    }

    List<Widget> filteredReqOnAirList;
    filteredReqOnAirList = List<Widget>();

    if (_reqOnAirData.isNotEmpty) {
      for (var chatData in _reqOnAirData) {
        Widget newWidget = filterReqOnAir(chatData);
        if (newWidget != null) {
          filteredReqOnAirList.add(newWidget);
        }
      }
    }

    switch (_companyVerificationState) {
      case CompanyVerificationState.emailNotSubmitted:
        _tabTitle = '';
        return employeeSignupSection();
        break;
      case CompanyVerificationState.emailNotVerified:
        _tabTitle = '';
        return checkCompanyEmailSection();
        break;
      case CompanyVerificationState.verified:
      default:
        _tabTitle = Singleton().remoteConfig.getString('tab_title_answer');
        return Column(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.refresh),
                onPressed: () {
                  _getQuestionsOnAirCalled = false;
                  _getQuestionsOnAir(true);
                },
              )
            ],
          ),
//          Column(children: filteredReqOnAirList)

          NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (scrollInfo.metrics.pixels ==
                    scrollInfo.metrics.maxScrollExtent) {
                  _getQuestionsOnAir();
                }
                return true;
              },
              child: Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: filteredReqOnAirList.length + 1,
                    itemBuilder: (context, index) {
                      if (index == filteredReqOnAirList.length) {
                        if (index == 0) {
                          return Column(
                            children: <Widget>[
                              Container(height: 24),
                              Text(
                                Singleton()
                                    .remoteConfig
                                    .getString('answer_no_question_on_air'),
                                style: TextStyle(fontSize: 18),
                                textAlign: TextAlign.center,
                              )
                            ],
                          );
                        }
                      } else if (filteredReqOnAirList.isNotEmpty) {
                        return Container(
                            padding: EdgeInsets.only(left: 16, right: 16),
                            child: Column(
                              children: <Widget>[
                                filteredReqOnAirList[index],
                                Divider(color: Colors.grey)
                              ],
                            ));
                      }
                      return Container();
                    }),
              ))
        ]);
        break;
    }
  }

  // one of the nav
  Widget questionPage() {
    _showLeftTopLogo(true);
    _tabTitle = Singleton().remoteConfig.getString('tab_title_question');

    Widget questionJobsOnly;
    Widget questionCompanySection;

    if (!_questionJobsOnlySelected) {
      questionJobsOnly = FlatButton(
        child: Text(Singleton().remoteConfig.getString('question_jobs_only')),
        color: Colors.grey[300],
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0)),
        onPressed: () {
          _questionJobsOnlySelected = !_questionJobsOnlySelected;
          questionCompanyController.text = "모든 회사";
          setState(() {
            _selectedPage = questionPage();
          });
        },
      );
      questionCompanySection = Column(
        children: <Widget>[
          Container(height: 8),
          Container(
              decoration: BoxDecoration(
                  borderRadius: new BorderRadius.all(new Radius.circular(4.0)),
                  color: Colors.grey[100]),
              child: TextField(
                controller: questionCompanyController,
                onChanged: (text) {
                  updateSearchCompanyList(questionCompanyController);
                },
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(
                        left: 20, right: 20, top: 10, bottom: 10),
                    border: InputBorder.none,
                    hintMaxLines: 2,
                    hintText: Singleton()
                        .remoteConfig
                        .getString('questions_company_placeholder'),
                    hintStyle: TextStyle(color: Colors.grey, fontSize: 12)),
              )),
          Wrap(
            direction: Axis.horizontal,
            runSpacing: 4.0, // gap between lines
            spacing: 8.0, // gap between adjacent chips
            children: <Widget>[
              for (var companyButton in _searchCompanyButtonList) companyButton,
              if (questionCompanyController.text.isEmpty)
                Text(
                  Singleton()
                      .remoteConfig
                      .getString('question_company_not_found')
                      .replaceAll('\\n', '\n'),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.red[300]),
                ),
            ],
          ),
          Container(height: 20),
        ],
      );
    } else {
      questionJobsOnly = FlatButton(
        child: Text(Singleton().remoteConfig.getString('question_search_company'),
          style: TextStyle(color: Colors.white)),
    color: Colors.blue[300],
    shape: RoundedRectangleBorder(
    borderRadius: new BorderRadius.circular(10.0)),
        onPressed: () {
          _questionJobsOnlySelected = !_questionJobsOnlySelected;
          questionCompanyController.clear();
          setState(() {
            _selectedPage = questionPage();
          });
        },
      );
      questionCompanySection = Container();
    }

    return ListView(children: [
      Container(height: 20),
      Container(
        padding: EdgeInsets.only(left: 40, right: 40),
        child: Column(
          children: <Widget>[
            Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              Text(
                Singleton().remoteConfig.getString('question_company_label'),
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 18),
              ),
              Container(width: 16),
              questionJobsOnly
            ]),
            questionCompanySection,
            Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              Text(
                Singleton().remoteConfig.getString('question_job_label'),
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 16),
              ),
            ]),
            DropdownButton<String>(
              value: _jobForQuestion,
              icon: Icon(Icons.keyboard_arrow_down),
              isExpanded: true,
              iconSize: 24,
              elevation: 16,
              style: TextStyle(color: Colors.black),
              underline: Container(
                height: 2,
                color: Colors.grey,
              ),
              onChanged: (String newValue) {
                _jobForQuestion = newValue;
                setState(() {
                  _selectedPage = questionPage();
                });
              },
              items: _jobList.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(
                    value,
                    style: TextStyle(fontSize: 16),
                  ),
                );
              }).toList(),
            ),
            Container(height: 40),
            Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              Text(
                Singleton().remoteConfig.getString('question_content_label'),
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 16),
              ),
            ]),
            TextField(
              controller: initialQuestionController,
              keyboardType: TextInputType.multiline,
              maxLines: 5,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  hintText: Singleton()
                      .remoteConfig
                      .getString('question_content_placeholder')
                      .replaceAll('\\n', '\n'),
                  hintStyle: TextStyle(fontSize: 14)),
            ),
            Container(height: 20),
            Container(
                padding: EdgeInsets.only(left: 60, right: 60),
                child: FlatButton(
                    color: Colors.red[300],
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0)),
                    onPressed: () {
                      _postNewQuestion();
                    },
                    child: Text(
                        Singleton().remoteConfig.getString('question_button'),
                        style: TextStyle(color: Colors.white, fontSize: 16)))),
            Container(height: 20),
          ],
        ),
      ),
    ]);
  }

  Widget answerChatTile(Map<String, dynamic> chatInfo) {
    InitQA initQA = InitQA(
        questionId: chatInfo["question"]["id"],
        questionerId: chatInfo["questioner"],
        initQ: chatInfo["question"]["content"],
        targetCompany: chatInfo["question"]["company"]["name"],
        targetJob: chatInfo["question"]["job"]["title"],
        questionDate: UTC2LOCAL(chatInfo["question"]["created_at"]),
        answerId: chatInfo["id"],
        initA: chatInfo["content"],
        answererId: chatInfo["answerer"]["id"],
        answererUsername: chatInfo["answerer"]["username"],
        answererCompany: chatInfo["answerer"]["job_histories"][0]["company"]
            ["name"],
        answererCompanyId: chatInfo["answerer"]["job_histories"][0]["company"]
            ["id"],
        answererJob: chatInfo["answerer"]["job_histories"][0]["job"]["title"],
        answererIntro: chatInfo["answerer"]["employee_intro"],
        answerDate: UTC2LOCAL(chatInfo["created_at"]),
        chatRoomId: chatInfo["chat_room"]);

    Widget chatStatusTag;
    if (initQA.chatRoomId == null) {
      chatStatusTag = chatStatusTag = Icon(
        Icons.rate_review,
        color: Colors.grey,
      );
    } else {
      chatStatusTag = Icon(
        Icons.question_answer,
        color: Colors.green[300],
      );
    }

    return Column(
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(top: 10),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: new BorderRadius.all(new Radius.circular(4.0)),
            ),
            child: Column(children: [
              FlatButton(
                padding: EdgeInsets.only(left: 10, right: 10),
                onPressed: () {
                  if (initQA.chatRoomId == null) {
                    employeeRespCheckPage(context, initQA);
                  } else {
                    newChatScreen(context, this, initQA);
                  }
                },
                child: Column(
                  children: <Widget>[
                    Row(
                      children: [
                        Flexible(
                            fit: FlexFit.tight,
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.only(left: 8),
                              child: Text(
                                reformatDateTime(initQA.answerDate),
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.fade,
                                maxLines: 2,
                                style: TextStyle(fontSize: 12),
                              ),
                            )),
                        Flexible(
                            fit: FlexFit.tight,
                            flex: 3,
                            child: Container(
                              padding: EdgeInsets.only(left: 8),
                              child: Text(
                                initQA.initQ,
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.fade,
                                maxLines: 2,
                                style: TextStyle(fontSize: 14),
                              ),
                            )),
                        Flexible(
                            fit: FlexFit.tight,
                            flex: 1,
                            child: Container(
                                padding: EdgeInsets.only(left: 8),
                                child: chatStatusTag)),
                      ],
                    ),
                    Divider(color: Colors.grey),
                    Row(
                      children: <Widget>[
                        Icon(Icons.arrow_right),
                        Expanded(
                          child: Text(
                            initQA.initA,
                            style: TextStyle(fontSize: 14),
                            maxLines: 2,
                            overflow: TextOverflow.fade,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ])),
        Container(
          height: 4,
        ),
      ],
    );
  }

  // each row of the chat history
//  Widget reqChatRow(ChatInfo chatInfo, [List<dynamic> resp]) {
  Widget questionChatTile(Map<String, dynamic> chatInfo) {
    InitQA initQA = InitQA(
        questionId: chatInfo["id"],
        questionerId: chatInfo["questioner"],
        initQ: chatInfo["content"],
        targetJob: chatInfo["job"]["title"],
        targetCompany: chatInfo["company"]["name"],
        questionDate: UTC2LOCAL(chatInfo["created_at"]));

    return Column(
      children: <Widget>[
        Container(height: 4),
        Row(
          children: [
            Flexible(
              fit: FlexFit.tight,
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    initQA.targetCompany,
                    maxLines: 1,
                    overflow: TextOverflow.fade,
                    style: TextStyle(fontSize: 14),
                  ),
                  Text(
                    initQA.targetJob,
                    style: TextStyle(fontSize: 10),
                  ),
                ],
              ),
            ),
            Flexible(
                fit: FlexFit.tight,
                flex: 3,
                child: Container(
                  padding: EdgeInsets.only(left: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        reformatDateTime(initQA.questionDate),
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 12),
                      ),
                      Text(
                        initQA.initQ,
                        textAlign: TextAlign.left,
                        maxLines: 2,
                        overflow: TextOverflow.fade,
                        style: TextStyle(fontSize: 14),
                      ),
                    ],
                  ),
                )),
          ],
        ),
        questionAnswersList(chatInfo),
        Container(height: 4),
      ],
    );
  }

  Widget questionAnswersList(Map<String, dynamic> chatInfo) {
    List<InitQA> initQAs = List<InitQA>();
    for (var answer in chatInfo["answers"]) {
      InitQA newInitQA = InitQA(
          questionId: chatInfo["id"],
          questionerId: chatInfo["questioner"],
          initQ: chatInfo["content"],
          targetJob: chatInfo["job"]["title"],
          targetCompany: chatInfo["company"]["name"],
          questionDate: UTC2LOCAL(chatInfo["created_at"]));

      newInitQA.questionerIntro =
          Singleton().sharedPrefs.getString("user_seeker_intro");
      newInitQA.answererIntro = answer["answerer"]["employee_intro"];

      newInitQA.answerId = answer["id"];
      newInitQA.initA = answer["content"];
      newInitQA.answererId = answer["answerer"]["id"];
      newInitQA.answererUsername = answer["answerer"]["username"];
      newInitQA.answererCompany =
          answer["answerer"]["job_histories"][0]["company"]["name"];
      newInitQA.answererCompanyId =
          answer["answerer"]["job_histories"][0]["company"]["id"];
      newInitQA.answererJob =
          answer["answerer"]["job_histories"][0]["job"]["title"];
      newInitQA.answerDate = UTC2LOCAL(answer["created_at"]);
      newInitQA.chatRoomId = answer["chat_room"];
      initQAs.add(newInitQA);
    }

    List<Widget> tileList = List<Widget>();
    for (var initQA in initQAs) {
      Icon chatStatusIcon;
      Color tileColor;

      if (initQA.chatRoomId == null) {
        chatStatusIcon = Icon(Icons.fiber_new, color: Colors.red[300]);
        tileColor = Colors.grey;
      } else if (initQA.chatRoomId != null) {
        chatStatusIcon = Icon(Icons.question_answer, color: Colors.green[300]);
        tileColor = Colors.green[300];
      }

      tileList.add(Container(
        height: 42,
        decoration: BoxDecoration(
          border: Border.all(color: tileColor),
          borderRadius: new BorderRadius.all(new Radius.circular(4.0)),
        ),
        child: FlatButton(
          padding: EdgeInsets.zero,
          onPressed: () {
            if (initQA.chatRoomId == null) {
              seekerAnswerCheckPage(context, this, initQA);
            } else {
              newChatScreen(context, this, initQA);
            }
          },
          child: Row(children: <Widget>[
            Flexible(
              fit: FlexFit.tight,
              flex: 1,
              child: Icon(
                Icons.arrow_right,
                color: tileColor,
              ),
            ),
            Flexible(
              fit: FlexFit.tight,
              flex: 5,
              child: Text(
                '[${initQA.answererUsername}] ${initQA.initA}',
                style: TextStyle(fontSize: 14),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Flexible(fit: FlexFit.tight, flex: 2, child: chatStatusIcon)
          ]),
        ),
      ));
    }

    return Column(children: tileList);
  }

  // one of the nav
  // needs to be different for seeker/helper
  Widget chatListPage() {
    _showLeftTopLogo(false);
    _tabTitle = Singleton().remoteConfig.getString('tab_title_chat');
    int chatListCount = 0;

    if (_isSeekerChatMode) {
      chatListCount = _questionChatData.length;
    } else {
      chatListCount = _respChatData.length;
    }

    Widget subtitleRowContents;
    if (singleton.sharedPrefs.getInt('user_role') == 2 &&
        _companyVerificationState == CompanyVerificationState.verified) {
      Color answersSubtitleColor;
      TextDecoration answersSubtitleDecoration;
      Color questionsSubtitleColor;
      TextDecoration questionsSubtitleDecoration;

      if (_isSeekerChatMode) {
        answersSubtitleColor = Colors.grey;
        answersSubtitleDecoration = TextDecoration.none;
        questionsSubtitleColor = Colors.black;
        questionsSubtitleDecoration = TextDecoration.underline;
      } else {
        answersSubtitleColor = Colors.black;
        answersSubtitleDecoration = TextDecoration.underline;
        questionsSubtitleColor = Colors.grey;
        questionsSubtitleDecoration = TextDecoration.none;
      }
      subtitleRowContents = ButtonBar(
        children: [
          FlatButton(
            padding: EdgeInsets.zero,
            child: Text(
                Singleton()
                    .remoteConfig
                    .getString('tab_subtitle_answers_button'),
                style: TextStyle(
                    fontSize: 20,
                    color: answersSubtitleColor,
                    decoration: answersSubtitleDecoration)),
            onPressed: () {
              _isSeekerChatMode = false;
              setState(() {
                _selectedPage = chatListPage();
              });
            },
          ),
          FlatButton(
            padding: EdgeInsets.zero,
            child: Text(
                Singleton()
                    .remoteConfig
                    .getString('tab_subtitle_questions_button'),
                style: TextStyle(
                    fontSize: 20,
                    color: questionsSubtitleColor,
                    decoration: questionsSubtitleDecoration)),
            onPressed: () {
              _isSeekerChatMode = true;
              setState(() {
                _selectedPage = chatListPage();
              });
            },
          ),
        ],
      );
    } else {
      _isSeekerChatMode = true;
//      subtitleRowContents = Text(
//        Singleton().remoteConfig.getString('tab_subtitle_questions_button'),
//        style: TextStyle(fontSize: 18),
//      );
      subtitleRowContents = Container();
    }

    return Column(children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Row(children: [subtitleRowContents]),
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              if (_isSeekerChatMode == true)
                _getMyQuestions(true);
              else
                _getMyAnswers(true);
            },
          )
        ],
      ),
      NotificationListener<ScrollNotification>(
          onNotification: (ScrollNotification scrollInfo) {
            if (scrollInfo.metrics.pixels ==
                scrollInfo.metrics.maxScrollExtent) {
              if (_isSeekerChatMode) {
                _getMyQuestions();
              } else {
                _getMyAnswers();
              }
            }
            return true;
          },
          child: Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: chatListCount + 1,
                itemBuilder: (context, index) {
                  if (index == chatListCount) {
                    if (index == 0) {
                      return Column(
                        children: <Widget>[
                          Container(height: 24),
                          Text(
                            Singleton()
                                .remoteConfig
                                .getString('chat_no_session_message'),
                            style: TextStyle(fontSize: 18),
                            textAlign: TextAlign.center,
                          )
                        ],
                      );
                    }
                  } else if (_isSeekerChatMode &&
                      _questionChatData.isNotEmpty) {
                    return Container(
                        padding: EdgeInsets.only(left: 16, right: 16),
                        child: Column(
                          children: <Widget>[
                            questionChatTile(_questionChatData[index]),
                            Divider(color: Colors.grey)
                          ],
                        ));
                  } else if (!_isSeekerChatMode && _respChatData.isNotEmpty) {
                    return Container(
                        padding: EdgeInsets.only(left: 16, right: 16),
                        child: Column(
                          children: <Widget>[
                            answerChatTile(_respChatData[index]),
                          ],
                        ));
                  }
                  return Container();
                }),
          ))
    ]);
  }

  Widget employeeSignupSection() {
    return ListView(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 20,
            ),
            Container(
                child: Text(
                    singleton.sharedPrefs.getString('user_fullname') +
                        '님,\n' +
                        Singleton()
                            .remoteConfig
                            .getString('signup_comveri_email_message'),
                    style: TextStyle(color: Colors.black38))),
            Container(
              height: 20,
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                  padding: EdgeInsets.only(left: 80, right: 80),
                  child: Text(
                      Singleton()
                          .remoteConfig
                          .getString('signup_comveri_company_label'),
                      style: TextStyle(color: Colors.black38))),
            ),
            Container(
                margin: EdgeInsets.only(left: 80, right: 80),
                decoration: BoxDecoration(
                    borderRadius:
                        new BorderRadius.all(new Radius.circular(4.0)),
                    color: Colors.grey[100]),
                child: TextField(
                  controller: companyNameController,
                  onChanged: (text) {
                    updateSearchCompanyList(companyNameController);
                  },
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(
                          left: 20, right: 20, top: 10, bottom: 10),
                      border: InputBorder.none,
                      hintMaxLines: 2,
                      hintText: Singleton()
                          .remoteConfig
                          .getString('signup_comvery_company_name_placeholder'),
                      hintStyle: TextStyle(color: Colors.grey, fontSize: 12)),
                )),
            Wrap(
              direction: Axis.horizontal,
              alignment: WrapAlignment.center,
              runSpacing: 4.0, // gap between lines
              spacing: 8.0, // gap between adjacent chips
              children: <Widget>[
                for (var companyButton in _searchCompanyButtonList)
                  companyButton,
              ],
            ),
            Container(
              height: 20,
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                  padding: EdgeInsets.only(left: 80, right: 80),
                  child: Text(
                      Singleton()
                          .remoteConfig
                          .getString('signup_comveri_email_label'),
                      style: TextStyle(color: Colors.black38))),
            ),
            Container(
                padding: EdgeInsets.only(left: 80, right: 80),
                child: TextField(
                  controller: companyEmailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      hintText: Singleton()
                          .remoteConfig
                          .getString('signup_comveri_email_placeholder')),
                )),
            Container(
              height: 20,
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                  padding: EdgeInsets.only(left: 80, right: 80),
                  child: Text(
                      Singleton().remoteConfig.getString('signup_comveri_job'),
                      style: TextStyle(color: Colors.black38))),
            ),
            Container(
              padding: EdgeInsets.only(left: 80, right: 80),
              child: DropdownButton<String>(
                value: _jobForRegistration,
                icon: Icon(Icons.keyboard_arrow_down),
                isExpanded: true,
                iconSize: 24,
                elevation: 16,
                style: TextStyle(color: Colors.black),
                underline: Container(
                  height: 2,
                  color: Colors.grey,
                ),
                onChanged: (String newValue) {
                  setState(() {
                    _jobForRegistration = newValue;
                    _selectedPage = questionOnAirListPage();
                  });
                },
                items: _jobList.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            Container(
                padding: EdgeInsets.only(left: 60, right: 60),
                child: FlatButton(
                    color: Colors.black12,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0)),
                    onPressed: () {
                      if (companyEmailController.text.isEmpty) {
                        userScaffoldKey.currentState.showSnackBar(SnackBar(
                            duration:
                                const Duration(seconds: SNACKBAR_DURATION),
                            content: Text(Singleton()
                                .remoteConfig
                                .getString('signup_comveri_sb_check_email'))));
                      } else {
                        sendCompanyEmailVerification();
                      }
                    },
                    child: Text(
                        Singleton()
                            .remoteConfig
                            .getString('signup_comveri_button'),
                        style: TextStyle(
                            fontWeight: FontWeight.w400, fontSize: 16)))),
            Container(
              height: 20,
            ),
            Container(
                child: Text(
                    Singleton()
                        .remoteConfig
                        .getString('signup_comveri_cs_message'),
                    style: TextStyle(color: Colors.black38))),
          ],
        ),
      ],
    );
  }

  Widget checkCompanyEmailSection() {
    return Column(
      children: [
        Container(
          height: 180,
        ),
        Container(
          child: Text(
              Singleton()
                  .remoteConfig
                  .getString('signup_comveri_email_sent_message')
                  .replaceAll('\\n', '\n'),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18)),
        ),
      ],
    );
  }
}
