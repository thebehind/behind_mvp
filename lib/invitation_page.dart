/// 2019.10.27
/// invitation page
///   page to send invitatino link to friends in users contact list
part of 'main.dart';

final GlobalKey<ScaffoldState> invitationPageScaffoldKey =
    GlobalKey<ScaffoldState>();

class InvitationWidget extends StatefulWidget {
  @override
  State createState() => new InvitationWidgetState();
}

class InvitationWidgetState extends State<InvitationWidget> {
  InvitationWidgetState();

  Iterable<Contact> _contactList;
  List<String> _recipients;

  double _tilePaddingHeight;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  initState() {
    super.initState();
    _contactList = List<Contact>();
    _recipients = List<String>();
    _tilePaddingHeight = Singleton().height * 0.03;

    _getContactList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: invitationPageScaffoldKey,
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          Singleton().remoteConfig.getString('invite_to_behind_page_title'),
          style: TextStyle(fontSize: 24, color: Colors.black),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        leading: Builder(builder: (BuildContext context) {
          return IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(context);
            },
          );
        }),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: _contactTileListView(),
    );
  }

  Future<void> _sendMMS(List<String> recipients) async {
    String message = (Platform.isAndroid)
        ? Singleton().remoteConfig.getString('link_to_play_store')
        : Singleton().remoteConfig.getString('link_to_app_store');
    message =
        message + '\n' + Singleton().remoteConfig.getString('invite_message');

    String _result =
        await FlutterSms.sendSMS(message: message, recipients: recipients)
            .catchError((onError) {
      print(onError);
    });
//    print(_result);
    _recipients.clear();
  }

  Future<void> _sendSMS(String recipient) async {
    String message = (Platform.isAndroid)
        ? Singleton().remoteConfig.getString('link_to_play_store')
        : Singleton().remoteConfig.getString('link_to_app_store');
    message =
        message + '\n' + Singleton().remoteConfig.getString('invite_message');
    List<String> recipients = List<String>();
    recipients.add(recipient);

    String _result =
        await FlutterSms.sendSMS(message: message, recipients: recipients)
            .catchError((onError) {
      print(onError);
    });
//    print(_result);
  }

  Future<void> _getContactList() async {
    // Checking permission #
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.contacts);
    if (permission != PermissionStatus.granted) {
      // Requesting permission #
      // iOS - info.plist - NSContactsUsageDescription
      Map<PermissionGroup, PermissionStatus> permissions =
          await PermissionHandler()
              .requestPermissions([PermissionGroup.contacts]);
      if (permissions[PermissionGroup.contacts] != PermissionStatus.granted) {
        // Open app settings #
//          bool isOpened = await PermissionHandler().openAppSettings();
        // Show a rationale for requesting permission (Android only) #
//        bool isShown = await PermissionHandler()
//            .shouldShowRequestPermissionRationale(PermissionGroup.contacts);
      }
    }
    // Get all contacts on device
    _contactList = await ContactsService.getContacts(withThumbnails: false);
    setState(() {});
  }

  Widget _contactTileListView() {
    Widget _contactTile(String name, String phoneNumber) {
      Color checkColor;
      bool isSelected = _recipients.contains(phoneNumber);
      if (isSelected) {
        checkColor = Colors.blue[300];
      } else {
        checkColor = Colors.grey[300];
      }
      return Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: IconButton(
                      onPressed: () {
                        setState(() {
                          if (!isSelected) {
                            _recipients.add(phoneNumber);
                          } else {
                            _recipients.remove(phoneNumber);
                          }
                        });
                      },
                      icon: Icon(
                        Icons.check_circle_outline,
                        color: checkColor,
                      ))),
              Flexible(
                flex: 4,
                fit: FlexFit.tight,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(name, style: TextStyle(fontSize: 20)),
                    Text(phoneNumber)
                  ],
                ),
              ),
              Flexible(
                  flex: 2,
                  fit: FlexFit.tight,
                  child: Container(
                      padding: EdgeInsets.only(right: 4),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: GestureDetector(
                            onTap: () {
                              _sendSMS(phoneNumber);
                            },
                            child: Container(
                                padding: EdgeInsets.all(8),
                                color: Colors.blue[300],
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.mail_outline,
                                      color: Colors.white,
                                      size: 14,
                                    ),
                                    Container(width: 4),
                                    Text(
                                        Singleton().remoteConfig.getString(
                                            'invite_individual_sms_button'),
                                        style: TextStyle(color: Colors.white))
                                  ],
                                )),
                          )))),
            ],
          ),
          Container(height: _tilePaddingHeight)
        ],
      );
    }

    List<Widget> contactTileList = List<Widget>();
    for (Contact contact in _contactList) {
      for (Item phone in contact.phones) {
        String givenName = contact.givenName != null ? contact.givenName : '';
        String familyName =
            contact.familyName != null ? contact.familyName : '';
        String displayName = givenName + ' ' + familyName;
        if (givenName.isEmpty && familyName.isEmpty) {
          displayName = contact.displayName;
        }
        contactTileList.add(_contactTile(displayName, phone.value));
        break;
      }
    }
//    if (contactTileList.length == 0) {
//      return Center(child: CircularProgressIndicator());
//    }

    Color sendMMSButtonColor;
    if (_recipients.isNotEmpty) {
      sendMMSButtonColor = Colors.blue[300];
    } else {
      sendMMSButtonColor = Colors.grey[300];
    }

    return Column(
      children: <Widget>[
        Expanded(child: ListView(shrinkWrap: true, children: contactTileList)),
        Container(
          padding: EdgeInsets.symmetric(vertical: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.only(right: 4),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: GestureDetector(
                        onTap: () {
                          if (_recipients.isNotEmpty) {
                            _sendMMS(_recipients);
                          }
                        },
                        child: Container(
                            padding: EdgeInsets.all(8),
                            color: sendMMSButtonColor,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.mail_outline,
                                  color: Colors.white,
                                  size: 14,
                                ),
                                Container(width: 4),
                                Text(
                                    Singleton()
                                        .remoteConfig
                                        .getString('invite_mms_button'),
                                    style: TextStyle(color: Colors.white))
                              ],
                            )),
                      )))
            ],
          ),
        )
      ],
    );
  }
}
